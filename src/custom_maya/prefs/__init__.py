"""
Maya Script Editor's customization Preferences management files.
"""

# shorten absolute imports
from custom_maya.prefs.factory import *
from custom_maya.prefs.data import *
