"""
"Preferences" window for customization editing.
"""

from PySide2 import QtWidgets, QtCore

from custom_maya import utils
from custom_maya import controls
from custom_maya import dialogs
from custom_maya.prefs import palette_editor
from custom_maya.prefs import PreferencesManager
from custom_maya.stylesheet import apply_style

WINDOW_OBJECT_NAME = 'CSE_PreferencesWindow'


class PreferencesWindow(QtWidgets.QDialog):
    """ Configuration window. """

    def __init__(self, prefs=None, *args, **kwargs):
        super(PreferencesWindow, self).__init__(*args, modal=True, **kwargs)

        self._prefs = PreferencesManager(parent=self) if prefs is None else prefs

        self.setWindowTitle('Preferences')
        self.setObjectName(WINDOW_OBJECT_NAME)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        apply_style(self)

        main_layout = QtWidgets.QVBoxLayout(self)
        widget = QtWidgets.QWidget(self)
        layout = QtWidgets.QVBoxLayout(widget)

        # create categories editors
        console_editor = ConsoleEditor(self._prefs, parent=self)
        mel_editor = TabEditor('MEL', self._prefs, parent=self)
        python_editor = TabEditor('Python', self._prefs, parent=self)
        expr_editor = ExpressionEditor(self._prefs, parent=self)
        help_editor = QuickHelpEditor(self._prefs, parent=self)
        console_filter_editor = Editor('Console-filter', self._prefs, parent=self)
        save_button = QtWidgets.QPushButton('Save and Close', parent=self)

        # populate layout
        layout.addWidget(console_editor)
        layout.addWidget(mel_editor)
        layout.addWidget(python_editor)
        layout.addWidget(expr_editor)
        layout.addWidget(help_editor)
        layout.addWidget(console_filter_editor)
        layout.addStretch(1)

        self.scroll_area = QtWidgets.QScrollArea(self)
        main_layout.addWidget(self.scroll_area)
        self.scroll_area.setWidget(widget)
        self.scroll_area.show()

        main_layout.addWidget(save_button)

        self.scroll_area.setStyleSheet('background: transparent;')
        self.scroll_area.setFrameShape(QtWidgets.QFrame.NoFrame)
        widget.setStyleSheet('background: rgba(0, 0, 0, 0);')

        # set some size/margins rules
        self.setContentsMargins(4, 4, 4, 4)
        main_layout.setContentsMargins(0, 0, 0, 0)
        layout.setContentsMargins(0, 0, 0, 0)

        # connect signal
        for editor in (console_editor, mel_editor, python_editor, help_editor):
            editor.palette_editor_asked.connect(self._open_palette_editor)
        save_button.clicked.connect(self.save_and_close)

    def save_and_close(self):
        """ Save current data to user prefs file and close this dialog. """

        self._prefs.save()
        self.close()

    def _open_palette_editor(self, rule):

        theme, profile = palette_editor.run(rule, parent=self, prefs=self._prefs)

        print ('theme', theme)
        print ('profile', profile)

    def clamp_size(self):
        self.setFixedWidth(self.size().width())
        self.setMaximumHeight(self.scroll_area.widget().height() +40)
        self.resize(self.size().width(), self.maximumHeight())

    def closeEvent(self, event):
        """
        Ask for save before closing if some modifications have not been saved.
        """

        # in case window would have been closed but not delete yet
        if not self.isVisible():
            return

        if self._prefs.is_dirty():
            user_answer = dialogs.save_before_close_prompt(self)

            # keep window opened
            if user_answer == dialogs.CANCEL:
                event.ignore()
                return

            if user_answer == dialogs.SAVE:
                self._prefs.save()

        event.accept()


class Editor(QtWidgets.QGroupBox):
    """ Custom QGroupBoxes base class. """

    palette_editor_asked = QtCore.Signal(str)

    def __init__(self, rule, _data, title=None, parent=None):
        """
        Args:
            rule (str)
            _data (dict)
            title (str, optional)
            parent (QtWidgets.QWidget, optional)

        Inititates self.
        """

        title = title if title else rule
        super(Editor, self).__init__(title, parent, checkable=True)

        self._data = _data
        self._rule = rule.lower()

        self.lay = QtWidgets.QVBoxLayout(self)

        self.setContentsMargins(6, 20, 6, 6)
        self.lay.setContentsMargins(4, 4, 4, 4)
        self.lay.setSpacing(4)

        # set sartup checked state from config data
        self.setChecked(self._data.get_by_names(self._rule, 'enabled'))
        # connect signal to dict's update
        self.clicked.connect(self.update_data)

        self.dirty_state = controls.DirtyHandler(self)

    def update_data(self):
        """ Update rule's enabled value into data. """
        self._data.set_by_names(self._rule, 'enabled', self.isChecked())
        self.dirty_state.set(self._data.is_attr_dirty(self._rule, 'enabled'))

    def add_editor(self, rule, title=None):
        """
        Args:
            rule (str)
            title (str, optional)

        Add a new QGroupBox to this one.
        """

        new_editor = Editor(rule, self._data, title=title, parent=self)
        self.lay.addWidget(new_editor)
        new_editor.palette_editor_asked.connect(self.palette_editor_asked.emit)

        return new_editor

    def add_checkbox(self, attribute):
        """
        Args:
            attribute (str)

        Add QCheckbox control to this QGroupBox for dict's <attribute>.
        """

        new_checkbox = controls.AttrCheckBox(self._rule, attribute, self._data, self)
        self.lay.addWidget(new_checkbox)

        return new_checkbox

    def add_combobox(self, attribute):
        """
        Args:
            attribute (str)

        Add QComboBox control to this QGroupBox for dict's <attribute>.
        """

        new_combobox = controls.ComboBox(self._rule, attribute, self._data, self)

        # connect palette button to Palette Editor opening
        if attribute == 'palette':
            new_combobox.button.clicked.connect(
                lambda : self.palette_editor_asked.emit(self._rule)
            )

        self.lay.addWidget(new_combobox)

        return new_combobox


class ConsoleEditor(Editor):
    """ Custom QGroupBox for Console highlight. """

    def __init__(self, _data, parent=None):
        super(ConsoleEditor, self).__init__('console', _data, title='Console',
                                                              parent=parent)

        self.add_combobox('palette')
        self.add_combobox('profile')

        mel_editor = self.add_editor(
            'console|mel-highlight',
            title='MEL highlight'
        )
        mel_editor.add_combobox('palette')
        mel_editor.add_combobox('profile')

        python_editor = self.add_editor(
            'console|python-highlight',
            title='Python highlight'
        )
        python_editor.add_combobox('palette')
        python_editor.add_combobox('profile')


class TabEditor(Editor):
    """ Custom QGroupBox for MEL or Python tabs highlights. """

    def __init__(self, rule, _data, title=None, parent=None):
        super(TabEditor, self).__init__(rule, _data, title=title, parent=parent)

        self.add_combobox('palette')
        self.add_combobox('profile')
        self.add_checkbox('blocks-collapse')
        self.add_checkbox('multi-cursors')
        self.add_checkbox('snippets')
        self.add_checkbox('hotkeys')

class ExpressionEditor(Editor):
    """ Custom QGroupBox for MEL or Python tabs highlights. """

    def __init__(self, _data, title=None, parent=None):
        super(ExpressionEditor, self).__init__(
            'expressions', _data, title=title, parent=parent
        )

        mel_editor = self.add_editor(
            'expressions|mel-highlight',
            title='MEL highlight'
        )
        mel_editor.add_combobox('palette')
        mel_editor.add_combobox('profile')

        self.add_checkbox('multi-cursors')
        self.add_checkbox('hotkeys')


class QuickHelpEditor(Editor):
    """ Custom QGroupBox for MEL or Python tabs highlights. """

    def __init__(self, _data, title=None, parent=None):
        super(QuickHelpEditor, self).__init__('Quick-Help', _data, title=title, parent=parent)

        self.add_combobox('palette')
        self.add_combobox('profile')


def run(pos=None):
    maya_ui_qt = utils.get_maya_window()
    utils.close_existing(maya_ui_qt, WINDOW_OBJECT_NAME)

    prefs = utils.get_child_instance(PreferencesManager)

    window = PreferencesWindow(prefs=prefs, parent=maya_ui_qt)
    window.show()
    window.clamp_size()

    # center window on mouse position
    if pos:
        window_size = window.size()
        centered_pos = (
            pos.x() -window_size.width()/2,
            max(0, pos.y() -window_size.height()/2),
        )

        window.move(*centered_pos)
