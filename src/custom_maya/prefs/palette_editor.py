"""
Palettes editor window. Called by Preferences window, allows user to edit palettes
and applied profiles on it.
"""

import os

from PySide2 import QtWidgets, QtCore, QtGui

from custom_maya import constants as kk
from custom_maya import utils
from custom_maya import controls
from custom_maya import dialogs
from custom_maya.prefs import data
from custom_maya.managers import HighlightManager
from custom_maya.stylesheet import apply_style


WINDOW_OBJECT_NAME = 'CSE_PaletteEditor'


class updates_disabled(object):
    """
    Context manager to disable highlight and controls updates while editing
    QCombobox contents.
    """

    def __init__(self, editor):

        self.editor = editor

    def __enter__(self, *args):
        self.editor._update_enabled = False

    def __exit__(self, *args):
        self.editor._update_enabled = True


class PaletteEditor(QtWidgets.QDialog):
    """ Configuration window. """

    update_requested = QtCore.Signal()

    def __init__(self, rule, prefs=None, *args, **kwargs):
        super(PaletteEditor, self).__init__(*args, modal=True, **kwargs)

        self._rule = rule
        self._widgets_by_attr = {}
        self._palette_data = {}

        self._update_enabled = True
        self._lasso = None

        self._prefs = data.PreferencesManager(parent=self) if prefs is None else prefs

        self.setWindowTitle('Palette editor - {}'.format(rule.replace('|', ' | ')))
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setObjectName(WINDOW_OBJECT_NAME)

                ###########################################
                #           set dialog's style            #
                ###########################################

        apply_style(self)

                    ###########################################
                    #     create main widgets and layouts     #
                    ###########################################

        layout = QtWidgets.QVBoxLayout(self)
        splitter = QtWidgets.QSplitter(self)
        # create controls widget and layout
        left_widget = QtWidgets.QWidget(self)
        left_layout = QtWidgets.QVBoxLayout(left_widget)
        controls_widget = QtWidgets.QWidget(self)
        self.lay = QtWidgets.QVBoxLayout(controls_widget)

        self._apply_button = QtWidgets.QPushButton('Apply and close', self)
        self._save_palette_button = QtWidgets.QPushButton('Save palette as...', self)
        self._save_profile_button = QtWidgets.QPushButton('Save profile as...', self)

                    ###########################################
                    #               set toolbar               #
                    ###########################################

        toolbar = QtWidgets.QToolBar(self, floatable=False, movable=False)
        toolbar.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        # highlight presets
        theme_label = QtWidgets.QLabel('Theme : ', self)
        self._theme_box = QtWidgets.QComboBox(self)
        # profile presets
        profile_label = QtWidgets.QLabel('Profile : ', self)
        self._profile_box = QtWidgets.QComboBox(self)

        toolbar.addWidget(theme_label)
        toolbar.addWidget(self._theme_box)
        toolbar.addWidget(profile_label)
        toolbar.addWidget(self._profile_box)

                    ###########################################
                    #  create QTextEdit and HighlightManager  #
                    ###########################################

        # create QTextEdit and HighlightManager
        self._txt_edit = QtWidgets.QTextEdit(self)
        self._txt_edit.setFont(QtGui.QFont("DejaVu Sans Mono", 8))
        self._txt_edit.setLineWrapMode(self._txt_edit.NoWrap)
        self._txt_edit.setObjectName(WINDOW_OBJECT_NAME +'_TextEdit')

        self._manager = HighlightManager(enabled=False, prefs=self._prefs, parent=self)
        self._highlighter = self._manager.apply(self._txt_edit, self._rule)

                    ###########################################
                    #             populate layouts            #
                    ###########################################

        # populate main layout
        layout.addWidget(toolbar)
        layout.addWidget(splitter)
        layout.addWidget(self._apply_button)
        # populate splitter
        splitter.addWidget(left_widget)
        splitter.addWidget(self._txt_edit)
        left_layout.addWidget(controls_widget)
        left_layout.addWidget(self._save_palette_button)
        left_layout.addWidget(self._save_profile_button)

                    ###########################################
                    #       set few size/margins rules        #
                    ###########################################

        for widget in (layout, controls_widget):
            widget.setContentsMargins(0, 0, 0, 0)

        for widget in (self, self.lay):
            widget.setContentsMargins(4, 4, 4, 4)

        layout.setStretch(0, 1)
        self.resize(900, 750)

                    ###########################################
                    #           set startup updates           #
                    ###########################################

        self._set_text_sample()
        self._update_theme_box(self._rule)
        self._update_profile_box(self._rule)
        self._sync_palette_data()
        self._set_controls()
        self._update()

                    ###########################################
                    #         edit signals and events         #
                    ###########################################

        # prevent keys propagation to Maya Window
        self.keyPressEvent = lambda *args: None
        self.keyReleaseEvent = lambda *args: None
        # connect signals
        self._save_palette_button.clicked.connect(self._save_palette_as)
        self._save_profile_button.clicked.connect(self._save_profile_as)
        self._apply_button.clicked.connect(self._apply_and_close)

        self._theme_box.currentTextChanged.connect(self._on_theme_changed)
        self._profile_box.currentTextChanged.connect(self._on_profile_changed)

    def _get_theme(self):
        """ Returns: (str) """
        return self._theme_box.currentText()

    def _get_profile(self):
        """ Returns: (str) """
        return self._profile_box.currentText()

    def _get_palette_as_dict(self):
        """
        Returns:
            (dict)

        Get palette's data as dict from window's AttributeWidgets.
        """

        result_dict = {}

        for attr in self._widgets_by_attr or ():
            widget = self._widgets_by_attr[attr]

            result_dict[attr] = {
                'value': widget.value(ignore_override=True)
            }

            # do not add "enabled" and "link" attributes on default attributes
            # as "background" and "normal"
            if widget.has_checkbox():
                result_dict[attr]['enabled'] = widget.is_enabled()
                result_dict[attr]['link'] = widget.get_link_as_str()

        return result_dict

    def _get_profile_as_dict(self):
        """
        Returns:
            (dict)

        Get profile's data as dict from window's AttributeWidgets.
        """

        result_dict = {}

        for attr in self._widgets_by_attr or ():
            widget = self._widgets_by_attr[attr]

            if not widget.has_checkbox():
                continue

            result_dict[attr] = widget.is_enabled()

        return result_dict

    def get_under_mouse_attr_widget(self):
        """
        Returns:
            (controls.AttributeWidget or None)

        Get AttributeWidget under mouse, even if some widgets (as Lasso widgets)
        are overlaying it.
        """

        for attr in self._widgets_by_attr:
            widget = self._widgets_by_attr[attr]

            if widget.under_mouse():
                return widget

        return None

    def _sync_palette_data(self):
        """
        Get profile-resulting palette's data upated into self._palette_data
        (called before self._set_controls).
        """

        palette = self._manager.get_palette_by_name(self._rule)
        self._palette_data = palette.get_profile_applied_data()

    def _on_theme_changed(self, theme):
        """
        Update HighlightManager's corresponding palette's data from <theme>
        (triggers text's re-highlight) and update window's controls.
        """

        self._manager.set_rule_theme(self._rule, theme)

        self._sync_palette_data()
        self._set_controls()

    def _on_profile_changed(self, profile):
        """
        Update HighlightManager's corresponding profile's data from <profile>
        (triggers text's re-highlight) and update window's controls.
        """

        self._manager.set_rule_profile(self._rule, profile)

        self._sync_palette_data()
        self._set_controls()

    def _save_palette_as(self):
        """ Save current palette. """

        root_dir = os.path.join(kk.PALETTES_ROOT, self._rule)

        file_path = dialogs.get_save_name(
            'palette',
            root_dir=root_dir,
            selected=self._get_theme(),
            parent=self
        )

        if file_path is None:
            return

        palette_data = self._get_palette_as_dict()
        data.save_to_file(file_path, palette_data)

        short_name = file_path.split('/')[-1].split('.json')[0]
        self._theme_box.addItem(short_name)
        self._theme_box.setCurrentText(short_name)

    def _save_profile_as(self):
        """ Save current palette. """

        root_dir = os.path.join(kk.PALETTES_ROOT, self._rule, '.profiles')

        file_path = dialogs.get_save_name(
            'profile',
            root_dir=root_dir,
            selected=self._get_profile(),
            parent=self
        )

        profile_data = self._get_profile_as_dict()
        data.save_to_file(file_path, profile_data)

        short_name = file_path.split('/')[-1].split('.json')[0]
        self._profile_box.addItem(short_name)
        self._profile_box.setCurrentText(short_name)

    def _apply_and_close(self):
        pass

    def _on_link_changed(self, old_link, new_link):
        """
        Args:
            old_link (controls.AttributeWidget)
            new_link (controls.AttributeWidget)

        Update event filters and signals connections on link change.
        """

        widget = self.sender()

        # disconnect old signal and remove old event filter
        if old_link:
            old_link.color_changed.disconnect(widget.on_linked_color_changed)
            old_link.removeEventFilter(self)
            widget.remove_override()

        # connect color_changed on new link
        if new_link:
            new_link.color_changed.connect(widget.on_linked_color_changed)
            widget.override_rgb(new_link.value())

    def _on_color_changed(self):
        """
        Update palette data on attribute change.
        """

        widget = self.sender()
        palette = self._manager.get_palette_by_name(self._rule)
        palette.set_color(widget.attr, widget.value())

    def _on_enabled_changed(self):
        """
        Update palette data on attribute's enabled state change.
        """

        widget = self.sender()
        profile = self._manager.get_profile_by_name(self._rule)
        profile.set_attr(widget.attr, widget.is_enabled())

    def _on_create_link_asked(self):
        """
        Create Lasso widget for "create_link_asked" QtCore.Signal() sender.
        """

        widget = self.sender()
        pos = QtCore.QPoint(widget.width() -33, widget.height()/2)

        self._lasso = Lasso(widget.mapTo(self, pos), widget, self)
        self._lasso.setGeometry(self.geometry())
        self._lasso.move(0, 0)

        self._lasso.link_clicked.connect(self._on_lasso_link_clicked)

        self._lasso.show()

    def _on_lasso_link_clicked(self, destination_widget, source_widget):
        """
        Args:
            destination_widget (controls.AttributeWidget) : the link receiver
            source_widget (controls.AttributeWidget)

        Set <destination_widget> linked on <source_widget> and remove current Lasso.
        """

        if source_widget is not None:
            destination_widget.set_link(source_widget)

        self._lasso.deleteLater()
        self._lasso = None

    def _clear_layout(self):
        """ Clear all layout's widgets. """

        while self.lay.count():
            widget = self.lay.takeAt(0).widget()

            if not widget:
                break

            widget.deleteLater()

    def _set_text_sample(self):
        """
        Set QTextEdit's content from the appropriate sample.txt file.
        """

        # add sample.txt content into the QTextEdit
        sample_file = os.path.join(kk.PALETTES_ROOT, '{}/sample.txt'.format(
                data.to_parent_rule_name(self._rule)
            )
        )

        # set the sample text
        with open(sample_file, 'r') as opened_file:
            text = opened_file.read()
            self._txt_edit.setText(text)

    def _set_controls(self):
        """
        Create all AttributeWidgets and Frames for current palette and set their
        startup values.
        """

        # disable both window and highlighter updates (especially highlighter's,
        # so it won't update at each color/link/enabled change)
        self.setUpdatesEnabled(False)
        self._highlighter.set_enabled(False)

        # remove all current layout's widgets
        self._clear_layout()

        template_dict = data.get_template_data(self._rule)
        attr_template = template_dict['attributes']
        attributes = sorted(attr_template.keys(), key=lambda x: attr_template[x]["label"])

        pages = template_dict["pages"]
        pages_dict = {}

        # create pages Frame widgets
        root = self

        for i, page in enumerate(pages):
            if not '|' in page:
                new_page = controls.Frame(page, root)
                self.lay.addWidget(new_page)
                pages_dict[page] = new_page

            # nested frames
            else:
                tokens = page.split('|')
                root = self

                while tokens:
                    name = tokens[0]

                    # Frame already exists
                    if name in pages_dict:
                        root = pages_dict[name]

                    # create Frame
                    else:
                        new_page = controls.Frame(page, root)
                        root.lay.addWidget(new_page)
                        root = new_page
                        pages_dict[page] = new_page

                    tokens = tokens[1:]

        self._widgets_by_attr = {}

        # add every parameter with its QColorButton set
        for attr in attributes or ():
            # attribute's palette data dict
            attr_dict = self._palette_data[attr]
            # infos from dicts
            value = attr_dict['value']
            enabled = attr_dict['enabled'] if 'enabled' in attr_dict else None
            label = attr_template[attr]["label"]
            tooltip = attr_template[attr]["tooltip"]
            page = attr_template[attr]["page"]

            # create widget
            widget = controls.AttributeWidget(attr, label, value, enabled, tooltip, self)

            # add widget to the root layout
            if not page:
                self.lay.addWidget(widget)

            # add widget the corresponding page's layout
            else:
                pages_dict[page].lay.addWidget(widget)
                widget.add_left_padding(pages_dict[page].button().level())

            # store widget for furthur operations
            self._widgets_by_attr[attr] = widget

            # connect signals
            widget.color_changed.connect(self._on_color_changed)
            widget.enabled_changed.connect(self._on_enabled_changed)
            widget.link_changed.connect(self._on_link_changed)
            widget.create_link_asked.connect(self._on_create_link_asked)

        # set all links (must be set after having added all the attributes as we
        # to be able to connect all signals to existing widgets)
        for attr in attributes or ():
            attr_dict = self._palette_data[attr]
            widget = self._widgets_by_attr[attr]
            link = attr_dict['link'] if 'link' in attr_dict else None

            if link is not None:
                widget.set_link(self._widgets_by_attr[link], startup=True)

        # alternate attribute wudgets colors
        for page in pages:
            layout = pages_dict[page].lay

            for i in range(layout.count()):
                widget = layout.itemAt(i).widget()
                if not isinstance(widget, controls.AttributeWidget):
                    continue

                if i%2:
                    widget.set_alternate_color()

                i += 1

        # set last row's stretch
        self.lay.addStretch(1)

        # set both window and highlighter updates enabled back
        self.setUpdatesEnabled(True)
        self._highlighter.set_enabled(True)

    def _update_theme_box(self, rule=None):
        """
        Populate "Themes" QComboBox with themes list from current rule, and set
        its current text to the appropriate theme (from preferences).
        """

        rule = self._rule if rule is None else rule
        themes = data.get_themes_list(rule)

        # temporary disabling controls update so currentTextChanged() won't
        # trigger controls to be re-created
        with updates_disabled(self):
            self._theme_box.clear()

            for t in themes:
                self._theme_box.addItem(t)

            # set current theme
            theme = self._prefs.get_by_names(rule, 'palette')
            self._theme_box.setCurrentText(theme)

    def _update_profile_box(self, rule=None):
        """
        Populate "Profiles" QComboBox with profiles list from current rule, and
        set its current text to the appropriate profile (from preferences).
        """

        rule = self._rule if rule is None else rule
        profiles = data.get_profiles_list(rule)

        # temporary disabling controls update so currentTextChanged() won't
        # trigger controls to be re-created
        with updates_disabled(self):
            self._profile_box.clear()

            for p in profiles:
                self._profile_box.addItem(p)

            # set current profile
            profile = self._prefs.get_by_names(rule, 'profile')
            self._profile_box.setCurrentText(profile)

    def _update(self, *args):
        """
        Update AttributeWidgets and text's highlight (through HighlightManager's
        update) if self.update_enabled.
        """

        if not self._update_enabled:
            return

        self._manager.set_enabled(False)

        theme = self._get_theme()
        profile = self._get_profile()

        self._manager.set_rule_theme(self._rule, theme)
        self._manager.set_rule_profile(self._rule, profile)

        self._manager.set_enabled(True)

    def get_settings(self):
        return self._get_theme(), self._get_profile()

    def closeEvent(self, event):
        """
        Qt re-implementation, check for modifications before close.
        """

        # in case window would have been closed but not delete yet
        if not self.isVisible():
            return

        is_dirty = False

        for attr in self._widgets_by_attr:
            if self._widgets_by_attr[attr].is_dirty():
                is_dirty = True
                break

        if is_dirty:
            user_answer = dialogs.save_before_close_prompt(self)

            # keep window opened
            if user_answer == dialogs.CANCEL:
                event.ignore()
                return

            if user_answer == dialogs.SAVE:
                self._save()

        event.accept()


class Lasso(QtWidgets.QWidget):
    """
    Links lasso widget.
    """

    link_clicked = QtCore.Signal(controls.AttributeWidget, object)

    def __init__(self, start_pos=QtCore.QPoint(), source_widget=None, parent=None):
        """
        Args:
            start_pos (QtCore.QPoint) : lasso's curve start point
            source_widget (controls.AttributeWidget) : link's destination widget
            parent (QtWidgets.QWidget, optional)

        Initiates self.
        """

        super(Lasso, self).__init__(parent)

        self._start_pos = start_pos
        self._end_pos = start_pos
        self._source_widget = source_widget
        self._parent = parent

        self._pen = QtGui.QPen(
            QtGui.QColor(190, 190, 190),
            2,
            QtCore.Qt.SolidLine,
            QtCore.Qt.RoundCap,
            QtCore.Qt.RoundJoin
        )

        self._under_mouse_widget = None

        # enable mouse tracking so mouseMoveEvent will be called and self will
        # be updated properly
        self.setMouseTracking(True)

    def mouseMoveEvent(self, event):
        """
        Qt re-implementation, update self and hovered AttributeWidgets.
        """

        # get AttributeWidget under mouse, passing through self overlay
        under_mouse = self._parent.get_under_mouse_attr_widget()

        if under_mouse != self._under_mouse_widget:
            # clear previously hovered AttributeWidget's highlight
            if not self._under_mouse_widget is None:
                self._under_mouse_widget.set_hovered(False)

            # set currently hovered AttributeWidget's highlight
            if not under_mouse is None and under_mouse != self._source_widget:
                under_mouse.set_hovered(True)

            self._under_mouse_widget = under_mouse

        if not under_mouse is None and under_mouse != self._source_widget:
            widget_pos = under_mouse.mapToGlobal(QtCore.QPoint(0, 0))
            widget_y = widget_pos.y() +under_mouse.height()/2
            end_pos = QtGui.QCursor().pos()

            self._end_pos = self.mapFromGlobal(QtCore.QPoint(end_pos.x(), widget_y))

        else:
            self._end_pos = self.mapFromGlobal(QtGui.QCursor().pos())

        self.update()

    def mousePressEvent(self, event):
        """
        Qt re-implementation, delete self and set AttributeWidgets link if some
        attribute has been "clicked" (actually it can't be clicked as self is
        overlaying it).
        """

        # clear previously hovered AttributeWidget's highlight
        if not self._under_mouse_widget is None:
            self._under_mouse_widget.set_hovered(False)

        if not self._under_mouse_widget is None:
            if event.button() == QtCore.Qt.LeftButton:
                if self._source_widget != self._under_mouse_widget:
                    # send link signal so AttributeWidgets will be linked and
                    # self be deleted
                    self.link_clicked.emit(self._source_widget, self._under_mouse_widget)
                    return

        # send no-link signal so self will be deleted
        self.link_clicked.emit(self._source_widget, None)

    def line_shape(self):
        """
        Returns:
            (QtGui.QPainterPath)

        Determines item's visual shape (used into paintEvent).
        """

        width = self._end_pos.x() -self._start_pos.x()
        path = QtGui.QPainterPath(self._start_pos)

        # no bezier on curve if striclty vertical or horizontal
        if not width or self._start_pos.y() == self._end_pos.y():
            path.lineTo(self._end_pos)
            return path

        # create bezier interpolation points
        cp1 = QtCore.QPoint(self._start_pos.x() +width/2.0, self._start_pos.y())
        cp2 = QtCore.QPoint(self._end_pos.x() -width/2.0, self._end_pos.y())

        path.cubicTo(cp1, cp2, self._end_pos)

        return path

    def paintEvent(self, event):
        """ Qt re-implementation, paints self. """

        painter = QtGui.QPainter(self)

        painter.setRenderHint(painter.Antialiasing)
        painter.setPen(self._pen)

        painter.drawPath(self.line_shape())


def run(rule, pos=None, parent=None, prefs=None):
    """
    Args:
        rule (str)
        pos (QtCore.QPoint, optional)
        parent (QtWidgets.QWidget, optional)

    Run "Palette Editor" window and set parent and position is set.
    """

    utils.close_existing(parent, WINDOW_OBJECT_NAME)

    dialog = PaletteEditor(rule, parent=parent)

    if pos:
        window_size = dialog.size()
        centered_pos = (
            pos.x() -window_size.width()/2,
            pos.y() -window_size.height()/2,
        )

        dialog.move(*centered_pos)

    if dialog.exec_():
        return dialog.get_settings()
    else:
        return None, None
