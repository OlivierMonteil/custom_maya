import os

from PySide2 import QtWidgets, QtGui

from custom_maya import constants as kk


class Frame(QtWidgets.QWidget):
    """
    Collapsible frame with title for attribute editor sections.
    """

    def __init__(self, title, parent, indent=0):
        super(Frame, self).__init__(parent)

        lay = QtWidgets.QVBoxLayout(self)
        self._widget = QtWidgets.QWidget(self)
        self.lay = QtWidgets.QVBoxLayout(self._widget)

        self._button = FrameButton(title, indent, self)

        lay.addWidget(self._button)
        lay.addWidget(self._widget)

        # few size rules
        for w in (self, lay, self.lay, self._widget):
            w.setContentsMargins(0, 0, 0, 0)

        self._button.setFixedHeight(20)

        # connect signal
        self._button.clicked.connect(self.toggle_frame)

    def toggle_frame(self):
        """ "Toggle" frame by showing/hiding its main widget. """

        if self._widget.isVisible():
            self.collapse()
        else:
            self.expand()

    def collapse(self):
        """ "Collapse" frame by hiding its main widget. """

        self._widget.setVisible(False)
        self._button.set_expanded(False)

    def expand(self):
        """ "Expand" frame by showing its main widget. """

        self._widget.setVisible(True)
        self._button.set_expanded(True)

    def button(self):
        """
        Returns:
            (FrameButton)

        Get frame's button instance.
        """

        return self._button


class FrameButton(QtWidgets.QPushButton):
    """
    Expand/collapse flat button with title and expand/collapse arrow.
    """

    def __init__(self, title, indent, parent=None):
        super(FrameButton, self).__init__(parent)

        self._expanded = True

        self._title = title

        self.setContentsMargins(indent*10, 0, 0, 0)

        self._expandedIcon = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'arrow_down.png'))
        self._collapsedIcon = QtGui.QPixmap(os.path.join(kk.ICONS_ROOT, 'arrow_right.png'))

        self._background = self._get_background_color()

    def level(self):
        """
        Returns:
            (int)

        Get the indentation level for self (in case of nested frames).
        """

        return self._level

    def set_expanded(self, expanded):
        """
        Args:
            expanded (bool)
        """

        self._expanded = expanded

    def is_expanded(self):
        """
        Returns:
            (bool)
        """

        return self._expanded

    def _get_background_color(self):
        """
        Returns:
            (QtGui.QColor)

        Get background's color depending on the level of the frame.
        """

        color = max(110 -15*self._level, 80)
        return QtGui.QColor(color, color, color)

    def paintEvent(self, event):
        """ Qt re-implementation, paints the widget itself. """

        rect = event.rect()
        painter = QtGui.QPainter(self)

        # paint flat background
        painter.fillRect(rect, self._background)

        pixmap = self._expandedIcon if self._expanded else self._collapsedIcon
        rect.setLeft(rect.left() +4 +kk.LEVEL_INDENT*(self._level -1))

        painter.setPen(QtCore.Qt.black)

        # paint arrow
        painter.drawPixmap(rect.left(), rect.height()/2-6, 12, 12, pixmap)
        # paint text
        painter.drawText(rect.left() +16, rect.height()/2 +4, self._title)
