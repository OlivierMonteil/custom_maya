import os
import json
import copy

from PySide2 import QtCore

from custom_maya import constants as kk


TEMPLATE = os.path.join(os.path.dirname(__file__), '.template')
USER_ROOT = os.path.join(
    os.path.expanduser(os.getenv('USERPROFILE')), '.config', 'custom-maya.json'
)
USER_PREFS = os.path.join(USER_ROOT, '.prefs')


def corrected_args(name, edit_func):
    def inner(func):
        def wrap(self, *args, **kwargs):
            if name in kwargs:
                kwargs[name] = edit_func(name) if name is not None else None
            elif args:
                as_list = list(args)
                args = tuple([edit_func(as_list[0])] +as_list[1:])
            return func(self, *args, **kwargs)

        return wrap
    return inner

def to_snake_case(name):
    if name is None:
        return
    return name.lower().replace('-', '_')

def to_parent(rule):
    if '|' in rule and 'mel' in rule:
        rule = 'mel'
    elif '|' in rule and 'python' in rule:
        rule = 'python'
    return rule

clean_name = corrected_args('name', to_snake_case)


class Attribute(object):
    @clean_name
    def __init__(self, name, data=None, parent=None):

        self._name = name
        self._data = data
        self._parent = parent
        self._attributes = []

        if isinstance(data, dict):
            for x in data:
                self.add_attr(x, data)

    def __str__(self):
        return format_attr(self)

    def __call__(self):
        return self.get()

    def name(self):
        return self._name

    def add_attr(self, name, data):
        attr_name = to_snake_case(name)
        self.__dict__[attr_name] = Attribute(name, data[name], self)
        self._attributes.append(name)

    @clean_name
    def get(self, name=None):
        if name and name in self.__dict__:
            return self.__dict__[name]
        return copy.deepcopy(self._data)

    def getattr(self, name):
        return self.__dict__[to_snake_case(name)]

    def parent(self):
        return self._parent

    def get_attributes(self):
        return [self.__dict__[to_snake_case(x)] for x in self._attributes]

    def get_attribute_names(self):
        return self._attributes

    def __contains__(self, obj):
        if isinstance(obj, Attribute):
            return obj in self.get_attributes()
        return obj in self.get_attribute_names()

    def __getitem__(self, item):
        return self.getattr(item)

    def __iter__(self):
        return self.get_attributes().__iter__()


class Preferences(QtCore.QObject, Attribute):

    changed = QtCore.Signal()
    _instance = None

    def __init__(self, name='preferences', parent=None):
        QtCore.QObject.__init__(self, parent)
        Attribute.__init__(self, name, parent)

        prefs_data = self.read()
        for x in prefs_data:
            self.add_attr(x, prefs_data)

    def read(self):
        prefs_data = read_json(USER_PREFS)

        prefs_files = get_prefs_files()
        prefs_files = [x.replace('\\', '/') for x in prefs_files]

        for path in prefs_files:
            tokens = get_tokens_from_path(path)
            if any(x == '.template' for x in tokens):
                continue

            content = read_json(path)
            content['control-icons'] = get_button_icons_from_prefs(path)
            content = create_prefs_dict(tokens, content)

            add_missing_to_dict(content, prefs_data)

        return prefs_data


def get_tokens_from_path(path):
    path = path.replace('\\', '/')
    tokens = path.split(kk.PACKAGE_ROOT.replace('\\', '/'))[-1].split('/')[1:-1]
    return [x.replace('_', '-') for x in tokens]

def read_json(path):
    content = {}
    try:
        with open(path, 'r') as opened_file:
            content = json.load(opened_file)
    finally:
        return content

def format_attr(obj, indent=0):
    lines = []
    attributes = obj.get_attributes()
    if attributes:
        lines.append('{}|- {}'.format(' '*indent, obj.name()))
        for attr in attributes:
            lines.append(format_attr(attr, indent=indent +4))
    else:
        lines.append(
            '{}|- {}: {}'.format(' '*indent, obj.name(), repr(obj()))
        )

    return '\n'.join(lines)

def get_button_icons_from_prefs(path):
    root = os.path.dirname(path)
    icons_root = os.path.join(root, '.icons')

    if not os.path.isdir(icons_root):
        return None

    found = []
    targets = ('button', 'button-off')

    for icon in os.listdir(icons_root):
        if any(icon.split('.')[0] == x for x in targets):
            found.append(os.path.join(icons_root, icon).replace('\\', '/'))

    if found:
        return sorted(found, key=lambda x: -len(x))

def get_prefs_files(root=kk.PACKAGE_ROOT):
    files = []

    for x in os.listdir(root) or ():
        path = os.path.join(root, x)

        if os.path.isfile(path):
            if x == '.prefs':
                files.append(path)
        else:
            files.extend(get_prefs_files(path))

    return files

def add_missing_to_dict(missing, tgt_dict):
    if not missing:
        return

    if isinstance(missing, (list, tuple)):
        if not missing[0] in tgt_dict:
            tgt_dict[missing[0]] = {}
        add_missing_to_dict(missing[1:], tgt_dict[missing[0]])

    if isinstance(missing, dict):
        for key in missing:
            if key not in tgt_dict:
                tgt_dict[key] = missing[key]

            else:
                if isinstance(missing[key], dict):
                    add_missing_to_dict(missing[key], tgt_dict[key])

def create_prefs_dict(tokens, prefs_dict):
    to_dict = {}
    current = to_dict

    for tok in tokens[:-1]:
        current[tok] = {}
        current = current[tok]
    current[tokens[-1]] = prefs_dict

    return to_dict
