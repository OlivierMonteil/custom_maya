from custom_maya.prefs.data import Preferences


def get():
    if Preferences._instance is None:
        Preferences._instance = Preferences()
    return Preferences._instance

def get_local_prefs(obj):
    modules = obj.__module__.split('.')[1:-1]

    preferences = get()
    for name in modules:
        preferences = preferences.getattr(name)
    return preferences
