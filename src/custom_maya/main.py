from PySide2 import QtCore, QtWidgets

from maya import cmds
from maya import mel

from custom_maya import utils
from custom_maya import constants as kk
from custom_maya import logger
from custom_maya import features
from custom_maya.tools.dir_navigator import main as dir_nav

log = logger.get(level='verbose')


@utils.traceback_on_all
class UiWatcher(QtCore.QObject):
    """
    Custom eventFilter installed on Maya's main window that will set all
    features on child added (evalDeferred).
    """

    _instance = None

    def __init__(self, *args, **kwargs):
        super(UiWatcher, self).__init__(*args, **kwargs)

        self._triggered = False
        self._children = []
        log.verbose('UIWatcher initialized.')

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.ChildAdded:
            child = event.child()

            if child.isWidgetType():
                # prevent deferred actions on already deleted widgets, as QMessageBoxes
                if isinstance(child, QtWidgets.QDialog):
                    return False

                if not self._triggered:
                    self._triggered = True
                    self._children.append(child)
                    cmds.evalDeferred(self.run, lp=True)

        return False

    def run(self):
        customize_maya(self._children)
        set_customize_on_tab_change()

        self._triggered = False
        self._children = []


@utils.catch_traceback
def set_customize_on_tab_change():
    """
    Connect ScriptEditor > QTabWidget.currentChanged signal to customize_script_editor().
    Used to customize new created tabs, as this signal is called after each tab
    creation.
    """

    tabs_lay = utils.get_script_editor().tabs_lay_widget
    if not tabs_lay:
        return

    for child in tabs_lay.children():
        if isinstance(child, QtWidgets.QTabWidget):
            if not hasattr(child, 'hasConnectionWithCustomizeMaya'):
                child.hasConnectionWithCustomizeMaya = True
                child.currentChanged.connect(customize_maya)
                break

@utils.catch_traceback
def customize_menu(menu):
    """
    Args:
        menu (str)

    Customize popupMenu on -postMenuCommand.
    """

    # run Maya's origin command (this command may remove custom menus that were
    # previously added).
    mel.eval("verifyCommandPopupMenus;")

    # add "Custom Menu" menu if not already in
    if not kk.CUSTOM_MENU_NAME in cmds.popupMenu(menu, q=True, itemArray=True):
        cmds.menuItem(
            kk.CUSTOM_MENU_NAME,
            p=menu,
            subMenu=True,
            radialPosition="NE",
            label=kk.CUSTOM_MENU_LABEL
        )

    # make sure menuItem existence checks are using a long name
    custom_menu = '{}|{}'.format(menu, kk.CUSTOM_MENU_NAME)

    # add "Python dir() Navigator" menu if not already in
    name = 'PythonDirNavigator'
    longname = '{}|{}'.format(custom_menu, name)
    if not cmds.menuItem(longname, q=True, exists=True):
        cmds.menuItem(
        name,
        p=custom_menu,
        radialPosition="W",
        label='Python dir() Navigator',
        command=run_dir_navigator
    )

    # add "Edit palettes..." menu if not already in
    longname = '{}|{}'.format(custom_menu, kk.PREFERENCES_MENU_NAME)
    if not cmds.menuItem(longname, q=True, exists=True):
        cmds.menuItem(
            kk.PREFERENCES_MENU_NAME,
            p=custom_menu,
            radialPosition="E",
            label=kk.PREFERENCES_MENU_LABEL,
            command=run_prefs_editor
        )
    
    cmds.popupMenu(menu, q=True, itemArray=True)

def run_dir_navigator(menu):
    """ Run "Script Tools" menu. """
    dir_nav.run()

def run_prefs_editor(menu):
    """ Run "Edit palette..." menu. """
    log.info("'run_prefs_editor' is not implemented yet")

    # pos = QtGui.QCursor().pos()
    # prefs_editor.run(pos)

@utils.silent_errors
def edit_line_numbers_controls():
    """
    Disable Maya's "Show line numbers" controls, as this tool has its own
    line-numbers widget implementation.
    """

    # disable script editor toolbar's icon button
    try:
        cmds.iconTextButton(
            'showLineNumbersButton', e=True, enable=False, image="showLineNumbersOff.png"
        )
    except:
        cmds.iconTextCheckBox(
            'showLineNumbersButton', e=True, enable=False, v=False
        )

def set_popup_menus_customizable():
    """
    Edit all Script Editor popupMenu's -postMenuCommand so customize_menu()
    will be called once the menu is created.
    """

    script_editor_popup_menus = utils.get_script_editor().popup_menus()

    for popup_menu in script_editor_popup_menus:
        if not cmds.popupMenu(popup_menu, q=True, exists=True):
            continue

        # lambda function seems to fail, so we will set postMenuCommand as a string
        cmd  = 'from custom_maya.main import customize_menu;'
        cmd += 'customize_menu("{}");'.format(popup_menu)

        cmds.popupMenu(popup_menu, e=True, postMenuCommand=cmd)

def edit_line_numbers_cmd():
    """
    Args:
        script_editor_is_opened (bool)

    Overwrite Maya's "Show line numbers" command, as this tool has its own
    line-numbers widget implementation.
    """

    if cmds.optionVar(q='commandExecuterShowLineNumbers'):
        mel.eval('toggleCommandExecuterShowLineNumbers();')
        # edit Maya's variables so Maya line-numbers won't be shown
        cmds.optionVar(iv=('commandExecuterShowLineNumbers', False))

    mel_proc = '''
    global proc toggleCommandExecuterShowLineNumbers(){
        global string $showLineNumbersMenuItemSuffix;
        string $menus[] = `lsUI -menuItems`;

        for ($menu in $menus) {
            if (`match $showLineNumbersMenuItemSuffix $menu` != ""){
                // set "Show line numbers" menu item un-checked and disabled
                menuItem -e -enable false $menu;
                menuItem -e -checkBox false $menu;
            }
        }
        // disable also script editor toolbar's icon button
        iconTextButton -e -enable false -image "showLineNumbersOff.png" showLineNumbersButton;

        print("// Info : \\"Show line numbers\\" is disabled by Custom Script Editor.\\n");
    }
    '''

    mel.eval(mel_proc)

@utils.catch_traceback
# @utils.dump_stats
def customize_maya(*args):
    """
    Iterate every tab from Script Editor and apply all features if necessary.
    """
    widgets_list = args[0] if args and not isinstance(args[0], int) else None
    with utils.get_maya_ui_singletons():
        edit_line_numbers_controls()
        features_manager = features.get_manager()
        features_manager.apply(widgets_list)
        set_popup_menus_customizable()

@utils.catch_traceback
def run():
    """
    (called by maya's userSetup.mel/py)

    Install event filter on Maya's main window to automate Maya customization.
    """

    # skip if maya is in batch mode
    if cmds.about(batch=True):
        return

    edit_line_numbers_cmd()

    # install ScriptEditorDetector event filter on Maya window if not already
    maya_ui_qt = utils.get_maya_window()
    if utils.obj_needs(maya_ui_qt, UiWatcher):
        ui_filter = utils.get_singleton(UiWatcher)
        maya_ui_qt.installEventFilter(ui_filter)

    # customize Script Editor if already opened and connect
    # ScriptEditor > QTabWidget.currentChanged signal to customize_script_editor.
    # This will allow to customize all new created tabs, because this signal is
    # called after each tab creation when the Script Editor automatically focuses
    # on the new tab.

    cmds.evalDeferred(customize_maya, lp=True)
    cmds.evalDeferred(set_customize_on_tab_change, lp=True)

    log.success('Custom Maya successfully actived.')
