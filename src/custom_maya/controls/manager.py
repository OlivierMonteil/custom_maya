from PySide2 import QtCore

from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.controls import ControlsHandler



class ControlsManager(Manager):
    _handler_class = ControlsHandler
    _instance = None
    _name = 'controls'

    def apply(self, *args):
        handler = super(ControlsManager, self).apply(*args)
        if handler:
            self.add_control = handler.add_control
        return handler

    def target_widget(self):
        return utils.get_script_editor().console_txt_edit

    def is_enabled(self, *args):
        return True


def get_manager():
    return utils.get_singleton(ControlsManager)
