"""
The topLevelWidgets parented to the Script Editor's console.
"""

from PySide2 import QtWidgets, QtGui, QtCore

from custom_maya import utils
from custom_maya import constants as kk


@utils.traceback_on_all
class IconButton(QtWidgets.QPushButton):

    _style = """QPushButton {
        border:none;
        background-color: rgba(255, 255, 255,0);
    }"""

    def __init__(self, icon, *args, **kwargs):
        super(IconButton, self).__init__('', *args, **kwargs)

        self._name = icon.replace('\\', '/').split('/.icons')[0].split('/')[-1].replace('_', '-')

        self.setStyleSheet(self._style)

        pixmap  = QtGui.QPixmap(icon).scaledToWidth(
            kk.CONTROLS_SIZE, QtCore.Qt.SmoothTransformation
        )
        icon = QtGui.QIcon(pixmap)
        self.setIcon(icon)

        self.setFixedSize(pixmap.size())
        self.setIconSize(pixmap.size())


@utils.traceback_on_all
class ToggleButton(IconButton):

    _tooltips = [
        'enable "{}"',
        'disable "{}"',
    ]

    state_changed = QtCore.Signal(bool)

    def __init__(self, icons, state, *args, **kwargs):
        super(ToggleButton, self).__init__(icons[0], *args, **kwargs)

        self._state = state

        pixmaps = [
            QtGui.QPixmap(x).scaledToWidth(
                kk.CONTROLS_SIZE, QtCore.Qt.SmoothTransformation
            ) for x in icons
        ]

        self._icons = [QtGui.QIcon(pix) for pix in pixmaps]

        if not 'toolTip' in kwargs:
            self.set_tooltip()
        self.update_icon()
        self.clicked.connect(self._toggle)

    def update_icon(self):
        self.setIcon(self._icons[self._state])

    def set_tooltip(self):
        self.setToolTip(self._tooltips[self._state].format(self._name))

    def _toggle(self):
        """ Toggle button's pixmap and tooltip. """

        self._state = not self._state
        self.set_tooltip()
        self.update_icon()
        self.repaint()

        self.state_changed.emit(self._state)

    def state(self):
        return self._state
