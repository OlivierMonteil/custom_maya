import os
import json

from PySide2 import QtWidgets

from custom_maya import constants as kk
from custom_maya import utils
from custom_maya import prefs
from custom_maya.controls import widgets


@utils.traceback_on_all
class ControlsHandler(QtWidgets.QWidget):

    _padding = 5

    def __init__(self, parent=None):
        super(ControlsHandler, self).__init__(parent)


        self._txt_edit = parent
        self._splitter = parent.parent()

        self._ctrls = []

        lay = QtWidgets.QVBoxLayout(self)

        self.toolbar_button = widgets.ToggleButton(
            [
            os.path.join(kk.ICONS_ROOT, 'toolbar_off.png'),
                os.path.join(kk.ICONS_ROOT, 'toolbar.png')
            ],
            True,
            parent=self,
            toolTip=None
        )

        self._bar = QtWidgets.QWidget(self)
        self._bar_lay = QtWidgets.QVBoxLayout(self._bar)

        lay.addWidget(self.toolbar_button)
        lay.addWidget(self._bar)
        self._ctrls.append(self.toolbar_button)

        for widget in (self, lay, self._bar, self._bar_lay):
            widget.setContentsMargins(0, 0, 0, 0)
        for layout in (lay, self._bar_lay):
            layout.setSpacing(0)

        self.toolbar_button.clicked.connect(self.toggle_bar)

    def install(self):
        utils.get_script_editor().widget.installEventFilter(self)
        self._txt_edit.verticalScrollBar().installEventFilter(self)

        self._stay_in_right_corner()
        self.show()

    def _ctrls_height(self):
        return self._ctrls[0].height(), sum([x.height() for x in self._ctrls])

    def eventFilter(self, obj, event):
        """
        Qt re-implementation, maintain the button in the right corner of the
        console on Script Editor's resizeEvent.
        """

        if isinstance(obj, QtWidgets.QScrollBar):
            if event.type() in (event.Hide, event.Show):
                self._stay_in_right_corner()

        elif event.type() == event.Resize:
            self._stay_in_right_corner()
        return False

    def add_control(self, icons, handler, state):
        if not isinstance(icons, (list, tuple)):
            icons = [icons]

        ctrl_name = icons[0].split('/.icons')[0].split('/')[-1] +'_button'
        if ctrl_name in self.__dict__:
            ctrl = self.__dict__[ctrl_name]

        else:
            if len(icons) == 2:
                ctrl = widgets.ToggleButton(icons, state, self)
            else:
                ctrl = widgets.IconButton(icons[0], self)

            self._bar_lay.addWidget(ctrl)
            self.__dict__[ctrl_name] = ctrl
            self._ctrls.append(ctrl)

        if len(icons) == 2:
            ctrl.state_changed.connect(handler.on_ctrl_signal)
        else:
            ctrl.clicked.connect(handler.on_ctrl_signal)

        self.update_size()


    def _stay_in_right_corner(self):
        vbar = self._txt_edit.verticalScrollBar()
        vbar_offset = vbar.width() if vbar.isVisible() else 0

        self.move(
            self._splitter.width() -vbar_offset -kk.CONTROLS_SIZE -self._padding,
            self._padding
        )

    def update_size(self):
        self.resize(kk.CONTROLS_SIZE, self._ctrls_height()[self._bar.isVisible()])

    def toggle_bar(self):
        state = self.toolbar_button.state()
        self._bar.setVisible(state)
        self.update_size()
