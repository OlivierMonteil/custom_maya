import sys
import traceback
import types
import cProfile
import pstats
from functools import wraps
try:
    from StringIO import StringIO   # for Python 2
except ImportError:
    from io import StringIO         # for Python 3

from PySide2 import QtWidgets, QtGui, QtCore

from custom_maya import logger

log = logger.get()



class mouseToBusyState(object):
    def __enter__(self):
        QtWidgets.QApplication.instance().setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))

    def __exit__(self, *args):
        QtWidgets.QApplication.instance().restoreOverrideCursor()

def busy(func):
    """ Decorator that sets mouse as busy during <func> execution. """
    def wrap(self, *args, **kwargs):
        with mouseToBusyState():
            result = func(self, *args, **kwargs)
            return result
    return wrap

def catch_traceback(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            exc_typ, exc_val, exc_tb = sys.exc_info()
            log.error(str(e) +' (from %s)' % func.__name__, error_type=exc_typ.__name__)
            log.traceback(traceback.format_exception(exc_typ, exc_val, exc_tb))
    return wrapper

def traceback_on_all(cls):
    for attr in cls.__dict__:
        if isinstance(getattr(cls, attr), types.FunctionType):
            setattr(cls, attr, catch_traceback(getattr(cls, attr)))
    return cls

def silent_errors(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except :
            pass
    return wrapper

def to_log_file(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        log.write_into_file('Calling \'%s\'...' % func.__name__)
        res = func(*args, **kwargs)
        log.write_into_file('   \'%s\' returns : %s' % (func.__name__, repr(res)))
        return res
    return wrapper

def dump_stats(func):
    """ Decorator for cProfile stats print. """
    @wraps(func)

    def wrapper(*args, **kwargs):
        try:
            with profiling():
                result = func(*args, **kwargs)
            return result
        except Exception as e:
            log.error(str(e) +' (from %s)' % func.__name__)
    return wrapper


class profiling(object):
    """
    Context manager for cProfile stats print.
    """

    def __init__(self):
        self._profiler = cProfile.Profile()

    def __enter__(self, *args):
        """ Enable cProfile. """
        self._profiler.enable()

    def __exit__(self, *args):
        """ Disable cProfile and print stats. """
        self._profiler.disable()

        s = io.StringIO()
        sort_by = 'cumulative'
        ps = pstats.Stats(self._profiler, stream=s).sort_stats(sort_by)
        ps.print_stats()

        print(s.getvalue())


