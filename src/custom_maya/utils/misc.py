import sys
import time

def chrono(func):
    def inner(*args, **kwargs):
        start_time = time.time()
        res = func(*args, **kwargs)
        print('# %s evaluated in %ss' % (
            func.__name__, round(time.time() -start_time, 3)
        ))
        return res
    return inner

def to_snake_case(name):
    if name is None:
        return
    return name.lower().replace('-', '_')

def ui_short_name(name):
    return name.split('|')[-1]

def same_len_arrays(a, b):
    """
    Return True if both <a> and <b> are list or tuple and have the same length.
    """

    if not all(isinstance(x, (tuple, list)) for x in (a, b)):
        return False
    if not len(a) == len(b):
        return False
    return True

def is_same_data(value, default, tolerance=4, is_enum=False):
    """
    Args:
        value (object)
        default (object)
        tolerance (int, optional)
        is_enum (bool, optional)

    Returns:
        (bool)

    (kind-of a non-strict "==" re-implementation)
    Determine whether <value> and <default> are similar, by rounding float values,
    and by recursing on tuples and list, to make sure that (0.0, 0.0, 0.0) will be
    considered as similar to [0.0, 0.0, 0.0])
    """

    if is_enum and isinstance(default, list):
        default = default[0]

    if all(isinstance(x, float) for x in (value, default)):
        value = round(value, tolerance)
        default = round(default, tolerance)

    isDefault = value == default
    if isDefault:
        return True

    # recurse on tuple and lists with the same length
    if same_len_arrays(value, default):
        if all(is_same_data(value[i], default[i]) for i, _ in enumerate(value)):
            isDefault = True

    return isDefault

def to_simple_dimension(value):
    """
    Get input value with the less array-nesting as possible.
    For instance:
      - [[0., 0., 0.]] will become [0., 0., 0.]
      - [True, [0., 0., 0.]] will stay put
    """

    if not isinstance(value, (list, tuple)):
        return value

    if len(value) > 1 and all(not isinstance(x, (list, tuple)) for x in value):
        return value

    if any(isinstance(x, (list, tuple)) for x in value) and len(value) > 1:
        return value

    value = value[0]
    if isinstance(value, (list, tuple)):
        return to_simple_dimension(value)
    return value

def is_python_3():
    return int(sys.version[0]) >= 3