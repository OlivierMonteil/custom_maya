import re

from PySide2 import QtWidgets, QtGui
import shiboken2 as shiboken

from maya.OpenMayaUI import MQtUtil
from maya import cmds, mel

from custom_maya import constants as kk
from custom_maya.overlay import OverlayWidget


class updates_disabled(object):
    def __init__(self, widget):
        self._widget = widget

    def __enter__(self, *args):
        self._widget.setUpdatesEnabled(False)

    def __exit__(self, *args):
        self._widget.setUpdatesEnabled(True)


class ExpressionEditor(object):
    def __init__(self, *args):

        self.widget = get_window_by_title('Expression Editor')
        self.txt_edit = get_txt_edits(self.widget)[0] if self.widget else None


class AttributeEditor(object):
    def __init__(self, *args):

        self.widget = get_qt_instance('AttributeEditor')


class ScriptEditor(object):
    def __init__(self, *args):

        self.widget = get_window_by_title('Script Editor')
        self.tabs_lay = None
        self.tabs_lay_widget = None
        self.tabs = []
        self.selected_tab = None
        self.console_txt_edit = None
        self.quick_help_widget = None

        self.parse_widget()

    def parse_widget(self):
        if not self.widget:
            return

        self.console_txt_edit = self.get_console_txt_edit()

        self.get_tabs_lay()
        if not self.tabs_lay:
            return
        self.tabs = self.get_tabs()
        self.selected_tab = self.get_selected_tab()
        self.quick_help_widget = self.get_quick_help_widget()

    def popup_menus(self):
        return mel.eval("$CSE_cmdPopupMenus = $gCommandPopupMenus;")

    def get_tab_layout_names(self):
        panels = cmds.lsUI(panels=True)
        script_editor = None
        for p in panels:
            if 'scriptEditor' in p:
                script_editor = p
                break

        return [
            lay for lay in cmds.lsUI(type='tabLayout')
            if script_editor in cmds.layout(lay, q=True, p=True)
        ]

    def get_tabs_lay(self, as_widget=False):
        for lay in self.get_tab_layout_names() or ():
            tab_names = cmds.tabLayout(lay, q=True, tabLabel=True)

            if all(is_valid_tab_name(x) for x in tab_names):
                self.tabs_lay_widget = get_qt_instance(lay)
                self.tabs_lay = lay
                return

    def get_tabs(self):
        labels = cmds.tabLayout(self.tabs_lay, q=True, tabLabel=True)
        tabs = cmds.tabLayout(self.tabs_lay, q=True, childArray=True)
        # 1-based index is returned
        sel_idx = cmds.tabLayout(self.tabs_lay, q=True, selectTabIndex=True) -1

        tab_instances = []

        for i, form_layout in enumerate(tabs) or ():
            ptr = MQtUtil.findControl(form_layout)
            widget = shiboken.wrapInstance(int(ptr), QtWidgets.QWidget)

            new_tab = ScriptTab()
            new_tab.label = labels[i]
            new_tab.form_layout = form_layout
            new_tab.widget = widget

            for txt_edit in get_txt_edits(widget):
                if 'cmdScrollFieldExecuter' in txt_edit.objectName():
                    new_tab.txt_edit = txt_edit
                    break

            new_tab.language = 'python' if is_valid_tab_name(labels[i], exlude_mel=True) else 'mel'

            tab_instances.append(new_tab)

        if sel_idx < len(tabs):
            tab_instances[sel_idx].selected = True

        return tab_instances

    def get_selected_tab(self):
        for tab in self.tabs or ():
            if tab.is_selected():
                return tab

    def get_quick_help_widget(self):
        """ Returns: (QtWidgets.QListWidget) """

        for lay in self.get_tab_layout_names() or ():
            tab_names = cmds.tabLayout(lay, q=True, tabLabel=True)

            if tab_names and tab_names[0] == 'Quick Help':
                form_lay = cmds.tabLayout(lay, q=True, childArray=True)[0]
                ptr = MQtUtil.findControl(form_lay)
                widget = shiboken.wrapInstance(int(ptr), QtWidgets.QWidget)

                if not hasattr(widget, 'children'):
                    continue

                for child in widget.children():
                    if isinstance(child, QtWidgets.QListWidget):
                        return child

    def get_console_txt_edit(self):
        for txt_edit in get_txt_edits(self.widget):
            if 'cmdScrollFieldReporter' in txt_edit.objectName():
                return txt_edit

    def children(self):
        if not hasattr(self.widget, 'children'):
            return []
        return self.widget.children()


class get_maya_ui_singletons(object):

    _cls_list = [ScriptEditor, ExpressionEditor, AttributeEditor]

    def __enter__(self, *args):
        for cls in self._cls_list:
            cls._instance = cls()

    def __exit__(self, *args):
        for cls in self._cls_list:
            cls._instance = None


class ScriptTab(object):
    def __init__(self):

        self.label = None
        self.form_layout = None
        self.widget = None
        self.txt_edit = None
        self.language = None
        self.selected = False

    def is_selected(self):
        return self.selected

    def get_txt_edit(self):
        if self.widget:
            return get_txt_edits(self.widget)

    def children(self):
        if not hasattr(self.widget, 'children'):
            return []
        return self.widget.children()


def get_qt_instance(maya_name):
    '''
    Given the name of a Maya UI element of any type,
    return the corresponding QWidget or QAction.
    If the object does not exist, returns None
    '''
    ptr = MQtUtil.findControl(maya_name)
    if ptr is None:
        ptr = MQtUtil.findLayout(maya_name)
    if ptr is None:
        ptr = MQtUtil.findMenuItem(maya_name)
    if ptr:
        return shiboken.wrapInstance(int(ptr), QtWidgets.QMainWindow)

def get_maya_window():
    maya_ui = MQtUtil.mainWindow()
    return shiboken.wrapInstance(int(maya_ui), QtWidgets.QMainWindow)

def get_attribute_editor():
    return get_singleton(AttributeEditor)

def get_script_editor():
    return get_singleton(ScriptEditor)

def get_expression_txt_edit():
    expression_editor = get_singleton(ExpressionEditor)
    return expression_editor.txt_edit

def get_window_by_title(title):
    # inspect only visible QWidgets, as many QMenu, QAction or else might not be
    # deleted and parented to anything
    top_level_widgets = [
        x for x in QtWidgets.QApplication.instance().topLevelWidgets()
        if type(x) in [QtWidgets.QWidget] and x.isVisible()
    ]

    for widget in top_level_widgets:
        if widget.windowTitle() == title:
            return widget

def get_txt_edits(widget):
    """
    Args:
        widget (QWidget)

    Recursively get all children with QTextDocument as child.
    """

    if widget is None or not hasattr(widget, 'children'):
        return []

    found = []
    for child in widget.children() or ():
        if isinstance(child, QtGui.QTextDocument):
            found.append(widget.parent())
        # recurse
        else:
            found.extend(get_txt_edits(child))

    return found

def get_popup_menu_language(menu):
    """
    Args:
        menu (str)

    Returns:
        (str) : 'python' or 'mel'

    Get <menu>'s associated tab language.
    """

    tokens = []

    for x in menu.split('|'):
        tokens.append(x)
        if 'tabLayout' in x:
            break

    tab_layout = '|'.join(tokens)
    idx = cmds.tabLayout(tab_layout, q=True, selectTabIndex=True)
    tab_label = cmds.tabLayout(tab_layout, q=True, tabLabel=True)[idx-1]

    return 'python' if is_valid_tab_name(tab_label, exlude_mel=True) else 'mel'

def is_valid_tab_name(name, exlude_mel=False):
    """
    Args:
        name (str)
        exlude_mel (bool, optional)

    Returns:
        (bool)

    Check if tab's title is MEL (unless exlude_mel == True), Python (also check
    if titles matches *.py or *.mel).
    """

    tabs_regex = kk.VALID_TABS_REGEX[2:] if exlude_mel else kk.VALID_TABS_REGEX
    return True if any(re.match(regex, name) for regex in tabs_regex) else False

def get_all_instances(cls, parent=None):
    """
    Args:
        cls (class)
        parent (QtWidgets.QWidget, optional)

    Find all <cls> instances in <parent> QTextEdits children (if no parent is set,
    Script Editor window will be taken as parent).
    """

    if not parent:
        parent = get_script_editor()

    txt_edits = get_txt_edits(parent)
    found = []

    for txt_edit in txt_edits or ():
        for child in txt_edit.children() or ():
            if isinstance(child, cls):
                found.append(child)

    return found

def get_instance(cls, parent=None, create_if_missing=True):
    """
    Args:
        cls (class)
        parent (QtWidgets.QWidget, optional)
        create_if_missing (bool, optional)

    Get <cls> instance from <parent> children. If no instance is found and
    <create_if_missing> is True, create a new <cls> instance and return it.
    """

    if parent is None:
        parent = get_maya_window()

    for child in parent.children() or ():
        if isinstance(child, cls):
            return child

    new_instance = None

    if create_if_missing:
        new_instance = cls(parent=parent)

    return new_instance

def close_existing(parent, window_name):
    """
    Args:
        parent (QtWidgets.QWidget)

    Close existing window instance from <parent>'s children, looking for matching
	<window_name>.
    """

    for widget in parent.children() or ():
        if widget.objectName() == window_name:
            widget.setParent(None)
            widget.close()
            widget.deleteLater()
            del widget
            break

def obj_needs(widget, cls):
    """
    Args:
        widget (QWidget)
        cls (class or tuple(class))

    Returns:
        (bool)

    Check whether any of  <target_classes> is found in <widget>'s children or not.
    (used to detect if eventFilters, Highlighters, etc. are to be
    installed on widget)
    """
    for child in widget.children():
        if isinstance(child, cls):
            return False

    return True

def get_indent_level(block, ignore_white_lines=False, keep_whitespaces=False):
    """
    Args:
		block (QtGui.QTextBlock)
		ignore_white_lines (bool=True)

	Returns:
		(int)

	Get <block>'s indentation-level.
	"""

    text = block.text()
    if not text:
        if ignore_white_lines:
            return None
        return 0

    non_whitespace_chars = re.search('\S', text)

    if keep_whitespaces and not non_whitespace_chars:
        return len(text)

    if ignore_white_lines and not non_whitespace_chars:
        return None
    if not non_whitespace_chars:
        return 0

    return non_whitespace_chars.start(0)

def get_overlay(txt_edit, padding=True):
    """
    Args:
        txt_edit (QtWidgets.QTextEdit)

    Returns:
        (OverlayWidget)

    Create OverlayWidget on <txt_edit> if necessary and return it.
    """

    for child in txt_edit.children():
        if isinstance(child, OverlayWidget):
            return child

    overlay = OverlayWidget(txt_edit, padding)
    overlay.show()

    return overlay

def get_block_bounding_rect(txt_edit, block):
    """
    Args:
        txt_edit (QTextEdit or QPlainTextEdit)
        block (QTextBlock)

    Returns:
        (QtCore.QRect)
    """

    # QPlainTextEdit
    if isinstance(txt_edit, QtWidgets.QPlainTextEdit):
        rectf = txt_edit.blockBoundingGeometry(block)
        offset = txt_edit.contentOffset()
        return rectf.translated(offset).toAlignedRect()

    # QTextEdit
    lay = txt_edit.document().documentLayout()

    rect = lay.blockBoundingRect(block)
    rect = rect.translated(
        txt_edit.viewport().geometry().x(),
        txt_edit.viewport().geometry().y() - txt_edit.verticalScrollBar().sliderPosition()
    )

    return rect.toRect()

def get_first_visible_block(txt_edit):
    block = txt_edit.document().begin()
    view_rect = txt_edit.viewport().geometry()

    while block.isValid():
        blck_rect = get_block_bounding_rect(txt_edit, block)
        if view_rect.intersects(blck_rect):
            return block

        block = block.next()

    return txt_edit.document().begin()

def run_after_event(new_method, target_method):
    """
    Wrap both functions into a new one so <new_method> is called after
    <target_method>.

    Usage:
        target_method = run_after_event(new_method, target_method)
    """

    def run_both(*args, **kwargs):
        res = target_method(*args, **kwargs)
        new_method(*args, **kwargs)
        return res

    return run_both

def get_singleton(cls, parent=None):
    if not hasattr(cls, '_instance') or cls._instance is None:
        if parent is None:
            parent = get_maya_window()
        cls._instance = cls(parent)

    return cls._instance
