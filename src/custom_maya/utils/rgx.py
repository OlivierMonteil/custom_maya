import re


def if_enabled(func):
    """
    Simple decorator that will call the input method only if the object is set
    as enabled.
    """

    def wrap(self, *args, **kwargs):
        if not self.is_enabled():
            return None
        return func(self, *args, **kwargs)

    return wrap


class Regex(object):
    """
    Compiled regex handler.
    """

    def __init__(self, regex, enabled=True):
        """
        Args:
            regex (str)
            enabled (bool, optional)

        Initiates self.
        """

        super(Regex, self).__init__()

        self._enabled = enabled
        self._regex_str = regex
        self._regex = re.compile(regex)

    def __len__(self):
        return len(self._regex_str.replace('\\', ''))

    def __str__(self):
        return self._regex_str

    def to_string(self):
        return self._regex_str

    @if_enabled
    def finditer(self, *args, **kwargs):
        return self._regex.finditer(*args, **kwargs)

    @if_enabled
    def match(self, *args, **kwargs):
        return self._regex.match(*args, **kwargs)

    @if_enabled
    def search(self, *args, **kwargs):
        return self._regex.search(*args, **kwargs)

    def index_in(self, line, pos):
        """
        Args:
            line (str)
            pos (int)

        Get next match index in line, starting from <pos> (returns -1 if no match).
        """

        match = self.search(line[pos:])
        return -1 if match is None else match.start() +pos

    def is_enabled(self):
        return self._enabled

    def enable(self):
        self._enabled = True

    def disable(self):
        self._enabled = False
