"""
Scripts constant values.
"""

import os
import string

from PySide2 import QtCore

            #######################################################
            #                        General                      #
            #######################################################

PACKAGE_ROOT = os.path.dirname(__file__)
ICONS_ROOT = os.path.join(PACKAGE_ROOT, '.icons')
FEATURES_ROOT = os.path.join(PACKAGE_ROOT, 'features')
STYLESHEET_ROOT = os.path.join(PACKAGE_ROOT, 'stylesheet')

VALID_TABS_REGEX = ['(?i)mel', '[\w\-_]+(.mel)$', '(?i)python', '[\w\-_]+(.py)$']
CUSTOM_MENU_NAME = 'CustomMenu'
CUSTOM_MENU_LABEL = 'Custom Script Editor'
SNIPPETS_BOX_NAME = 'SnippetBox'
SNIPPETS_BOX_LABEL = 'Snippets'
WORD_WRAP_BOX_NAME = 'LineWrapBox'
WORD_WRAP_BOX_LABEL = 'Line-wrap'
CUSTOM_TOOLS_MENU_NAME = 'CustomToolsMenu'
CUSTOM_TOOLS_MENU_LABEL = 'Custom Tools'
PREFERENCES_MENU_NAME = 'PreferencesMenu'
PREFERENCES_MENU_LABEL = 'Preferences'

PALETTES_ROOT = os.path.join(FEATURES_ROOT, 'highlight', '.palettes')
PROFILES_DIR = '.profiles'

INF_HEIGHT = 2000
INF_WIDTH = 10000

PROFILE_NONE = 'None'
PROFILE_FULL = 'Full'
PROFILE_OFF = 'Off'

CONTROLS_SIZE = 28
LEFT_PADDING = 55

INDENT_LENGTH = 4

            #######################################################
            #                         Keys                        #
            #######################################################

MOVE_KEYS = [
    QtCore.Qt.Key_Down,
    QtCore.Qt.Key_Up,
    QtCore.Qt.Key_Left,
    QtCore.Qt.Key_Right,
    QtCore.Qt.Key_End,
    QtCore.Qt.Key_Home
]

DIGIT_KEYS = [
    QtCore.Qt.Key_0,
    QtCore.Qt.Key_1,
    QtCore.Qt.Key_2,
    QtCore.Qt.Key_3,
    QtCore.Qt.Key_4,
    QtCore.Qt.Key_5,
    QtCore.Qt.Key_6,
    QtCore.Qt.Key_7,
    QtCore.Qt.Key_8,
    QtCore.Qt.Key_9,
]

CHARACTERS = string.printable.split(' ')[0] +' '
# special chars
SPECIAL_CHARS = u'\x83\x9a\x9c\x9e\xaa\xb5\xba\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6'
SPECIAL_CHARS += u'\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5'
SPECIAL_CHARS += u'\xf6\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff\x8a\x8c\x8e\x9f\xc0\xc1'
SPECIAL_CHARS += u'\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0'
SPECIAL_CHARS += u'\xd1\xd2\xd3\xd4\xd5\xd6\xd8\xd9\xda\xdb\xdc\xdd\xde'

            #######################################################
            #                   Blocks Collapse                   #
            #######################################################

COLLAPSIBLE_PATTERNS = [
    'def ',
    'class ',
    'proc ',
    'global proc '
]

            #######################################################
            #                   Syntax Highlight                  #
            #######################################################

PYTHON_NUMBERS = ['None', 'True', 'False']
MEL_NUMBERS = ['none', 'true', 'false']

PYTHON_KEYWORDS = [
    'and',
    'as',
    'assert',
    'async',
    'await',
    'break',
    'class',
    'continue',
    'def',
    'del',
    'elif',
    'else',
    'except',
    'finally',
    'for',
    'from',
    'global',
    'if',
    'import',
    'in',
    'is',
    'lambda',
    'nonlocal',
    'not',
    'or',
    'pass',
    'raise',
    'return',
    'try',
    'while',
    'with',
    'yield'
]

MEL_KEYWORDS = [
    'break',
    'catch',
    'continue',
    'else if',
    'else',
    'for',
    'global',
    'if',
    'in',
    'proc',
    'return',
    'try',
    'while'
]

OPERATORS = [
    '=',
    '==',
    '!=',
    '<',
    '<=',
    '>',
    '>=',
    '\+',
    '-',
    '\*',
    '/',
    '//',
    '\%',
    '\*\*',
    '\+=',
    '-=',
    '\*=',
    '/=',
    '\%=',
    '\^',
    '\|',
    '\&',
    '\~',
    '>>',
    '<<'
]

PYTHON_BUILTINS = [
    'ArithmeticError',
    'AssertionError',
    'AttributeError',
    'BaseException',
    'BlockingIOError',
    'BrokenPipeError',
    'BufferError',
    'BytesWarning',
    'ChildProcessError',
    'ConnectionAbortedError',
    'ConnectionError',
    'ConnectionRefusedError',
    'ConnectionResetError',
    'DeprecationWarning',
    'EOFError',
    'Ellipsis',
    'EnvironmentError',
    'Exception',
    'FileExistsError',
    'FileNotFoundError',
    'FloatingPointError',
    'FutureWarning',
    'GeneratorExit',
    'IOError',
    'ImportError',
    'ImportWarning',
    'IndentationError',
    'IndexError',
    'InterruptedError',
    'IsADirectoryError',
    'KeyError',
    'KeyboardInterrupt',
    'LookupError',
    'MemoryError',
    'ModuleNotFoundError',
    'NameError',
    'NotADirectoryError',
    'NotImplemented',
    'NotImplementedError',
    'OSError',
    'OverflowError',
    'PendingDeprecationWarning',
    'PermissionError',
    'ProcessLookupError',
    'RecursionError',
    'ReferenceError',
    'ResourceWarning',
    'RuntimeError',
    'RuntimeWarning',
    'StopAsyncIteration',
    'StopIteration',
    'SyntaxError',
    'SyntaxWarning',
    'SystemError',
    'SystemExit',
    'TabError',
    'TimeoutError',
    'TypeError',
    'UnboundLocalError',
    'UnicodeDecodeError',
    'UnicodeEncodeError',
    'UnicodeError',
    'UnicodeTranslateError',
    'UnicodeWarning',
    'UserWarning',
    'ValueError',
    'Warning',
    'WindowsError',
    'ZeroDivisionError',
    # '__build_class__',
    # '__debug__',
    # '__doc__',
    # '__import__',
    # '__loader__',
    # '__name__',
    # '__package__',
    # '__spec__',
    'abs',
    'all',
    'any',
    'ascii',
    'bin',
    'bool',
    'breakpoint',
    'bytearray',
    'bytes',
    'callable',
    'chr',
    'classmethod',
    'compile',
    'complex',
    'copyright',
    'credits',
    'delattr',
    'dict',
    'dir',
    'divmod',
    'enumerate',
    'eval',
    'exec',
    'exit',
    'filter',
    'float',
    'format',
    'frozenset',
    'getattr',
    'globals',
    'hasattr',
    'hash',
    'help',
    'hex',
    'id',
    'input',
    'int',
    'isinstance',
    'issubclass',
    'iter',
    'len',
    'license',
    'list',
    'locals',
    'map',
    'max',
    'memoryview',
    'min',
    'next',
    'object',
    'oct',
    'open',
    'ord',
    'pow',
    'print',
    'property',
    'qApp',
    'quit',
    'range',
    'repr',
    'reversed',
    'round',
    'set',
    'setattr',
    'slice',
    'sorted',
    'staticmethod',
    'str',
    'sum',
    'super',
    'tuple',
    'type',
    'vars',
    'zip'
]

MEL_BUILTINS = [
    'abs',
    'bool',
    'eval',
    'float',
    'int',
    'max',
    'min',
    'pow',
    'print',
    'round',
    'string',
]
