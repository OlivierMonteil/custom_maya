from PySide2 import QtWidgets, QtCore

from custom_maya import utils
from custom_maya import logger
from custom_maya import prefs
from custom_maya.core import Handler

log = logger.get()


@utils.traceback_on_all
class Manager(QtCore.QObject):
    """
    Managers base class.
    """

    _handler_class = Handler
    _name = None
    _window_titles = None

    def __init__(self, parent=None):
        super(Manager, self).__init__(parent=parent)

        self.setParent(parent)

        self._prefs = prefs.get()
        self._prefs.changed.connect(self.on_prefs_changed)

        log.verbose('{} instance created.'.format(self.__class__.__name__))

    def on_prefs_changed(self):
        pass

    def apply(self, *args):
        widget = self.target_widget()
        if not widget or not self.is_needed(widget):
            return

        handler = self._handler_class(*args, parent=widget)
        handler.install()
        return handler

    def target_widget(self):
        raise NotImplementedError(
            "'{}.target_widget' must be re-implemented.".format(
                self.__class__.__name__
            )
        )

    def interested_by(self, widgets_list):
        if not self.initial_condition():
            return False

        if self._window_titles and widgets_list:
            for widget in widgets_list:
                while widget:
                    try:
                        if hasattr(widget, 'windowTitle'):
                            if widget.windowTitle() in self._window_titles:
                                return True

                        if hasattr(widget, 'parent'):
                            widget = widget.parent()
                        else:
                            break

                    except:
                        # Internal C++ object already deleted
                        return True     # in case
            return False
        return True

    def initial_condition(self):
        if self._window_titles:
            app = QtWidgets.QApplication.instance()

            for widget in app.topLevelWidgets():
                if widget.windowTitle() in self._window_titles:
                    return True

            return False
        return True

    def is_needed(self, txt_edit, tab_type=None):
        return self.is_enabled(tab_type) and utils.obj_needs(txt_edit, self._handler_class)

    def is_enabled(self, tab_type=None):
        if self._name is None:
            raise AttributeError("'{}._name' is None.".format(self.__class__.__name__))

        if tab_type:
            arg_pref = self._prefs.getattr(tab_type)
            return arg_pref.enabled() and arg_pref.getattr(self._name).enabled()

        return self._prefs.getattr(self._name).enabled()

    def get_local_prefs(self):
        return prefs.get_local_prefs(self)
