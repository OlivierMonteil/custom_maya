from PySide2 import QtCore


class Handler(QtCore.QObject):
    def __init__(self, parent=None):
        super(Handler, self).__init__(parent)
        self.setParent(parent)

        self._txt_edit = parent
        self._enabled = False

    def on_ctrl_signal(self, state):
        pass

    def install(self):
        raise NotImplementedError(
            "'{}.install' must be re-implemented.".format(
                self.__class__.__name__
            )
        )

    def set_enabled(self, state):
        self._enabled = state

    def is_enabled(self):
        return self._enabled
