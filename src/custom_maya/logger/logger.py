"""
Simple logger class with singleton behaviour.
"""

import os
import re
from datetime import datetime

from maya import OpenMayaUI as OMUI
from PySide2 import QtWidgets, QtGui
import shiboken2

from maya import cmds

from custom_maya import __version__, __author__, __tool_name__

MAX_LOGS = 30

PATTERN = '{:04d}-{:02d}-{:02d}_{:02d}h{:02d}.log'
LEVELS = [
    'verbose',
    'info',
    'success',
    'warning',
    'error',
    'critical'
]

STATUS_COLORS = {
    'success': QtGui.QColor(89, 167, 61),
    'info': QtGui.QColor(130, 130, 130),
    'verbose': QtGui.QColor(130, 130, 130),
    'debug': QtGui.QColor(130, 130, 130),
    'error': QtGui.QColor(217, 82, 82),
    'critical': QtGui.QColor(217, 82, 82),
    'warning': QtGui.QColor(204, 191, 128)
}

LOGS_ROOT = 'E:/TEMP/custom_maya'
if not os.path.exists(LOGS_ROOT):
    os.makedirs(LOGS_ROOT)


class errors_to_log(object):
    """
    Simple context manager that forces errors and criticals to be written into
    log files, even if the logger's' "write-to-file" attribute is set to False.
    """

    def __init__(self, level):
        self._log = get()
        self._level = level
        self._write_to_file = self._log.write_to_file
        self._filename = self._log.filename

    def __enter__(self, *args, **kwargs):
        if LEVELS.index(self._level) >= LEVELS.index('error'):
            self._log.write_to_file = True

    def __exit__(self, *args, **kwargs):
        self._log.write_to_file = self._write_to_file
        self._log.filename = self._filename


class Logger(object):
    _instance = None

    prefix = __tool_name__
    level = 'info'
    write_to_file = False
    filename = None
    _to_cmd_line_enabled = True

    def __init__(self):

        # add write function for each level in LEVELS list
        for lvl in LEVELS:
            self.__dict__[lvl] = self.level_function(lvl)

    def write(self, msg, level, error_type=None):
        with errors_to_log(level):
            if LEVELS.index(self.level) > LEVELS.index(level):
                return

            txt = '# [{}]  {} : {}'.format(self.prefix, '{}', msg)
            txt = txt.format(error_type) if error_type is not None else \
                  txt.format(level.capitalize())

            print(txt)

            if self.write_to_file:
                self.write_into_file(txt)
                if self._to_cmd_line_enabled:
                    display_in_cmd_line(txt, level)

    def write_into_file(self, txt):
        # remove prefix from log files
        txt = re.sub(r'#\s+\[{}\]\s+'.format(self.prefix), '# ', txt)

        if Logger.filename is None:
            Logger.filename = get_new_file_name()
        new = True if not os.path.isfile(Logger.filename) else False

        try:
            with open(Logger.filename, 'a+') as opened_file:
                if new:
                    opened_file.write(self.get_header())
                opened_file.write(txt +'\n')
        except:
            pass

    def traceback(self, trace):
        """
        Args:
            trace (list[str])
        """

        txt = ''
        for line in trace:
            for x in line.split('\n')[:-1]:
                if not x.startswith('# '):
                    x = '# ' +x
                txt += x +'\n'

        self.write_into_file(txt)
        print(txt)

    def level_function(self, lvl):
        return lambda msg, **kwargs: self.write(msg, lvl, **kwargs)

    def get_header(self):
        txt  = '#'*80 +'\n'
        txt += '# {}\n'.format(cmds.about(ctime=True))
        txt += '# {} {}  (author {})\n'.format(self.prefix, __version__, __author__)
        txt += '\n'
        txt += '# Log level : {}\n'.format(self.level.upper())
        txt += '# Maya version : {}\n'.format(cmds.about(installedVersion=True))
        txt += '# Maya LT : {}\n'.format(cmds.about(ltVersion=True))
        txt += '# Maya batch : {}\n'.format(cmds.about(batch=True))
        txt += '# Os version : {}\n'.format(cmds.about(operatingSystemVersion=True))
        txt += '# Qt version : {}\n'.format(cmds.about(qtVersion=True))
        txt += '#'*80 +'\n'
        txt += '\n'

        return txt


def display_in_cmd_line(message, messageType):
    ptr = OMUI.MQtUtil.findControl('commandLine1')
    widget = shiboken2.wrapInstance(int(ptr), QtWidgets.QWidget)
    cmd_line = widget.findChildren(QtWidgets.QLineEdit)[0]

    cmd_line.setText(message)
    palette = cmd_line.palette()

    palette.setColor(palette.Base, STATUS_COLORS[messageType])
    palette.setColor(palette.Text, QtGui.QColor(0, 0, 0))

    cmd_line.setPalette(palette)
    cmd_line.update(cmd_line.geometry())

def get(**kwargs):
    """ Get singleton instance of Logger. """

    if 'level' in kwargs:
        Logger.level = kwargs['level']
    if 'write_to_file' in kwargs:
        Logger.write_to_file = kwargs['write_to_file']
    if 'prefix' in kwargs:
        Logger.prefix = kwargs['prefix']

    if Logger._instance is None:
        cleanup_logs()
        Logger._instance = Logger()

    return Logger._instance

def get_new_file_name():
    """
    Returns:
        (str) : absolute path
    """

    d = datetime.now()
    filename = PATTERN.format(d.year, d.month, d.day, d.hour, d.minute)
    return os.path.join(LOGS_ROOT, filename)

def cleanup_logs():
    """
    Make sure the log file count does not exceed the MAX_LOGS constant value.
    If so, remove as many as older logs to get this maximum count value.
    """

    if not os.path.isdir(LOGS_ROOT):
        return

    files = os.listdir(LOGS_ROOT)
    if not files:
        return

    rgx = PATTERN
    substitutions = [
        ('{:0', '(\d{'),
        ('d}', '})'),
        ('-', '\-'),
        ('.', '\.')
    ]

    for sub in substitutions:
        rgx = rgx.replace(*sub)
    rgx = re.compile(rgx)

    paths = [os.path.join(LOGS_ROOT, x) for x in files if rgx.match(x)]
    if len(paths) <= MAX_LOGS:
        return

    paths = sorted(paths, key=lambda x: os.path.getmtime(x))
    for path in paths[:-MAX_LOGS]:
        os.remove(path)
