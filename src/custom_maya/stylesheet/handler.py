"""
Windows stylesheet's fonctions and classes.
"""

import os
import json

from PySide2 import QtGui

from custom_maya import constants as kk


CSS_ROOT = os.path.join(kk.STYLESHEET_ROOT, '.css')
CSS_ICONS_ROOT = os.path.join(kk.STYLESHEET_ROOT, '.icons')


def apply(widget):
    """
    Args:
        widget (QtWidgets.QWidget)

    Apply stylesheet from ./.css/*.css files on <widget> and set logo on widget.
    """

    widget.setStyleSheet(get_stylesheet())
    widget.setWindowIcon(QtGui.QIcon(os.path.join(CSS_ICONS_ROOT, 'logo.ico')))

def get_stylesheet():
    """
    Returns:
        (str)

    Get stylesheet from all .css/*.css files, with string replacement from
    colors.json dict and icon's root.
    """

    colors = get_colors()
    style = get_raw_stylesheet()

    colors['icons-root'] = CSS_ICONS_ROOT.replace('\\', '/')

    return style % colors

def get_raw_stylesheet():
    """
    Returns:
        (str)

    Get non-formatted stylesheet text from ./.css/*.css files
    (% string replacements will be applied later).
    """

    css_files = get_css_files(CSS_ROOT)

    style = ''

    for f in css_files:
        with open(f, 'r') as opened_file:
            style += opened_file.read()

    return style

def get_css_files(root):
    """
    Args:
        root (str) : absolute path

    Returns:
        (list[str]) : *.cc files list (sorted)

    Get all CSS files from ./.css folder.
    """

    if not os.path.isdir(root):
        return []

    result = []

    for item in os.listdir(root):
        path = os.path.join(root, item)

        if os.path.isdir(path):
            result.extend(get_css_files(path))

        else:
            if path.endswith('.css'):
                result.append(path)

    return sorted(result)

def get_colors():
    """
    Returns:
        (dict)

    Get stylesheet %-string-replacement colors as dict from colors.json file.
    """

    colors_file = os.path.join(kk.STYLESHEET_ROOT, 'colors.json')

    with open(colors_file, 'r') as opened_file:
        data = json.load(opened_file)
        for key in data:
            data[key] = tuple(data[key])

        return data
