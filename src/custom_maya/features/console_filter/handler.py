import os
import sys

from PySide2 import QtCore

from custom_maya import utils
from custom_maya.logger import logger
from custom_maya.core import Handler
from custom_maya.features.console_filter.data import MultiLineRegex
from custom_maya.features.collapser import get_manager as get_collapser


LOG_FILE = os.path.join(logger.LOGS_ROOT, 'console_filter.log')

with open(LOG_FILE, 'w') as opened_file:
    opened_file.write('')

def stdout_to_log(func):
    def wrap(*args, **kwargs):
        old_stdout = sys.stdout
        sys.stdout = open(LOG_FILE, 'a+')
        try:
            return func(*args, **kwargs)
        except Exception as e:
            print (e)

        finally:
            sys.stdout.close()
            sys.stdout = old_stdout
    return wrap

# def silent_stdout(func):
#     def wrap(*args, **kwargs):
#         old_stdout = sys.stdout
#         sys.stdout = open(os.devnull, 'w')
#         try:
#             return func(*args, **kwargs)
#         finally:
#             sys.stdout.close()
#             sys.stdout = old_stdout
#     return wrap


@utils.traceback_on_all
class ConsoleFilterHandler(Handler):
    """
    Console filtering handler. This instance will be destroyed when the Script
    Editor is closed, but both FilterManager and FilterData instances are attached
    to the main window, so we will be able to restore the last calculations at
    re-opening the Script Editor.
    """

    new_range_added = QtCore.Signal(str, int, int)

    def __init__(self, filter_data, parent=None):
        super(ConsoleFilterHandler, self).__init__(parent)

        self._txt_edit = parent
        self._filter_data = filter_data
        self._collapser = None
        self._multi_rgx = None
        self._rgx = None
        self._start = None
        self._current_block = None
        # get the current block number from FilterData (if Script Editor was closed
        # then re-opened, we don't need to re-evaluate what has already been done)
        self._idx = filter_data.last_block_checked


    @stdout_to_log
    def install(self):
        """
        Add ToggleButton on the right corner of the console and apply it.
        """

        self._collapser = get_collapser()
        self._txt_edit.document().blockCountChanged.connect(self._on_content_changed)

        self._on_content_changed()

    @stdout_to_log
    def _on_content_changed(self, *args):
        """ Filter valid blocks from the last evaluated. """

        silent = True if not self._enabled else False

        # user cleared the console
        if self._idx > 0 and self._txt_edit.document().blockCount() == 1:
            self._filter_data.clear_hidden_range()
            self._idx = 0
            self._current_block = None
            return

        # first time in this instance
        if self._current_block is None:
            self._current_block = self._txt_edit.document().findBlockByNumber(self._idx)

        while self._current_block.isValid():
            self._filter_line(silent)
            self._idx += 1
            self._current_block = self._current_block.next()
        # we need to roll back to the last valid block
        self._current_block = self._current_block.previous()
        self._idx -= 1
        # store the current index
        self._filter_data.last_block_checked = self._idx

    @stdout_to_log
    def _filter_line(self, silent):
        """
        """

        text = self._current_block.text()

        # all other rules will be skipped until the current MultiRegex is "clased"
        if self._multi_rgx is not None:
            if self._multi_rgx.search(text):
                self._multi_rgx.search(text)
                # start block is already caught at this point
                self.hide_from_regex(self._multi_rgx, self._start, self._idx-1, silent)
                self._multi_rgx.toggle()    # switch back on start-regex
                self._multi_rgx = None
            return

        for rgx in self._filter_data.active_rules():
            if rgx.search(text):
                if isinstance(rgx, MultiLineRegex):
                    self._multi_rgx = rgx
                    self._multi_rgx.toggle()

                if self._start is None:
                    self._start = self._idx
                # no need to check the other regex from the list
                self._rgx = rgx
                break

        # hide previous blocks if any start block has been detected
        if self._start is not None and self._multi_rgx is None:
            self.hide_from_regex(self._rgx, self._start, self._idx, silent)

    @stdout_to_log
    def set_enabled(self, state):
        self._enabled = state
        self.apply_filter()

    @stdout_to_log
    def on_ctrl_signal(self, state):
        self.set_enabled(state)

    @stdout_to_log
    def apply_filter(self):
        for start, end in self._filter_data.get_hidden_range():
            if start is not None:
                self._collapser.set_blocks_visibility(
                    self._txt_edit, start, end, not self._enabled
                )

    @stdout_to_log
    def hide_from_regex(self, rgx, start, end, silent):
        if not silent:
            self._collapser.set_blocks_visibility(self._txt_edit, start, end+1, False)

        # merge start/end range to the FilterData's hidden-ranges string
        self._filter_data.merge_hidden(rgx, start, end)

        self._start = None
        self._rgx = None
