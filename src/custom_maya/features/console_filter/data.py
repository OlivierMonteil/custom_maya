"""
These class and methods are used to store the .json rules for the Console filtering,
and also its results as a simple string.
The FilterData is attached to the FilterManager (and so to the main window).
That way, the filter rules and results are stored and may be re-used at each toggle,
and when the Script Editor is clased and re-opened later.
"""

import os
import json

from custom_maya.utils import Regex


RULES_FILES = [
    '.rules',
    '.custom-rules'
]


class FilterData(object):
    def __init__(self):

        self.rules = self._get_rules_from_json()

        # for current states storage
        self.hidden_range = RangeString()
        self.last_block_checked = 0

    def __iter__(self):
        return self.rules.__iter__()

    def clear_hidden_range(self):
        self.hidden_range = RangeString()
        self.last_block_checked = 0

    def get_hidden_range(self):
        """
        Yields:
            (int)

        Translate the hidden-ranges string into an int generator.
        """

        if not self.hidden_range.str:
            yield None, None
        else:
            for tok in self.hidden_range.split('|'):
                if '-' in tok:
                    sub_tokens = tok.split('-')
                    yield int(sub_tokens[0]), int(sub_tokens[1]) +1
                else:
                    yield int(tok), int(tok) +1

    def _get_rules_from_json(self):
        """
        Returns:
            (dict)
        """

        rules = {}

        for rule_file in RULES_FILES:
            path = os.path.join(os.path.dirname(__file__), rule_file)
            try:
                with open(path, 'r') as opened_file:
                    content = json.load(opened_file)

                    for x in content:
                        if x not in rules:
                            rules[x] = []

                        for rule in content[x]:
                            # MultiLineRegex
                            if isinstance(rule, (list, tuple)):
                                rules[x].append(MultiLineRegex(*rule))
                            else:
                                rules[x].append(Regex(rule))
            except:
                pass

        return rules

    def merge_hidden(self, rgx, start, end=None):
        self.hidden_range.merge(RangeString(str(start) +'-' +str(end)))

    def active_rules(self):
        for x in self.rules:
            for rgx in self.rules[x]:
                yield rgx


class MultiLineRegex(Regex):
    """
    Regex derivate with a toggle() method that allows to switch from a start-regex
    to an end one, and so on.
    """

    def __init__(self, start_rgx, end_rgx):
        super(MultiLineRegex, self).__init__(start_rgx)

        self._start_rgx = Regex(start_rgx)
        self._end_rgx = Regex(end_rgx)

        self._regex = self._start_rgx

    def toggle(self):
        if self._regex == self._start_rgx:
            self._regex = self._end_rgx
        else:
            self._regex = self._start_rgx


class FilterCategory(object):
    """
    """

    def __init__(self, name):

        self.name = name
        self.range_str = RangeString()

        # for further features with category checkboxes for more selective filtering
        self.active = True

    def add_range(self, start, end):
        self.range_str.merge(RangeString(str(start) +'-' +str(end)))

    def add_block(self, line):
        self.range_str.merge(RangeString(str(line)))

    def blocks(self):
        return self.range_str.__iter__()

    def __str__(self):
        return '<FilterCategory object: "{}" -> {}>'.format(self.name, self.range_str)

    def __contains__(self, x):
        return self.range_str.__contains__(x)

    def __iter__(self):
        if not self.active:
            return ()
        return self.range_str.__iter__()


class RangeString(object):
    def __init__(self, range_str=''):

        self.str = range_str

    def __iter__(self):
        for x in self.str.split('|'):
            if not '-' in x:
                yield int(x)
            else:
                tokens = x.split('-')
                for i in range(int(tokens[0]), int(tokens[1]) +1):
                    yield i

    def __contains__(self, x):
        x = int(x)
        for i in self:
            if x == i:
                return True
        return False

    def split(self, char):
        return self.str.split(char)

    def __str__(self, *args):
        return self.str

    def merge(self, others):
        if not isinstance(others, list):
            others = [others]

        result_list = []

        for x in others +[self]:
            if not x.str:
                continue
            for tok in x.split('|'):
                if '-' in tok:
                    sub_tokens = tok.split('-')
                    result_list.extend(list(range(int(sub_tokens[0]), int(sub_tokens[1]) +1)))
                else:
                    result_list.append(int(tok))

        if len(result_list) == 1:
            self.str = str(result_list[0])
            return

        result_list = sorted(list(set(result_list)))

        result = str(result_list[0])
        current = result_list[0]
        in_range = False

        for i in result_list[1:]:
            if i == current +1:
                in_range = True
            else:
                if in_range:
                    result += '-' +str(current)
                result += '|' +str(i)
                in_range = False
            current = i

        if in_range:
            result += '-' +str(current)

        self.str = result

def merged_ranges(ranges):
    new_range = RangeString()
    new_range.merge(ranges)
    return new_range
