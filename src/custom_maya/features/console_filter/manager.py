from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.console_filter import ConsoleFilterHandler, FilterData


@utils.traceback_on_all
class ConsoleFilterManager(Manager):
    _handler_class = ConsoleFilterHandler
    _instance = None
    _name = None
    _window_titles = ['Script Editor']

    def __init__(self, parent=None):
        super(ConsoleFilterManager, self).__init__(parent)

        self._filter_data = FilterData()

    def apply(self):
        return super(ConsoleFilterManager, self).apply(self._filter_data)

    def target_widget(self):
        return utils.get_script_editor().console_txt_edit

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
