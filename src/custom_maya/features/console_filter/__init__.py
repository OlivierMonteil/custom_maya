"""
This package is made for filtering Scipt Editor's console log content using .json
rule files.
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.console_filter.data import FilterData
from custom_maya.features.console_filter.handler import ConsoleFilterHandler
from custom_maya.features.console_filter.manager import ConsoleFilterManager


def get_manager():
    return utils.get_singleton(ConsoleFilterManager)
