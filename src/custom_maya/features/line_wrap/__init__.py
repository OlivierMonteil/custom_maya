# shorten absolute imports
from custom_maya import utils
from custom_maya.features.line_wrap.handler import LineWrapHandler
from custom_maya.features.line_wrap.manager import LineWrapManager


def get_manager():
    return utils.get_singleton(LineWrapManager)
