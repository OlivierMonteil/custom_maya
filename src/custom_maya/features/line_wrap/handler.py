from custom_maya import utils
from custom_maya.core import Handler


@utils.traceback_on_all
class LineWrapHandler(Handler):

    def on_ctrl_signal(self, state):
        self.set_enabled(state)

    def set_enabled(self, enabled):
        mode = self._txt_edit.WidgetWidth if enabled else self._txt_edit.NoWrap
        self._txt_edit.setLineWrapMode(mode)

    def install(self):
        pass
