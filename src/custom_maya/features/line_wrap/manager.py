from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.line_wrap import LineWrapHandler


@utils.traceback_on_all
class LineWrapManager(Manager):
    _handler_class = LineWrapHandler
    _instance = None
    _name = None

    def target_widget(self):
        return utils.get_script_editor().console_txt_edit

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
