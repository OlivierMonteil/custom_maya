"""
This manager will be used as a common class for all QTextBlock "collapse" (actually
we will set its visibility). It also allows to perform synchronized collapsing on
several QTextEdits at the same time (originally the line numbers were handled on
a QTextEdit, and this system has been leaved there in case of future need).
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.collapser.manager import CollapseManager


def get_manager():
    return utils.get_singleton(CollapseManager)
