from custom_maya.core import Manager


class CollapseManager(Manager):
    _handler_class = None
    _instance = None
    _name = None

    _targets = []

    def apply(self, *args):
        return None

    def is_needed(self):
        return True

    def is_enabled(self):
        return True

    def set_blocks_visibility(self, txt_edit, start, end, state):
        """
        Args:
            txt_edit (QtWidgets.QTextEdit)
            start (int)
            end (int)
            state (bool)

        Force blocks from start/end input-range's visibility to <state>.
        """

        start_pos = None
        length = 0

        for i in range(start, end):
            block = txt_edit.document().findBlockByNumber(i)
            block.setVisible(state)

            if start_pos is None:
                start_pos = block.position()
            length += block.length()

        start_pos = 0 if start_pos is None else start_pos
        txt_edit.document().markContentsDirty(start_pos, length +1)
