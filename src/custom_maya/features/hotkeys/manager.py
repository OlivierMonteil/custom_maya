from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.hotkeys import HotkeysHandler


@utils.traceback_on_all
class HotkeysManager(Manager):
    _handler_class = HotkeysHandler
    _instance = None
    _name = None
    _window_titles = ['Script Editor', 'Expression Editor']

    def apply(self, *args):
        created = []

        for widget, language in self.target_widget():
            if not widget or not self.is_needed(widget):
                continue

            handler = HotkeysHandler(language, parent=widget)
            handler.install()
            created.append(handler)

        return created

    def target_widget(self):
        yield utils.get_expression_txt_edit(), 'mel'
        tab = utils.get_script_editor().selected_tab
        if tab:
            yield tab.txt_edit, tab.language

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
