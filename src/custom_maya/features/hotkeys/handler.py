#-*- coding: utf-8 -*-

"""
Script for customizing Maya's script editor hotkeys.
"""

from PySide2 import QtCore, QtGui

from custom_maya import utils
from custom_maya import constants as kk
from custom_maya.core import Handler
from custom_maya.features.multi_cursor import MultiCursorHandler
from custom_maya.features.multi_cursor import MultiCursor


class extra_selections_updated(object):
    """
    Context manager for automatic QTextEdit extra-selections update on keyEvents.
    """

    def __init__(self, handler):
        self._handler = handler
        self._multi_handler = None

    def __enter__(self, *args, **kwargs):
        self._multi_handler = self._handler.get_multi_handler()

    def __exit__(self, *args, **kwargs):
        if self._multi_handler:
            self._multi_handler.update_extra_selections()


class HotkeysHandler(Handler):
    """
    Events filter for Keys handling.
    """

    def __init__(self, language, parent=None):
        super(HotkeysHandler, self).__init__(parent)

        self.comment_char = '// ' if language == 'mel' else '# '

    def install(self):
        """ Install the event. """
        self.parent().installEventFilter(self)

    def get_multi_handler(self):
        """
        Returns:
            (MultiCursorHandler)

        Get MultiCursorHandler instance from QTextEdit.
        """

        txt_edit = self.parent()
        return txt_edit.findChild(MultiCursorHandler)

    def get_cursor(self, obj):
        """
        Args:
            obj (QTextEdit)

        Returns:
            (MultiCursor)

        Get new cursor as MultiCursor insance. This cursor will "inherit" from
        current MultiCursorHandler's cursors if exists.
        """

        multi_manager = self.get_multi_handler()
        if not multi_manager:
            return MultiCursor([obj.textCursor()], obj)

        return MultiCursor(multi_manager.cursors, obj)

    def eventFilter(self, obj, event):
        """
        Args:
            obj (QTextEdit)

        Filter and handle key events (only).
        """

        if event.type() != event.KeyPress:
            return False

        with extra_selections_updated(self), utils.updates_disabled(self.parent()):

            key = event.key()

            if event.modifiers() == QtCore.Qt.AltModifier:
                # add new multi-cursor under current one
                if key == QtCore.Qt.Key_Down:
                    multi_handler = self.get_multi_handler()
                    multi_handler.add_cursor_from_key('down')
                    return True
                # add new multi-cursor above current one
                if key == QtCore.Qt.Key_Up:
                    multi_handler = self.get_multi_handler()
                    multi_handler.add_cursor_from_key('up')
                    return True

                return False

            cursor = self.get_cursor(obj)

            if event.modifiers() == (QtCore.Qt.ShiftModifier | QtCore.Qt.ControlModifier):
                # handle lines duplication on Ctrl+Shift+D
                if key == QtCore.Qt.Key_D:
                    cursor.duplicate_lines()
                    return True

                # extend selections on previous/next word on Ctrl +Shift +Left/Right
                if key in (QtCore.Qt.Key_Left, QtCore.Qt.Key_Right):
                    cursor.extend_selections(key, by_word=True)
                    return True

                return False


            if event.modifiers() == (QtCore.Qt.ControlModifier | QtCore.Qt.KeypadModifier):
                # handle block comments on Ctrl+Shift+/
                if key == QtCore.Qt.Key_Slash:
                    cursor.toggle_block_comment(self.comment_char)
                    return True

                if key in kk.DIGIT_KEYS:
                    num = kk.DIGIT_KEYS.index(key)
                    cursor.multi_digits(num)
                    return True

                return False


            if event.modifiers() == QtCore.Qt.ControlModifier:
                # move lines on Ctrl +down/up
                if key == QtCore.Qt.Key_Down:
                    return cursor.move_lines('down')
                if key == QtCore.Qt.Key_Up:
                    return cursor.move_lines('up')

                if key in (QtCore.Qt.Key_Left, QtCore.Qt.Key_Right):
                    operation = cursor.get_move_operation_from_key(key, by_word=True)
                    cursor.multi_movePosition(operation, cursor.MoveAnchor)
                    return True

                # paste on Ctrl +V
                if key == QtCore.Qt.Key_V:
                    text = QtGui.QClipboard().text()
                    cursor.insertText(text)
                    return True

                # paste on Ctrl +V
                if key == QtCore.Qt.Key_C:
                    cursor = self.parent().textCursor()
                    text = cursor.selection().toPlainText()
                    text = text.replace('\t', ' '*4)
                    QtGui.QClipboard().setText(text)
                    return True

                # clear multi-cursors on Ctrl +A and select all
                if key == QtCore.Qt.Key_A:
                    multi_manager = self.get_multi_handler()
                    multi_manager.clear_cursors()
                    cursor = multi_manager.cursors[-1]
                    cursor.movePosition(cursor.Start, cursor.MoveAnchor)
                    cursor.movePosition(cursor.End, cursor.KeepAnchor)
                    self.parent().setTextCursor(cursor)
                    return True

                return False


            if event.modifiers() == QtCore.Qt.ShiftModifier:
                # extend selections on Shift +cursor move
                if key in kk.MOVE_KEYS:
                    cursor.extend_selections(key)
                    return True

            if key in kk.MOVE_KEYS:
                operation = cursor.get_move_operation_from_key(key)
                cursor.multi_movePosition(operation, cursor.MoveAnchor)
                return True

            if key in (QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return):
                cursor.handle_return()
                return True

            text = event.text()

            # handle embracing characters
            if text == '`':
                return cursor.embrace_text_with('`', '`')
            if key == QtCore.Qt.Key_ParenLeft:
                return cursor.embrace_text_with('(', ')')
            if key == QtCore.Qt.Key_BracketLeft:
                return cursor.embrace_text_with('[', ']')
            if key == QtCore.Qt.Key_BraceLeft:
                return cursor.embrace_text_with('{', '}')
            if key == QtCore.Qt.Key_ParenRight:
                return cursor.ignore_if_next(')')
            if key == QtCore.Qt.Key_BracketRight:
                return cursor.ignore_if_next(']')
            if key == QtCore.Qt.Key_BraceRight:
                return cursor.ignore_if_next('}')
            if key == QtCore.Qt.Key_QuoteDbl:
                return cursor.embrace_text_with('"', '"')
            if key == QtCore.Qt.Key_Apostrophe:
                return cursor.embrace_text_with('\'', '\'')

            # handle blocks unindent
            if key == QtCore.Qt.Key_Backtab:
                cursor.unindent()
                return True
            # handle blocks unindent
            if key == QtCore.Qt.Key_Tab:
                cursor.indent()
                return True

            # delete
            if key == QtCore.Qt.Key_Delete:
                cursor.deleteChar()
                return True

            # backspace
            if key == QtCore.Qt.Key_Backspace:
                return cursor.handle_backspace()

            # any other cases
            if text and text in kk.CHARACTERS:
                cursor.insertText(text)
                return True

            if text and text in kk.SPECIAL_CHARS:
                cursor.insertText(text)
                return True

        # handle event as usual if nothing has been filtered ahead
        return False
