"""
Common classes for text highlighting :

- Highlighter : derived QtGui.QSyntaxHighlighter.
- Painter : handles the highlighting process (dissociated from Highlighter so
  ConsoleHighlighter may use all Console, Mel and Python highlights).
- Regex : re.compiled regex handler.
"""


import re

from PySide2 import QtGui, QtCore, QtWidgets

from custom_maya import utils
from custom_maya import constants as kk
from custom_maya import prefs
from custom_maya.utils import Regex, if_enabled
from custom_maya.features.highlight.palettes import get_filtered_palette


STYLE_PATTERN = """
%(widgetType)s#%(object_name)s {
    color: rgb%(color)s;
    background : rgb%(background)s;
}
"""


class Painter(QtCore.QObject):
    """
    Highlights Painter base class, derivated into console/mel/python_higlight.
    """

    docstr_chars = []
    docstr_close_chars = []
    raw_str_chars = []
    raw_str_close_chars = []
    cmnt_chars = []
    str_chars = []
    language = None

    escaped_regex = Regex(r'\\.')

    def __init__(self, highlighter, language=None):
        """
        Args:
            highlighter (derivated Highlighter)
            palette (derivated palettes.Palette)

        Initiates self.
        """

        if language is not None:
            self.language = language

        super(Painter, self).__init__(highlighter)

        self._highlighter = highlighter
        self._txt_edit = self._highlighter._txt_edit

        self._palette = self._get_palette()
        self._rules = self._get_rules()

        self._delim_strings = []

        self._docstr_states = self._get_docstr_states()
        self._raw_str_states = self._get_raw_str_states()
        self._str_states = self._get_str_states()
        self._comment_states = self._get_comment_states()

        self._delim_regex_list = [Regex(x) for x in self._delim_strings]
        self._raw_str_close_regex_list = [Regex(x) for x in self.raw_str_close_chars]
        self._docstr_close_regex_list = [Regex(x) for x in self.docstr_close_chars]

        self._enabled = True

        self._set_style_sheet()
        self._map_methods()

    def _set_style_sheet(self):
        """
        Set QTextEdit's  base style (background's and base text's colors + selected
        text and left-padding) based on theme's colors.
        """

        preferences = prefs.get()

        widgetType = 'QPlainTextEdit' if isinstance(self._txt_edit, QtWidgets.QPlainTextEdit) else 'QTextEdit'

        style_body = STYLE_PATTERN % {
            'widgetType': widgetType,
            'object_name': self._txt_edit.objectName(),
            'color': self._palette['normal']['rgb'],
            'background': self._palette['background']['rgb']
        }
        self._txt_edit.setStyleSheet(style_body)

        qpalette = self._txt_edit.palette()
        # set no brush on highlighted text (will keep sinthax highlight)
        qpalette.setBrush(
            QtGui.QPalette.HighlightedText,
            QtGui.QBrush(QtCore.Qt.NoBrush)
        )
        qpalette.setBrush(
            QtGui.QPalette.Highlight,
            QtGui.QColor(201, 214, 255, 50)
        )
        self._txt_edit.setPalette(qpalette)

    def update(self):
        """ Update rules and styles. """
        if self.language is None:
            return

        self._palette = self._get_palette()
        self._rules = self._get_rules()

    def paint(self, line):
        """
        Args:
            line (str)

        Apply syntax highlighting to the given line.
        """

        if not self._enabled:
            return

        # strings, docstrings and comments rules, using block states to propagate
        # un-closed rules from one line to another
        starts, ends = self.apply_multiline_style(line)

        if not starts:
            return

        ranges = [(starts[i], ends[i]) for i in range(len(ends))]

        # straight-forward regex rules, no block state used
        for regex, nth, txt_format in self._rules:
            self.apply_rule(line, regex, nth, txt_format, ranges)

    def apply_rule(self, line, regex, nth, rule, ranges):
        """
        Args:
            line (str)
            regex (Regex)
            nth (int) : the nth matching group that is to be highlighted
            rule (str)

        Apply <txt_format> on segments that matches <regex> in <line>.
        """

        for rge in ranges:
            segment = line[rge[0]:rge[1]]
            if not segment:
                continue

            for match in regex.finditer(segment):
                start = match.start(nth)
                end = match.end(nth)

                self.set_format(start +rge[0], end -start, rule)

    def apply_multiline_style(self, line, start_state=None):
        """
        Args:
            line (str)
            start_state (int, optional)

        Apply strings, docstrings and comments highlight on line.
        """

        if start_state is None:
            start_state = self.previous_block_state()

        # propagate previous line's state
        self.set_current_block_state(start_state)

        offset = 99999999999
        start = 0
        pos = 0

        starts = []
        ends = []


        while len(line) > pos > -1:
            # get current delimiter
            state = self.current_block_state()
            regex = self._delim_regex_list[state] if state > -1 else None

            # switch to docstring closing char
            if state in self._docstr_states:
                idx = self.docstr_chars.index(regex.to_string())
                regex = self._docstr_close_regex_list[idx]
            # switch to non-excaped closing char
            if state in self._raw_str_states:
                idx = self.raw_str_chars.index(regex.to_string())
                regex = self._raw_str_close_regex_list[idx]

                        ############################
                        #   No state is going on   #
                        ############################

            # if no current delimiter, get the next delimiter in line
            if regex is None:
                positions = [x.index_in(line, pos) for x in self._delim_regex_list]

                # no more delimiter in line
                if all(x == -1 for x in positions):
                    break

                positions = [x +offset if x == -1 else x for x in positions]
                pos = min(positions)

                # if found delimiter is escaped, search again
                if self._is_escaped(line, pos):
                    pos +=1
                    continue

                # "open" current state and delimiter
                state = positions.index(pos)
                self.set_current_block_state(state)

                if state in self._comment_states:      # comments, paint until the end of the line
                    self.set_format(pos, len(line) -pos, 'comments')
                    self.set_current_block_state(-1)

                    if not starts:
                        starts.append(0)
                    ends.append(pos)

                    return starts, ends         # there's no need for further analysis in this line

                # set current positions and regex
                start = pos
                if pos > 0:
                    if not starts:
                        starts.append(0)
                    ends.append(pos)

                regex = self._delim_regex_list[state]
                pos += len(regex)


                    ######################################
                    #   Some state is already going on   #
                    ######################################

            # else, search for the next occurence of the current delimiter in line
            else:
                next_pos = regex.index_in(line, pos)

                # not found
                if next_pos == -1:
                    # check for comments after a \-propagated string
                    comment_regex = self._delim_regex_list[self._comment_states[0]]
                    comment_pos = comment_regex.index_in(line, pos)
                    # comments found
                    if comment_pos and self.current_block_state() in self._str_states:
                        # last character before was a non-escaped \
                        if self._last_util_char(line, comment_pos) == '\\':
                            self._paint_string(start, comment_pos-1 -start, line)
                            self.set_format(
                                comment_pos,
                                len(line) -comment_pos,
                                'comments'
                            )
                            return starts, ends
                    break

                # closing char is escaped
                if self._is_escaped(line, next_pos):
                    self._paint_string(start, next_pos -start, line)
                    pos = next_pos +1
                    continue

                # "close" and paint string
                self._paint_string(
                    start,
                    next_pos +len(regex) -start -1,
                    line
                )
                self.set_current_block_state(-1)
                pos = next_pos +len(regex)
                starts.append(pos)

        ##############################################################
        #   after iterations (current state may still be "opened")   #
        ##############################################################

        if self.current_block_state() == -1:
            if not starts:
                starts = [0]
            ends.append(len(line))
            return starts, ends

        # comments (those after \ string-propagation are handled higher)
        if self.current_block_state() in self._comment_states:
            self.set_format(start, len(line) -start -1, 'comments')
            self.set_current_block_state(-1)

        elif self.current_block_state() in self._str_states:
            # no string-propagation

            if line[-1] != '\\':
                self.set_current_block_state(-1)
            else:
                self._paint_string(start, len(line) -start -1, line)
                self.set_format(len(line)-1, 1, 'escaped')

        # do not propagate unfinished non-escaped strings on next line (like r'...)
        elif self.current_block_state() in self._raw_str_states:
            self.set_current_block_state(-1)

        else:
            self._paint_string(start, len(line) -start -1, line)

        return starts, ends

    def _paint_string(self, start, count, line):
        """
        Args:
            start (int) : string start pos in <line>
            count (int) : number of character to paint
            line (str)

        Set each charcater's style from start to end, checking if it is escaped
        or not.
        """

        state = self.current_block_state()

        # paint docstrings
        if state in self._docstr_states:
            self.set_format(start, count+1, 'docstrings')
            return

        # paint strings
        self.set_format(start, count+1, 'strings')

        # skip next, as string is already painted
        if state in self._raw_str_states:
            return

        # paint escaped characters
        if 'escaped' in self._palette:
            for x in self.escaped_regex.finditer(line[start:start +count+1]):
                start_pos = start +x.start()
                length = x.end() -x.start()

                self.set_format(start_pos, length, 'escaped')

    def set_enabled(self, state):
        self._enabled = state

    def _get_rules(self):
        """ Returns empty list by default, must be re-implemented. """
        return []

    def _get_palette(self):
        return get_filtered_palette(self)

    def _get_chars_states(self, chars_list):
        """
        Returns:
            (list[int])

        Get <chars_list> block states as ints
        (also add characters to self._delim_strings).
        """

        offset = len(self._delim_strings)
        self._delim_strings += chars_list
        return [i +offset for i, _ in enumerate(chars_list)]


    def _get_docstr_states(self):
        """ Returns: (list[int]) """
        return self._get_chars_states(self.docstr_chars)

    def _get_str_states(self):
        """ Returns: (list[int]) """
        return self._get_chars_states(self.str_chars)

    def _get_raw_str_states(self):
        """ Returns: (list[int]) """
        return self._get_chars_states(self.raw_str_chars)

    def _get_comment_states(self):
        """ Returns: (list[int]) """
        return self._get_chars_states(self.cmnt_chars)

    def _last_util_char(self, line, pos):
        """
        Args:
            line (str)
            pos (int)

        Returns:
            (str or None)

        Perform backward-lookup from <pos> in line for the first non-whitespace
        character.
        Return None if this character is escaped.
        """

        line = line[:pos]

        while line:
            if not re.match('\s', line[-1]):
                # return None if char is escaped
                if self._is_escaped(line, len(line)-1):
                    return

                return line[-1]

            line = line[:-1]

    def _is_escaped(self, line, pos):
        escaped = False

        while pos:
            if line[pos-1] == '\\':
                escaped = not escaped
                pos -= 1
            else:
                break

        return escaped

    def set_format(self, start, length, rule):
        if rule in self._palette:
            self._highlighter.setFormat(start, length, self._palette[rule]['format'])

    def _map_methods(self):
        """ Simplify methods acces by creating aliases. """
        self.format = self._highlighter.format
        self.set_current_block_state = self._highlighter.setCurrentBlockState
        self.current_block_state = self._highlighter.currentBlockState
        self.previous_block_state = self._highlighter.previousBlockState
        self.current_block = self._highlighter.currentBlock
        self.current_block_user_data = self._highlighter.currentBlockUserData
        self.set_current_block_user_data = self._highlighter.setCurrentBlockUserData


class Highlighter(QtGui.QSyntaxHighlighter):
    """
    Highlighters base class, derivated into console/mel/python_higlight.
    """

    corrupted = QtCore.Signal()
    needs_rehighlight = QtCore.Signal()
    painter_class = Painter

    def __init__(self, txt_edit):
        super(Highlighter, self).__init__(highlight_target(txt_edit))
        
        self._enabled = False
        self._txt_edit = txt_edit

        self._painter = self.painter_class(self)

    def highlightBlock(self, line):
        """ Qt re-implementation. """

        if not hasattr(self, '_painter'):
            self.corrupted.emit()
            return

        self._painter.paint(line)

    @if_enabled
    def _update(self):
        """ Update Painter, style sheet and ask for re-highlight (if enabled). """

        self._painter.update()
        self._set_style_sheet()
        self.needs_rehighlight.emit()

    def set_painter_enabled(self, state):
        """
        Args:
            state (bool)

        Enable/Disable Painter instance (used to avoid Painter's rules and styles
        to be re-calculated at each palette update).
        """

        self._painter.set_enabled(state)

    def on_palette_change(self):
        self._update()

    def set_enabled(self, state):
        self._enabled = state

    def is_enabled(self):
        return self._enabled


def highlight_target(txt_edit):
    return txt_edit.document() if isinstance(txt_edit, QtWidgets.QPlainTextEdit) else txt_edit