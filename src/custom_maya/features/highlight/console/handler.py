from qtWrapper import QtCore

from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight.console.highlighter import ConsoleHighlighter


class ConsoleHighlight(HighlightHandler):
    _highlight_class = ConsoleHighlighter

    def install(self):
        super(ConsoleHighlight, self).install()

        self._txt_edit.appendHtml = append_no_html


def append_no_html(txt_edit, txt):
    no_html_txt = QtCore.QString(txt).toStdString()
    txt_edit.appendPlainText(no_html_txt)


