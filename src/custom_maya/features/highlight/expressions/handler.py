from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight.expressions.highlighter import ExpressionsHighlighter


class ExpressionsHighlight(HighlightHandler):
    _highlight_class = ExpressionsHighlighter
