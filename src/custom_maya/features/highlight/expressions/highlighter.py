"""
Script Editor's Console higlight classes (derivated from ./common.py).
"""

from custom_maya.features.highlight.common import Highlighter
from custom_maya.features.highlight.mel.highlighter import MelPainter


class ExpressionsPainter(MelPainter):
    """
    Syntax highlight painter for the Console panel (MEL/Python/console).
    """

    language = 'mel'

    def __init__(self, highlighter):
        super(ExpressionsPainter, self).__init__(highlighter, 'expressions|mel')


class ExpressionsHighlighter(Highlighter):
    painter_class = ExpressionsPainter
