from custom_maya import prefs
from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight import MelHighlight
from custom_maya.features.highlight import PythonHighlight
from custom_maya.features.highlight import QuickHelpHighlight
from custom_maya.features.highlight import ConsoleHighlight
from custom_maya.features.highlight import ExpressionsHighlight


@utils.traceback_on_all
class HighlightManager(Manager):
    _handler_class = HighlightHandler
    _instance = None
    _name = None
    _window_titles = ['Script Editor', 'Expression Editor']

    def apply(self, *args):
        created = []

        for txt_edit, language in self.target_widget():
            self._handler_class = PythonHighlight if language == 'python' else \
                                  MelHighlight if language == 'mel' else \
                                  ConsoleHighlight if language == 'console' else \
                                  ExpressionsHighlight if language == 'expressions' else \
                                  QuickHelpHighlight if language == 'quick-help' else None

            if self._handler_class is None:
                continue

            if not txt_edit or not self.is_needed(txt_edit):
                continue

            handler = self._handler_class(txt_edit)
            handler.install()
            created.append(handler)

        return created

    def target_widget(self):
        yield utils.get_expression_txt_edit(), 'expressions'

        script_editor = utils.get_script_editor()

        tab = script_editor.selected_tab
        if tab:
            yield tab.txt_edit, tab.language

        console = script_editor.console_txt_edit
        if console:
            yield console, 'console'

        quick_help = script_editor.quick_help_widget
        if quick_help:
            yield quick_help, 'quick-help'

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
