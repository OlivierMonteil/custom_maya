from PySide2 import QtGui

from custom_maya import utils
from custom_maya.core import Handler


@utils.traceback_on_all
class HighlightHandler(Handler):

    _highlight_class = None

    def __init__(self, parent):
        super(HighlightHandler, self).__init__(parent)

        self._txt_edit = parent
        self.highlighter = None

    def install(self):
        # remove current highlight
        for syntax_hl in self._txt_edit.findChildren(QtGui.QSyntaxHighlighter):
            syntax_hl.setDocument(None)

        self.highlighter = self._highlight_class(self._txt_edit)
