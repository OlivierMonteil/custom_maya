from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight.mel.highlighter import MelHighlighter


class MelHighlight(HighlightHandler):
    _highlight_class = MelHighlighter
