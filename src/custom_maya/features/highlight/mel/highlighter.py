"""
Script Editor's Mel tabs (or Console, whenever Mel language is detected) higlight
classes (derivated from ./common.py).
"""

from maya import cmds

from custom_maya import constants as kk
from custom_maya.utils import Regex
from custom_maya.features.highlight.common import Painter, Highlighter


class MelPainter(Painter):
    """
    Syntax highlighter for MEL tabs.
    """

    docstr_chars = ['/\\*']
    docstr_close_chars = ['\\*/']
    raw_str_chars = []
    raw_str_close_chars = []
    cmnt_chars = ['//']
    str_chars = ['"']
    maya_cmds = list(set(cmds.help('[a-z]*', list=True, lng='mel')))
    language = 'mel'

    specific_formats = {
        'def_names' : 'bold',
        'class_names' : 'bold',
        'comments' : 'italic',
    }

    def _get_rules(self):
        """
        Returns:
            (list[tuple(Regex, int, QtGui.QTextCharFormat)])

        Get all MEL rules, except for comments, strings and triple-quotes,
        that will be handled differently.
        """

        rules = []

        if 'numbers' in self._palette:
            # digits rule
            rules += [('\\b\d+\\b', 0, 'numbers')]

        if 'called' in self._palette:
            rules += [('^\s*\w+', 0, 'called')]
            rules += [('\\b(\w+)\s*\(.*\)', 1, 'called')]

        if 'maya_cmds' in self._palette:
            rules += [
                ('\\b%s\\b' % x, 0, 'maya_cmds')
                for x in self.maya_cmds
            ]

        if 'flags' in self._palette:
            rules += [('\\b-(\w+)\\b', 1, 'flags')]

        if 'called_expr' in self._palette:
            # expressions between ``
            rules += [('(`.*`)', 1, 'called_expr')]

        # if 'strings' in self._palette:
        #     rules += [('(\"\w*\")', 1, 'strings')]

        if 'variables' in self._palette:
            # $variables rules
            rules += [('\$\w+', 0, 'variables')]

        if 'keywords' in self._palette:
            # add MEL keywords rules
            rules += [('\\b(%s)\\b' % w, 0, 'keywords') for w in kk.MEL_KEYWORDS]
        if 'mel_numbers' in self._palette:
            # add MEL numbers rules
            rules += [('\\b(%s)\\b' % n, 0, 'mel_numbers') for n in kk.MEL_NUMBERS]
        if 'builtins' in self._palette:
            # add MEL builtins rules
            rules += [('\\b(%s)\\b' % n, 0, 'builtins') for n in kk.MEL_BUILTINS]
        if 'operators' in self._palette:
            # add operators rules
            rules += [('%s' % o, 0, 'operators') for o in kk.OPERATORS]

        if 'proc_names' in self._palette:
            # declared procedures rule
            rules += [ ('(\\bproc\\b\s+)(.+\s+)*(\w+)\s*\(', 3, 'proc_names')]

        if 'numbers' in self._palette:
            # numeric literals
            rules += [
                (r'\b[+-]?[0-9]+[lL]?\b', 0, 'numbers'),
                (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, 'numbers'),
                (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, 'numbers')
            ]

        return [(Regex(regex), nth, style) for regex, nth, style in rules]


class MelHighlighter(Highlighter):
    painter_class = MelPainter
