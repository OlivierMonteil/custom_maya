"""
Custom QItemDelegate and QSyntaxHighlighter for Quick Help list syntax highlight.
"""

import re

from PySide2 import QtWidgets, QtGui, QtCore

from custom_maya.features.highlight.palettes import get_filtered_palette


class QuickHelpDelegate(QtWidgets.QItemDelegate):
    """
    Custom QItemDelegate for Quick Help's QListView. Uses a synthax highlighter
    to paint each item.
    """

    needs_rehighlight = QtCore.Signal()

    def __init__(self, parent):
        super(QuickHelpDelegate, self).__init__(parent)

        self.apply_highlight()

        qpalette = parent.palette()
        # set no brush on highlighted text (will keep sinthax highlight)
        qpalette.setBrush(
            QtGui.QPalette.HighlightedText,
            QtGui.QBrush(QtCore.Qt.NoBrush)
        )
        qpalette.setBrush(
            QtGui.QPalette.Highlight,
            QtGui.QColor(201, 214, 255, 35)
        )
        parent.setPalette(qpalette)

    def apply_highlight(self):
        self._txt_document = QtGui.QTextDocument(self)
        self._highlight = QuickHelpHiglighter(self._txt_document)

        self.set_enabled = self._highlight.set_enabled

    def drawDisplay(self, painter, option, rect, text):
        if not hasattr(self, '_txt_document'):
            self.apply_highlight()
        rect = option.rect

        self._txt_document.setPlainText(text)
        painter.translate( rect.x(), rect.y() -4)
        ctxt = QtGui.QAbstractTextDocumentLayout.PaintContext()
        self._txt_document.documentLayout().draw(painter, ctxt)
        painter.restore()

    def on_palette_change(self):
        self.needs_rehighlight.emit()

    def highlightBlock(self, line):
        pass

    def rehighlight(self):
        self._highlight.update_styles()
        self._highlight.rehighlight()
        self.parent().repaint(self.parent().visibleRegion())


class QuickHelpHiglighter(QtGui.QSyntaxHighlighter):

    needs_rehighlight = QtCore.Signal()
    line_regex = re.compile('(-)(\w+)(/)(-)(\w+)([\s\w|]*)')
    language = 'quick-help'
    _enabled = True

    def __init__(self, txt_document):
        self._palette = self._get_palette()
        self._rules = self._get_rules()
        super(QuickHelpHiglighter, self).__init__(txt_document)

    def update_styles(self):
        self._palette = self._get_palette()
        self._rules = self._get_rules()

    def set_enabled(self, state):
        self._enabled = state

    def on_palette_change(self):
        self.needs_rehighlight.emit()

    def _set_style_sheet(self):
        pass

    def _get_palette(self):
        return get_filtered_palette(self)

    def highlightBlock(self, line):
        """
        Args:
            line (str)

        Apply syntax highlighting to the given line.
        """

        if not self._enabled:
            return

        match = self.line_regex.search(line)
        if match:
            for i, rule in enumerate(self._rules):
                if rule not in self._palette:
                    continue

                start = match.start(i+1)
                end = match.end(i+1)
                self.setFormat(start, end -start, self._palette[rule]['format'])

    def _get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])
        """

        return [
            'operators',
            'shortname',
            'builtins',
            'operators',
            'shortname',
            'builtins'
        ]
