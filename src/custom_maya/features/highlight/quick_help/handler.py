from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight.quick_help import QuickHelpDelegate


class QuickHelpHighlight(HighlightHandler):

    _highlight_class = QuickHelpDelegate

    def install(self):
        self._txt_edit.setItemDelegate(None)
        super(QuickHelpHighlight, self).install()
        self._txt_edit.setItemDelegate(self.highlighter)
