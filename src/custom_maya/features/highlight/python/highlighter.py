"""
Script Editor's Python tabs (or Console, whenever Python language is detected)
higlight classes (derivated from ./common.py).
"""

from PySide2 import QtGui

from maya import cmds

from custom_maya import constants as kk
from custom_maya.utils import Regex
from custom_maya.features.highlight.common import Painter, Highlighter


class PythonPainter(Painter):
    """
    Syntax highlighter for Python tabs.
    """

    docstr_chars = ["'''", '"""']
    docstr_close_chars = ["'''", '"""']
    raw_str_chars = ["r'", 'r"']
    raw_str_close_chars = ["'", '"']
    cmnt_chars = ['#']
    str_chars = ["'", '"']
    def_opening_regex = Regex('^\s*def\s+\w+\s*\(')
    def_closing_regex = Regex('\s*\)\s*\:')
    parenthesis_regex = Regex('[\(\)]')
    unfold_char_regex = Regex('^(\s*\*+)\w+')
    kwargs_regex = Regex('\\b(\w+)\s*\=')
    cmds_aliases = ['cmds', 'mc']
    maya_cmds = list(set(cmds.help('[a-z]*', list=True, lng='Python')))
    specific_formats = {
        'def_names' : 'bold',
        'class_names' : 'bold',
        'comments' : 'italic',
    }
    language = 'python'

    def __init__(self, *args, **kwargs):

        self._in_def_args = False

        super(PythonPainter, self).__init__(*args, **kwargs)

        self._kwargs_states = [
            max(self._comment_states) +1 +i for i in range(len(self._docstr_states) +1)
        ]

    def _get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])

        Get all Python rules, except for comments, strings and triple-quotes,
        that will be handled differently.
        """

        rules = []

        if 'intermediates' in self._palette:
            # intermediate objects rule
            rules += [('\.(\w+)', 1, 'intermediates')]

        if 'called' in self._palette:
            rules += [('(\\b_*\w+_*)(\s*\()', 1, 'called')]

        if 'maya_cmds' in self._palette:
            for alias in self.cmds_aliases:
                rules += [
                    ('\\b%s\.(%s)\s*\(' % (alias, x),
                    1,
                    'maya_cmds')
                    for x in self.maya_cmds
                ]

        if 'builtins' in self._palette:
            rules += [('\\b%s\\b' % x, 0, 'builtins') for x in kk.PYTHON_BUILTINS]

        if 'class_args' in self._palette:
            rules += [('(\\bclass\\b\s*_*\w+_*\s*\()(.+)(\))', 2, 'class_args')]

        if 'class_names' in self._palette:
            rules += [('(\\bclass\\b\s*)(_*\w+_*)', 2, 'class_names')]

        if 'def_names' in self._palette:
            rules += [('(\\bdef\\b\s*)(_*\w+_*)', 2, 'def_names')]

        if 'keywords' in self._palette:
            rules += [('\\b(%s)\\b' % w, 0, 'keywords') for w in kk.PYTHON_KEYWORDS]

        if 'operators' in self._palette:
            rules += [('%s' % o, 0, 'operators') for o in kk.OPERATORS]

        if 'python_numbers' in self._palette:
            rules += [('\\b(%s)\\b' % n, 0, 'python_numbers') for n in kk.PYTHON_NUMBERS]

        if 'numbers' in self._palette:
            rules += [
                (r'\b[+-]?[0-9]+[lL]?\b', 0, 'numbers'),
                (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, 'numbers'),
                (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, 'numbers')
            ]

        if 'decorators' in self._palette:
            rules += [('^\s*\@.+', 0, 'decorators')]

        if 'self' in self._palette:
            rules += [('\\b(self)\\b', 0, 'self')]

        if 'dots' in self._palette:
            rules += [('[\w\'\"\)\]\}]+(\.)\w+', 1, 'dots')]

        return [(Regex(regex), nth, style) for regex, nth, style in rules]

    def apply_multiline_style(self, line):
        """
        Args:
            line (str)

        Rule re-implementation, also applies arguments (in def() expressions) and
        keywords arguments painting rule.
        """

        if not any(x in self._palette for x in ('kwargs', 'args')):
            return super(PythonPainter, self).apply_multiline_style(line)

        # we will keep track of the parenthesis opened state by counting them :
        #   - an opening parenthesis will increment our counter by 1
        #   - an closing parenthesis will decrement our counter by 1
        # --> a counter to 0 will then mean that all parenthesis has been closed
        # at the end of the block
        parenthesis_count = 0

        block = self.current_block()
        previous_block = block.previous()

        # get previous block parenthesis count
        if previous_block and previous_block.isValid():
            try:
                parenthesis_count = previous_block.userData().parenthesis_count
            except:
                pass

        # set current block's user data and propagate previous parenthesis count
        # (we will edit it on current block while processing it)
        user_data = block.userData()
        if not user_data:
            user_data = BlockData()
            block.setUserData(user_data)

        user_data.parenthesis_count = parenthesis_count

        state = self.previous_block_state()
        self.set_current_block_state(state)

        # in order to keep track of both docstring states and def() arguments,
        # we will offset the docstring states at the end of this function if we
        # are also into a def() arguments declaration. Meanwhile, we need to set
        # it back to its origin value so the docstrings will be detected correctly
        if state in self._kwargs_states:
            relative_state = state -1 -self._kwargs_states[0]

            if relative_state in self._docstr_states:
                self.set_current_block_state(relative_state)
            else:
                self.set_current_block_state(-1)

            self._in_def_args = True

        # apply classic multi-line style (strings, docstrings and comments)
        starts, ends = super(PythonPainter, self).apply_multiline_style(
            line, start_state=self.current_block_state()
        )

        # set args and kwargs formats (self._in_def_args may be modified at this
        # step if a def() arguments declaration is opened or closed into the
        # current line
        self.set_args_ranges_format(line, user_data)

        # as said earlier, we will offset the current block state if a def()
        # arguments declaration is still opened at the end of the line
        if self._in_def_args:
            state = self.current_block_state()
            if state in self._docstr_states:
                state = self._kwargs_states[0] +1 +max(self._docstr_states)
            else:
                state = self._kwargs_states[0]

            self.set_current_block_state(state)

        return starts, ends

    def set_args_ranges_format(self, line, user_data, offset=0):
        """
        Args:
            line (str)
            user_data (BlockData)
            offset (int, optional)

        Get def() or called funcs args and kwargs ranges.
        """

        # check if any def() is declared at current line
        if not self._in_def_args:
            match = self.def_opening_regex.search(line)

            if match:
                self._in_def_args = True
                user_data.parenthesis_count += 1
                end = match.end(0)
                # recurse (with self._in_def_args now set to True)
                self.set_args_ranges_format(line[end:], user_data, offset=end)
                return

        parenthesis = list(self.parenthesis_regex.finditer(line))

        if parenthesis:
            # iterate parenthesis, update user_data.parenthesis_count
            # and apply keyword arguments when user_data.parenthesis_count
            # is > 0 (0 means that all parenthesis are closed)
            for i, p in enumerate(parenthesis) or ():
                position = p.end()
                # update count
                if p.group() == '(':
                    if not self.is_comment_or_string(position +offset-1):
                        user_data.parenthesis_count += 1

                else:
                    if not self.is_comment_or_string(position +offset-1):
                        user_data.parenthesis_count -= 1

                # after an unclosed parenthesis
                if user_data.parenthesis_count > 0:
                    # get the whole end of the line if no more parenthesis
                    if i == len(parenthesis) -1:
                        segment = line[position +offset:]
                    # else inspect until the next parenthesis
                    else:
                        segment = line[position +offset:parenthesis[i+1].end()]

                    self.set_args_format_in_segment(
                        segment,
                        position +offset,
                        include_args=self._in_def_args
                    )

                # inspect the begining of the line if self._in_def_args (as it
                # may start with 'position = (0, 0, 0)')
                if i == 0 and self._in_def_args:
                    self.set_args_format_in_segment(
                        line[:p.start()],
                        offset,
                        include_args=self._in_def_args
                    )

            # close def args (and stop including *args) if the ')' character
            # is followed by a ':'
            closing_match = self.def_closing_regex.search(line)
            if closing_match:
                if not self.is_comment_or_string(closing_match.start()):
                    self._in_def_args = False

        else:
            # look for keyword aguments in line breaks
            if user_data.parenthesis_count > 0:
                self.set_args_format_in_segment(
                    line, offset, include_args=self._in_def_args
                )

        if user_data.parenthesis_count == 0:
            self._in_def_args = False

    def set_args_format_in_segment(self, segment, offset=0, include_args=False):
        """
        Args:
            segment (str) : line segment
            offset (int, optional)
            include_args (bool, optional)

        Get all args (if included) and kwargs start and end positions from segment.
        """

        # set **kwargs format
        if 'kwargs' in self._palette:
            kwarg_matches = self.kwargs_regex.finditer(segment)

            # get keyword arguments
            for match in kwarg_matches or ():
                start = match.start(1) +offset

                if not self.is_comment_or_string(start):
                    self.set_format(
                        start,
                        match.end(1) +offset -start,
                        'kwargs'
                    )

        # set *args format
        if 'args' in self._palette and include_args:
            start = 0

            if not any(x in segment for x in '()[]{}'):
                for token in segment.split(',') or ():
                    if '=' in token:
                        start += len(token) +1
                        continue

                    if token.strip() == 'self' and 'self' in self._palette:
                        start += len(token) +1
                        continue

                    # set format if not already string or comments-formatted
                    if not self.is_comment_or_string(start +offset):
                        unfold_char = self.unfold_char_regex.match(token)

                        # handle * unfolds case
                        if unfold_char:
                            unfold_offset = unfold_char.end(1)

                            self.set_format(
                                start +offset +unfold_offset,
                                len(token) -unfold_offset,
                                'args'
                            )

                        # handle "regular" cases
                        else:
                            self.set_format(
                                start +offset,
                                len(token),
                                'args'
                            )

                    start += len(token) +1

    def is_comment_or_string(self, pos):
        """
        Args:
            pos (int)

        Returns:
            (bool)

        Check if character at <pos> is already string or comments-painted.
        """

        cformat = self.format(pos)
        for x in ('comments', 'strings'):
            if x in self._palette and cformat == self._palette[x]['format']:
                return True
        return False


class PythonHighlighter(Highlighter):
    painter_class = PythonPainter


class BlockData(QtGui.QTextBlockUserData):
    """
    Custom QTextBlockUserData for parenthesis count over QTextBlocks.
    """

    def __init__(self):
        super(BlockData, self).__init__()

        self.parenthesis_count = 0
