from custom_maya.features.highlight import HighlightHandler
from custom_maya.features.highlight.python.highlighter import PythonHighlighter


class PythonHighlight(HighlightHandler):
    _highlight_class = PythonHighlighter
