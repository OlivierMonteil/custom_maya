import os
import copy

from PySide2 import QtGui

from custom_maya import utils
from custom_maya import prefs
from custom_maya import constants as kk
from custom_maya.prefs import Attribute, clean_name, read_json

SKIPPED = ['template.json']


class PalettesCollection(Attribute):

    _instance = None

    def __init__(self, parent=None):

        self._name = 'palettes'
        self._parent = parent
        self._attributes = []

        self._data = get_contents_as_dict(kk.PALETTES_ROOT)

        for palette in self._data:
            self.add_attr(palette, self._data)

    def add_attr(self, name, data):
        attr_name = utils.to_snake_case(name)
        self.__dict__[attr_name] = Palette(name, data[name], self)


class Palette(Attribute):

    @clean_name
    def get_filtered(self, palette, profile_name):
        palette = self.getattr(palette)
        data = palette.get()

        if str(profile_name) == kk.PROFILE_NONE:
            return self.clean_data(data)

        elif profile_name in [kk.PROFILE_FULL, kk.PROFILE_OFF]:
            value = True if profile_name == kk.PROFILE_FULL else False
            for x in data:
                if 'enabled' in data[x]:
                    data[x]['enabled'] = value

        else:
            profile = self.profiles.getattr(profile_name)
            for x in profile.get():
                data[x]['enabled'] = profile.get(x)

        for x in data:
            if 'link' in data[x]:
                link = data[x]['link']
                if link:
                    data[x]['value'] = data[link]['value']

        return self.clean_data(data)

    def clean_data(self, data):
        # cleanup data
        to_remove = []
        for x in data:
            # remove disabled
            if 'enabled' in data[x] and not data[x]['enabled']:
                to_remove.append(x)
            # convert 'key': {'value': x, ...} into 'key': x
            else:
                data[x] = data[x]['value']

        for x in to_remove:
            del data[x]

        return data

def get_contents_as_dict(root, data=None):
    """
    Args:
        root (str) : absolute path
        data (dict, optional)

    Returns:
        (dict)

    Recursively parse <root> and get all
    """

    if data is None:
        data = {}

    for x in os.listdir(root):
        name = x[1:] if x.startswith('.') else x
        name = name.split('.')[0].lower().replace('-', '_')

        path = os.path.join(root, x)
        if os.path.isdir(path):
            data[name] = {}
            get_contents_as_dict(path, data[name])      # recurse
            continue
        if not x.endswith('.json') or x.startswith('_') or x in SKIPPED:
            continue

        data[name] = read_json(path)

    return data

def get_palettes_collection():
    if PalettesCollection._instance is None:
        PalettesCollection._instance = PalettesCollection()
    return PalettesCollection._instance

def get_filtered_palette(obj):
    if not hasattr(obj, 'language') or obj.language is None:
        return {}

    feature_prefs = prefs.get().features.highlight
    palette = get_palettes_collection()

    for x in obj.language.split('|'):
        feature_prefs = feature_prefs.getattr(x)
    palette = palette.getattr(x)

    raw_data = palette.get_filtered(feature_prefs.palette(), feature_prefs.profile())
    data = {}
    specific_formats = obj.specific_formats if hasattr(obj, 'specific_formats') else []

    for name in raw_data:
        fmt = specific_formats[name] if name in specific_formats else ''
        data[name] = {
            'format': char_format(raw_data[name], fmt),
            'rgb': tuple(raw_data[name])
        }

    return data

def char_format(rgb, style=''):
    """
    Args:
        rgb (tuple(int))
        style (str, optional)

    Returns:
        (QtGui.QTextCharFormat)

    Return a QtGui.QTextCharFormat with the given attributes (color, font
    weigth, etc).
    """

    if isinstance(rgb, (tuple, list)):
        color = QtGui.QColor(*rgb)
    else:
        # handle named colors as "red"
        color = QtGui.QColor()
        color.setNamedColor(rgb)

    ch_format = QtGui.QTextCharFormat()
    ch_format.setForeground(color)

    if 'bold' in style:
        ch_format.setFontWeight(QtGui.QFont.Bold)
    if 'italic' in style:
        ch_format.setFontItalic(True)

    return ch_format
