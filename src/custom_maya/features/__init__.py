"""
Auto-completion Snippets management files for Mel and Python tabs.

The custom_script_editor.managers.SnippetsManager class is used to add this
system to the Script Editor tabs QTextEdit instances.

To do:
    User-Interface for custom snippets editing (custom_snippets.json file).
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.handler import FeaturesManager


def get_manager():
    return utils.get_singleton(FeaturesManager)
