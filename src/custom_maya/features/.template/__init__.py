# shorten absolute imports
from custom_maya import utils
from custom_maya.features.template.handler import TemplateHandler
from custom_maya.features.template.manager import TemplateManager


def get_manager():
    return utils.get_singleton(TemplateManager)
