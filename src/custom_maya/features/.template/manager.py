from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.template import TemplateHandler


@utils.traceback_on_all
class TemplateManager(Manager):
    _handler_class = TemplateHandler
    _instance = None
    _name = None

    def target_widget(self):
        return None

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
