from custom_maya import utils
from custom_maya.core import Handler


@utils.traceback_on_all
class TemplateHandler(Handler):
    def __init__(self, parent=None):
        super(TemplateHandler, self).__init__(parent)

        self.txt_edit = parent

    def install(self):
        pass
