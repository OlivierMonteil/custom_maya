"""
Text blocks expand/collapse widgets. The update of the CollapseButtons is handled
into the CollapseWidget class.
"""


from custom_maya import constants as kk
from custom_maya import utils
from custom_maya.core import Handler
from custom_maya.features.line_numbers import get_line_numbers_widget


@utils.traceback_on_all
class LineNumbersHandler(Handler):
    def __init__(self, parent=None):
        super(LineNumbersHandler, self).__init__(parent)

        self._txt_edit = parent

    def install(self):
        """
        Apply line-numbers "on" parent QTextEdit.
        """

        line_nbr_widget = get_line_numbers_widget(self._txt_edit)

        # connect signals
        txt_bar = self._txt_edit.verticalScrollBar()
        txt_bar.valueChanged.connect(line_nbr_widget.update_from_txt_edit_slider)
        txt_bar.rangeChanged.connect(line_nbr_widget.update_from_txt_edit_slider)
        txt_bar.sliderMoved.connect(line_nbr_widget.update_from_txt_edit_slider)

        # set left-margin on parent QTextEdit (where this widget will over-lay).
        self._txt_edit.setViewportMargins(
            max(kk.LEFT_PADDING, self._txt_edit.viewportMargins().left()), 0, 0, 0
        )

        # override parent QTextEdit's paint event so it will update line-numbers
        # widget too
        self._txt_edit.viewport().paintEvent = utils.run_after_event(
            line_nbr_widget.paintEvent,
            self._txt_edit.viewport().paintEvent
        )

        line_nbr_widget.show()
