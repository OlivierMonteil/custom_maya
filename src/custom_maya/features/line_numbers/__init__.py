# shorten absolute imports
from custom_maya import utils
from custom_maya.features.line_numbers.widget import get_line_numbers_widget
from custom_maya.features.line_numbers.handler import LineNumbersHandler
from custom_maya.features.line_numbers.manager import LineNumbersManager


def get_manager():
    return utils.get_singleton(LineNumbersManager)
