try:
    from PySide2 import QtWidgets, QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore
    from PySide import QtGui as QtWidgets

from custom_maya import constants as kk
from custom_maya import utils


class LineNumbersWidget(QtWidgets.QWidget):
    """ Simple widget used for line numbers painting. """

    _background_color = QtGui.QColor(255, 255, 255, 10)
    _alternate_color = QtGui.QColor(255, 255, 255, 8)
    _color = QtGui.QColor(255, 255, 255, 100)

    def __init__(self, txt_edit):
        super(LineNumbersWidget, self).__init__(txt_edit)

        self.txt_edit = txt_edit

        self.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        # set font on both QTextEdits at startup, so we are sure that both sizes
        # will match
        font = self.get_font()
        font.setFamily(self.txt_edit.font().family())
        font.setPixelSize(12)
        font.setHintingPreference(font.PreferVerticalHinting)
        self.setFont(font)

        self.txt_edit.cursorPositionChanged.connect(self.update)

    def sizeHint(self):
        return QtCore.QSize(self.txt_edit.viewportMargins().left(), kk.INF_HEIGHT)

    def get_font(self):
        return self.txt_edit.currentFont()

    def paintEvent(self, event):
        """
        Qt re-implementation.
        Paint line numbers on widget using self.txt_edit data, as first visible
        block et block sizes.
        """

        painter = QtGui.QPainter(self)
        painter.save()

        painter.fillRect(event.rect(), self._background_color)
        painter.setPen(self._color)

        block = utils.get_first_visible_block(self.txt_edit)
        block_rect = utils.get_block_bounding_rect(self.txt_edit, block)
        top = block_rect.top()
        bottom = top + block_rect.height()
        block_num = block.blockNumber()

        left_margin = self.txt_edit.viewportMargins().left()
        height = None

        px_size = self.get_font().pixelSize()
        font = painter.font()
        font.setPixelSize(px_size)
        painter.setFont(font)

        selected_block = self.txt_edit.textCursor().block()

        while block.isValid() and top <= event.rect().bottom():
            if block.isVisible() and bottom >= event.rect().top():
                if height is None:
                    height = utils.get_block_bounding_rect(self.txt_edit, block).height()    

                # paint alternate color on selected line
                if block == selected_block:
                    painter.fillRect(
                        0,
                        top,
                        left_margin,
                        height,
                        self._alternate_color
                    )

                # paint line number
                painter.drawText(
                    0, top +(height -px_size)//2, left_margin -5, height,
                    QtCore.Qt.AlignRight,
                    str(block_num + 1)
                )

            block = block.next()
            top = bottom
            bottom = top + utils.get_block_bounding_rect(self.txt_edit, block).height()
            block_num += 1

        painter.restore()

    def update_from_txt_edit_slider(self, *args):
        self.update()


class PlainTextLineNumbersWidget(LineNumbersWidget):
    def get_font(self):
        return self.txt_edit.document().defaultFont()


def get_line_numbers_widget(txt_edit):
    if isinstance(txt_edit, QtWidgets.QTextEdit):
        return LineNumbersWidget(txt_edit)
    elif isinstance(txt_edit, QtWidgets.QPlainTextEdit):
        return PlainTextLineNumbersWidget(txt_edit)