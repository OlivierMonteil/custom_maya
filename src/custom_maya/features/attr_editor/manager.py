from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.attr_editor import AttrEditorHandler


@utils.traceback_on_all
class AttrEditorManager(Manager):
    _handler_class = AttrEditorHandler
    _instance = None
    _name = None

    def target_widget(self):
        return utils.get_attribute_editor().widget

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
