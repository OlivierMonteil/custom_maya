from PySide2 import QtWidgets

from custom_maya.features.attr_editor import constants as ae_kk


class AEWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AEWidget, self).__init__(parent)

        self.setObjectName(ae_kk.AE_CUSTOM_WIDGET_NAME)

        lay = QtWidgets.QGridLayout(self)
        self.non_default_box = QtWidgets.QCheckBox('non-default only', self)

        lay.addWidget(self.non_default_box)

        for widget in (self, lay):
            widget.setContentsMargins(0, 0, 0, 0)
