from maya import mel, cmds

from custom_maya import utils

from custom_maya.features.attr_editor import constants as ae_kk
from custom_maya.features.attr_editor.plugins.core import get_template_from_plugins

CLASSES =  {
    'attrEnumOptionMenuGrp': cmds.attrEnumOptionMenuGrp,
    'optionMenuGrp': cmds.optionMenuGrp,
    'attrColorSliderGrp': cmds.attrColorSliderGrp,
    'attrControlGrp': cmds.attrControlGrp,
    'attrFieldGrp': cmds.attrFieldGrp,
    'attrFieldSliderGrp': cmds.attrFieldSliderGrp,
    'attrNavigationControlGrp': cmds.attrNavigationControlGrp,
    'checkBoxGrp': cmds.checkBoxGrp,
    'colorIndexSliderGrp': cmds.colorIndexSliderGrp,
    'colorSliderButtonGrp': cmds.colorSliderButtonGrp,
    'colorSliderGrp': cmds.colorSliderGrp,
    'floatFieldGrp': cmds.floatFieldGrp,
    'floatSliderButtonGrp': cmds.floatSliderButtonGrp,
    'floatSliderGrp': cmds.floatSliderGrp,
    'intFieldGrp': cmds.intFieldGrp,
    'intSliderGrp': cmds.intSliderGrp,
    'radioButtonGrp': cmds.radioButtonGrp,
    'textFieldButtonGrp': cmds.textFieldButtonGrp,
    'textFieldGrp': cmds.textFieldGrp,
    'checkBox': cmds.checkBox,
    'text': cmds.text
}

SKIP_TYPES = [
    'field',
    'floatField',
    'floatSlider',
    'symbolButton',
    'popupMenu',
    'canvas',
    'separator',
    'radioButton',
    'timeField',
    'button',
    'iconTextButton',
    'cmdScrollField',
    'intSlider',
    'intField',
    'ImageSwatch',
    'port3D',
    'frameLayout'
]


class AEDataBaseClass(object):

    _child_class = None

    def __init__(self):

        self.children = []
        self._hidden = False
        self._complete = False
        self._fully_complete = False

    def __getitem__(self, name):
        for child in self:
            if child.name == name:
                return child

    def __iter__(self, *args, **kwargs):
        return self.children.__iter__(*args, **kwargs)

    def __reversed__(self, *args, **kwargs):
        return self.children.__reversed__(*args, **kwargs)

    def __contains__(self, name):
        return True if any(child.name == name for child in self) else False

    def get_child(self, name, *args, **kwargs):
        if not name in self:
            child = self._child_class(name, *args, **kwargs)
            self.children.append(child)
            child.parse_children()
            return child
        else:
            return self[name]

    def cleanup(self):
        if self._child_class is None:
            return

        to_delete = []

        for child in reversed(self):
            if not child.exists():
                self.children.remove(child)

    def exists(self):
        return True

    def parse_children(self):
        return

    def is_fully_complete(self):
        return self._fully_complete

    def is_complete(self):
        return self._complete

    def get_controls(self):
        controls = []

        for child in self:
            if isinstance(child, Control):
                controls.append(child)
            else:
                controls.extend(child.get_controls())

        return controls

    def check_completion(self):
        self._complete = bool(self.children)
        for child in self:
            child.check_completion()

        self._fully_complete = self._complete and all(child.is_fully_complete() for child in self)

    def shortname(self):
        return self.name.split('|')[-1]

    def is_hidden(self):
        return self._hidden


@utils.traceback_on_all
class Control(AEDataBaseClass):

    _child_class = None

    def __init__(self, name, attribute, node_type, is_layout=False):
        super(Control, self).__init__()

        self.name = name
        self.node_type = node_type
        self.attr = attribute
        self.is_layout = is_layout
        self.cls = cmds.layout if self.is_layout else cmds.control

        self.height = self.cls(self.name, q=True, h=True) or 1

        node = get_current_ae_node()

        self.attr_type = cmds.getAttr('%s.%s' % (node, self.attr), typ=True)
        self.default = utils.to_simple_dimension(
            cmds.attributeQuery(self.attr, typ=self.node_type.name, listDefault=True)
        )

    def is_default(self, node):
        if cmds.listConnections('%s.%s' % (node, self.attr), s=True, d=False):
            return False
        return utils.is_same_data(self.get_value(node), self.default)

    def get_value(self, node):
        if cmds.attributeQuery(self.attr, n=node, message=True):
            return self.default
        return utils.to_simple_dimension(cmds.getAttr('%s.%s' % (node, self.attr)))

    def __str__(self):
        return '    - %s' % self.attr

    def is_complete(self):
        return True

    def is_fully_complete(self):
        return True

    def check_completion(self):
        return

    def update_height(self):
        height = 1 if self._hidden else self.height
        self.cls(self.name, e=True, h=height)

    def set_visible(self, state):
        self.cls(self.name, e=True, vis=state)
        self._hidden = not state
        self.update_height()


@utils.traceback_on_all
class Category(AEDataBaseClass):

    _child_class = Control

    def __init__(self, name, node_type):
        super(Category, self).__init__()

        self.name = name
        self.label = cmds.frameLayout(self.name, q=True, l=True)
        self.node_type = node_type
        self.evaluated = False
        self.category_attributes_from_json = {}
        self.child_categories = []

        self.height = None

        if node_type.template_from_plugin:
            if self.label in node_type.template_from_plugin:
                self.category_attributes_from_json = node_type.template_from_plugin[self.label]

        if self.label in ae_kk.JSON_ATTRIBUTES['common-categories']:
            for label, attr in ae_kk.JSON_ATTRIBUTES['common-categories'][self.label].items():
                self.category_attributes_from_json[label.lower()] = attr

    def parse_children(self):
        if cmds.frameLayout(self.name, q=True, cl=True):
            return

        if not self.evaluated:
            for layout in cmds.frameLayout(self.name, q=True, ca=True) or ():
                self.get_controls_under(layout)

        if self.children:
            self.evaluated = True
            self.height = cmds.frameLayout(self.name, q=True, h=True)

    def update_height(self):
        if self.height is None:
            return

        if cmds.frameLayout(self.name, q=True, cl=True):
            cmds.frameLayout(self.name, e=True, h=1)

        height = self.height
        for ctrl in self:
            if ctrl.is_hidden():
                height -= ctrl.height -1

        cmds.frameLayout(self.name, e=True, h=height)

    def add_child_category(self, category):
        self.child_categories.append(category)

    def get_controls_under(self, obj):
        try:
            ui_type = cmds.objectTypeUI(obj)
        except:
            obj = obj.split('|')[-1]
            try:
                ui_type = cmds.objectTypeUI(obj)
            except:
                return

        if ui_type in SKIP_TYPES:
            return

        if 'layout' in ui_type.lower():
            for child in cmds.layout(obj, q=True, childArray=True) or ():
                self.get_controls_under(obj +'|' +child)

        else:
            try:
                if ui_type == 'staticText':
                    label = cmds.text(obj, q=True, label=True)
                    parent = cmds.text(obj, q=True, p=True)

                    while cmds.objectTypeUI(parent) != 'columnLayout':
                        obj = parent
                        parent = cmds.control(obj, q=True, p=True)

                    parent = cmds.control(parent, q=True, fpn=True)

                    attr = self.get_attr_name_from_label(label)
                    self.get_child(obj, attr, self.node_type, is_layout=True)

                else:
                    control_type = get_type_from_controls(obj)
                    if control_type:
                        label = self.get_label(obj, control_type)

                        if isinstance(label, list):
                            for x in label:
                                attr = self.get_attr_name(obj, x, control_type)
                                self.get_child(obj, attr, self.node_type)
                        else:
                            attr = self.get_attr_name(obj, label, control_type)
                            self.get_child(obj, attr, self.node_type)

                    else:
                        print(ui_type)

            except Exception as e:
                # print ('# Error : Category.get_controls_under :', e.message.strip())
                pass

    def get_label(self, obj, control_type):
        if control_type == 'checkBoxGrp':
            label = cmds.checkBoxGrp(obj, q=True, labelArray4=True)
            label = [x for x in label if x]
            if len(label) == 1:
                label = label[0]

        else:
            label = CLASSES[control_type](obj, q=True, label=True)
        return label

    def get_attr_name(self, obj, label, control_type):
        attr = self.get_attr_name_from_label(label)
        if not attr:
            try:
                attr = CLASSES[control_type](obj, q=True, attribute=True)
                attr = '.'.join(attr.split('.')[1:])
            except Exception as e:
                return

        return attr

    def get_child(self, obj, attr, node_type, *args, **kwargs):
        if not attr:
            return

        return super(Category, self).get_child(obj, attr, node_type, *args, **kwargs)

    def get_attr_name_from_label(self, label):
        if label.lower() in self.category_attributes_from_json:
            return self.category_attributes_from_json[label.lower()]

        if label.lower() in self.node_type.attr_nice_names:
            idx = self.node_type.attr_nice_names.index(label.lower())
            attr = self.node_type.attributes[idx]
            return attr

    def exists(self):
        return cmds.frameLayout(self.name, q=True, ex=True)

    def __str__(self):
        result = '  - %s  (%s)' % (self.label, self.is_fully_complete())
        for ctrl in self:
            result += '\n' +str(ctrl)
        return result

    def set_visible(self, state):
        cmds.frameLayout(self.name, e=True, vis=state)
        self._hidden = not state


@utils.traceback_on_all
class NodeType(AEDataBaseClass):

    _child_class = Category

    def __init__(self, name):
        super(NodeType, self).__init__()

        self.name = name

        self.attributes = cmds.attributeInfo(t=self.name, all=True)
        self.attr_nice_names = [
            x.lower()
            for x in cmds.attributeInfo(t=self.name, all=True, ui=True)
        ]

        if self.name in ae_kk.JSON_ATTRIBUTES:
            for label, attr in ae_kk.JSON_ATTRIBUTES[self.name].items():
                if not label in self.attr_nice_names:
                    self.attr_nice_names.append(label.lower())
                    self.attributes.append(attr)

        self.template_from_plugin = get_template_from_plugins(name)

    def get_categories_under(self, obj):
        try:
            ui_type = cmds.objectTypeUI(obj)
        except:
            obj = obj.split('|')[-1]
            try:
                ui_type = cmds.objectTypeUI(obj)
            except:
                return

        if ui_type == 'frameLayout':
            parent = None
            for category in reversed(self.children):
                if category.name in obj and category.name != obj:
                    parent = category
                    break

            child = self.get_child(obj, self)
            if parent:
                parent.add_child_category(child)

        try:
            for child in cmds.layout(obj, q=True, childArray=True) or ():
                self.get_categories_under(obj + '|' +child)

        except Exception as e:
            # print('# Error : NodeType.get_categories_under :', e.message.strip())
            pass

    def __str__(self):
        result = self.name
        for category in self:
            result += '\n' +str(category)
        return result

    def parse_children(self):
        if self.is_fully_complete():
            return

        if not self.children:
            selected_tab = cmds.tabLayout(ae_kk.AE_CONTROL_FORM_LAYOUT, q=True, st=True)
            self.get_categories_under(selected_tab)

        else:
            for category in self.children:
                if not category.is_fully_complete():
                    self.get_categories_under(category.name)
                    category.parse_children()


class AEData(AEDataBaseClass):

    _child_class = NodeType

    def __init__(self):
        super(AEData, self).__init__()

        self._recording = False
        self.node = None
        self.node_type = None
        self.category = None

    def get_visible_layout_from_stack(self):
        for x in cmds.layout(ae_kk.AE_STACK_LAYOUT, q=True, ca=True):
            if cmds.layout(x, q=True, vis=True):
                return cmds.control(ae_kk.AE_STACK_LAYOUT, q=True, fpn=True) +'|' +x

    # @utils.chrono
    def parse_children(self):
        layout = self.get_visible_layout_from_stack()
        if layout == ae_kk.AE_NOTHING_SELECTED:
            return

        current_node = get_current_ae_node()
        if not current_node or not cmds.objExists(current_node):
            return

        current_node_data = self.get_child(cmds.nodeType(current_node))

        self.cleanup()
        current_node_data.check_completion()

        if not current_node_data.is_fully_complete():
            current_node_data.parse_children()

        return current_node_data

    def __str__(self):
        result = ''
        for node in self:
            result += '\n' +str(node)
        return result


def get_type_from_controls(obj):
    for t in CLASSES:
        if is_from_UI_type(obj, t):
            return t

def is_from_UI_type(name, ui_type):
        try:
            return cmds.objectTypeUI(name, isType=ui_type)
        except:
            return False

def get_current_ae_node():
    return mel.eval('$gAECurrentTab = $gAECurrentTab')


if __name__ == '__main__':
     ae_data = AEData()
     ae_data.parse_children()
