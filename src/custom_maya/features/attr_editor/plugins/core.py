from custom_maya.features.attr_editor.plugins import MODULES

def get_template_from_plugins(node_type):
    for mod in MODULES:
        if not hasattr(mod, 'node_types'):
            continue
        if not hasattr(mod, 'get_template'):
            continue

        if node_type in mod.node_types():
            template = mod.get_template(node_type)
            result = {}

            for category in template:
                result[category] = {}
                for label in template[category]:
                    result[category][label.lower()] = template[category][label]

            return result
