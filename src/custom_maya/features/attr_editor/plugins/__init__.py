import os

SKIPPED_FILES = ('__init__.py', 'core.py')


def get_modules():
    modules = []
    root = os.path.dirname(__file__).replace('\\', '/')
    plugins_root = 'custom_maya.' +'.'.join(root.split('custom_maya/')[-1].split('/'))

    for x in os.listdir(root):
        if not x.endswith('.py') or x in SKIPPED_FILES:
            continue

        try:
            mod_path = plugins_root +'.' +x[:-3]
            mod = __import__(mod_path, locals(), globals(), [plugins_root])
            modules.append(mod)

        except Exception as e:
            print (e)
            continue

    return modules


MODULES = get_modules()
