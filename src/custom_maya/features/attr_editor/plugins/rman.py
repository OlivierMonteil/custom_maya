from rfm2.config import cfg


def get_template(node_type):
    result = {}

    desc = cfg().node_desc_dict[node_type]
    param_dict = desc.param_dict

    for attr in param_dict or ():
        category = getattr(desc.get_param_desc(attr), 'page', None)
        if not category:
            continue

        category = category.split('|')[-1]

        if not category in result:
            result[category] = {}

        label = getattr(desc.get_param_desc(attr), 'label', None)
        if not label:
            continue
        result[category][label] = attr

    return result

def node_types():
    return cfg().node_desc_dict.keys()
