# shorten absolute imports
from custom_maya import utils
from custom_maya.features.attr_editor.widget import AEWidget
from custom_maya.features.attr_editor.handler import AttrEditorHandler
from custom_maya.features.attr_editor.manager import AttrEditorManager


def get_manager():
    return utils.get_singleton(AttrEditorManager)
