import os
import json

AE_WIDGET_NAME = 'AttributeEditor'
AE_ROOT_LAYOUT = 'AErootLayoutPane'
AE_STACK_LAYOUT = 'AEStackLayout'
AE_CONTROL_FORM_LAYOUT = 'AEcontrolFormLayout'
AE_NOTHING_SELECTED = 'AENothingSelectedLayout'

AE_CUSTOM_WIDGET_NAME = 'customMayaAEWidget'

JSON_ATTRIBUTES_FILE = os.path.join(os.path.dirname(__file__), 'attributes.json')

def get_attributes_from_json():
    content = {'common-categories': []}
    try:
        with open(JSON_ATTRIBUTES_FILE, 'r') as opened_file:
            content = json.load(opened_file)
    finally:
        return content

JSON_ATTRIBUTES = get_attributes_from_json()
