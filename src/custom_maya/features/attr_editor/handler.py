from PySide2 import QtWidgets
import shiboken2 as shiboken

from maya.OpenMayaUI import MQtUtil
from maya import cmds, mel

from custom_maya import utils
from custom_maya.core import Handler

from custom_maya.features.attr_editor import constants as ae_kk
from custom_maya.features.attr_editor import data
from custom_maya.features.attr_editor import AEWidget


@utils.traceback_on_all
class AttrEditorHandler(Handler):

    def __init__(self, *args, **kwargs):
        super(AttrEditorHandler, self).__init__(*args, **kwargs)

        self._data = data.AEData()

    def install(self):
        # get AttributeEditor's widget
        ae_widget = utils.get_attribute_editor().widget
        if not ae_widget:
            return None

        # get AttributeEditor's root layout
        ae_root_layout = None

        for lay in ae_widget.findChildren(QtWidgets.QVBoxLayout):
            if lay.objectName() == ae_kk.AE_ROOT_LAYOUT:
                ae_root_layout = lay
                break

        if not ae_root_layout:
            return None

        if ae_widget.findChild(QtWidgets.QWidget, ae_kk.AE_CUSTOM_WIDGET_NAME):
            return None

        self.widget = AEWidget(ae_widget)
        self.widget.setParent(ae_widget)
        self.widget.show()
        ae_root_layout.insertWidget(0, self.widget)

        # update on selection change
        cmds.scriptJob(
            event=['SelectionChanged', self.update_ae],
            parent=ae_kk.AE_CUSTOM_WIDGET_NAME
        )
        # update on Attribute Editor's tab change
        cmds.tabLayout('AEtabLayout', e=True, selectCommand=self.update_ae)
        self.widget.non_default_box.stateChanged.connect(self.update_ae)

    def update_ae(self, *args):
        current_ae_node = mel.eval('$gAECurrentTab = $gAECurrentTab')
        if not current_ae_node:
            return

        node_data = self._data.parse_children()
        if not node_data:
            return

        for category in node_data.children:
            cmds.frameLayout(category.name, e=True, ec=self.update_ae)

        self.update_controls(current_ae_node, node_data)

    def update_controls(self, node, node_data):
        hide_default = self.widget.non_default_box.isChecked()

        for ctrl in node_data.get_controls():
            if hide_default:
                if ctrl.is_default(node):
                    ctrl.set_visible(False)
            else:
                ctrl.set_visible(True)

        for category in reversed(node_data.children):
            children = category.children +category.child_categories
            if children:
                if all(x.is_hidden() for x in children):
                    category.set_visible(False)
                else:
                    category.set_visible(True)
                    # category.update_height()

            else:
                if hide_default and category.label == 'Extra Attributes':
                    category.set_visible(False)

            if hide_default and category.label == 'UUID':
                category.set_visible(False)
