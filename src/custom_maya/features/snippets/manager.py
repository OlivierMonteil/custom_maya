from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.snippets import MelSnippetsHandler, PythonSnippetsHandler


@utils.traceback_on_all
class SnippetsManager(Manager):
    _handler_class = None
    _instance = None
    _name = None
    _window_titles = ('Script Editor', 'Expression Editor')

    def apply(self, *args):
        created = []

        for txt_edit, language in self.target_widget():
            self._handler_class = MelSnippetsHandler if language == 'mel' else \
                                  PythonSnippetsHandler

            if not txt_edit or not self.is_needed(txt_edit):
                continue

            handler = self._handler_class(txt_edit)
            handler.install()
            created.append(handler)

        return created

    def target_widget(self):
        yield utils.get_expression_txt_edit(), 'mel'
        tab = utils.get_script_editor().get_selected_tab()
        if tab:
            yield tab.txt_edit, tab.language

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
