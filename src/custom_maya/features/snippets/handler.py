"""
Snippets base classes and methods.
"""

import json
import os
import sys
import re

from PySide2 import QtWidgets, QtCore, QtGui

from maya import cmds
import maya.OpenMayaUI as OMUI

import shiboken2 as shiboken

from custom_maya import constants as kk
from custom_maya import utils
from custom_maya.stylesheet import apply_style
from custom_maya.core import Handler


CUSTOM_JSON = os.path.dirname(__file__).replace('\\', '/') +'/custom_snippets.json'
LAST_WORD_REGEX = re.compile(r'\b([\w\.]+)$')


@utils.traceback_on_all
class SnippetsHandler(Handler):

    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    def __init__(self, text_edit):
        super(SnippetsHandler, self).__init__(parent=text_edit)

        self._txt_edit = text_edit
        self._snippets_box = None
        self._trigger_on_cursor_change = True
        self._box = None
        self._enabled = False
        self._type = None

        self._txt_edit.cursorPositionChanged.connect(self.on_cursor_change)

    def set_enabled(self, state):
        self._enabled = state

    def install(self):
        self._txt_edit.installEventFilter(self)

    def on_ctrl_signal(self, state):
        self.set_enabled(state)

    def is_enabled(self):
        return self._enabled

    def eventFilter(self, obj, event):
        # close snippet box on RMB and MMB
        # (LMB seems to be handled directly into QTextCursor class)

        event_type = event.type()

        # close snippet box on mouse click
        if event_type == QtCore.QEvent.MouseButtonPress:
            self._kill_box()
            return False

        # handle snippet box on key press events
        if event_type == QtCore.QEvent.KeyPress:
            key = event.key()
            text = event.text()
            # skip if key is not a char key
            if not text or not text in kk.CHARACTERS +kk.SPECIAL_CHARS:
                if not key in [
                    QtCore.Qt.Key_Return,
                    QtCore.Qt.Key_Down,
                    QtCore.Qt.Key_Up,
                    QtCore.Qt.Key_Backspace
                ]:
                    return False

            if not self.is_enabled():
                self._kill_box()
                return False

            self._trigger_on_cursor_change = False

            cursor = self._txt_edit.textCursor()

            if self._box:
                if key == QtCore.Qt.Key_Return:
                    self.validate_snippet(self._box.current_item().text())
                    return True

                if key == QtCore.Qt.Key_Down:
                    self._box.go_down()
                    return True
                if key == QtCore.Qt.Key_Up:
                    self._box.go_up()
                    return True

                # close snippet box (will be reconstructed later if necessary)
                self._kill_box()

            to_lower = True
            if event.modifiers():
                if event.modifiers() == QtCore.Qt.ShiftModifier:
                    to_lower = False
                else:
                    return False

            key_as_str = QtGui.QKeySequence(key).toString()
            key_as_str = key_as_str.lower() if to_lower else key_as_str

            # skip if key as string is more than one character (ex: space)
            # except for Backspace, so the snippets would still be triggered
            if key != QtCore.Qt.Key_Backspace and len(key_as_str) > 1:
                return False

            cursor = self._txt_edit.textCursor()
            line = cursor.block().text()

             # on alphabetical characters
            if re.match('[a-zA-Z_]', key_as_str) or key_as_str.lower() in ['.', 'backspace']:
                pos = cursor.positionInBlock()
                if key_as_str.lower() == 'backspace':
                    pos = max(0, pos -1)
                    key_as_str = ''

                found_snippets = self.get_snippets(line, pos, key_as_str)

                # if snippets have been found, show SnippetBox under current word
                if found_snippets:
                    rect = self._txt_edit.cursorRect()
                    pos = QtCore.QPoint(rect.x() +35, rect.y() +15)

                    self._box = SnippetBox(self._txt_edit, found_snippets, pos)
                    self._box.itemPressed.connect(lambda x: self.validate_snippet(x.text()))
                    self._box.show()

        return False

    def validate_snippet(self, text):
        """
        Args:
            text (str)

        Insert selected completion into QTextEdit and close the snippets box.
        """

        cursor = self._txt_edit.textCursor()
        pos = cursor.position()
        cursor.movePosition(cursor.StartOfWord, cursor.MoveAnchor)

        if cursor.position() == pos:
            cursor.movePosition(cursor.PreviousWord, cursor.MoveAnchor)

        cursor.movePosition(cursor.EndOfWord, cursor.KeepAnchor)
        cursor.insertText(text)

        cmds.evalDeferred(self._kill_box, lowestPriority=True)

    def _kill_box(self):
        """
        Close the snippets box.
        """

        if not self._box is None:
            self._box.close()

        self._box = None

    def get_custom_snippets(self):
        """
        Returns:
            (list[str] or [])

        Get user snippets from custom_snippets.json.
        """

        if self._type is None:
            return []

        try:
            with open(CUSTOM_JSON, 'r') as opened_file:
                content = json.load(opened_file)
                return content[self._type]
        except:
            return []

    def _get_tokens_before_key(self, line, i, key):
        match = LAST_WORD_REGEX.search(line[:i])
        if not match:
            return None, None

        word_before_key = match.group(1)
        word_to_key = word_before_key.split('.')[-1] +key

        return word_before_key, word_to_key

    def _get_matching_snippets(self, snippets, word_to_key):
        snippets = list(set(snippets))

        return [
            s for s in snippets
            if s.lower().startswith(word_to_key.lower())
            and word_to_key.lower() != s.lower()
        ]

    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        Needs to be re-implemented.
        """

        return []

    def _get_text_snippets(self):
        """
        Returns:
            (list[str])

        Return words from current tab with length > 3.

        TO DO: buffer system rather than parsing the whole text at each key press.
        """

        text = self._txt_edit.toPlainText()
        return [x for x in re.findall('\w+', text) if len(x) >3]

    def on_cursor_change(self):
        """ Close the snippets box if users clicks into the QTextEdit. """
        if self._trigger_on_cursor_change:
            if self._box:
                self._kill_box()

        else:
            self._trigger_on_cursor_change = True


@utils.traceback_on_all
class MelSnippetsHandler(SnippetsHandler):

    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    maya_cmds = list(set(cmds.help('[a-z]*', list=True, lng='mel')))

    def __init__(self, txt_edit):
        super(MelSnippetsHandler, self).__init__(txt_edit)

        self._type = 'mel'

    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        """

        _, word_to_key = self._get_tokens_before_key(line, i, key)

        if not word_to_key:
            return None

        snippets  = self.maya_cmds
        snippets += self._get_text_snippets()
        snippets += self.get_custom_snippets()

        return self._get_matching_snippets(snippets, word_to_key)


@utils.traceback_on_all
class PythonSnippetsHandler(SnippetsHandler):
    """
    Custom eventFilter installed on Maya's Script Editor to handle snippets.
    """

    maya_cmds = list(set(cmds.help('[a-z]*', list=True, lng='python')))
    cmds_regex = re.compile(r'(\bcmds|mc)\.\w+$')

    class_regex = QtCore.QRegExp('class\s+(\w+)')
    def_regex = QtCore.QRegExp('def\s+(\w+)')

    def __init__(self, txt_edit):
        super(PythonSnippetsHandler, self).__init__(txt_edit)

        self._type = 'python'
        self.python_errors = [
            word for word in __builtins__
            if any(x in word.lower() for x in ('error', 'exception'))
        ]

    def get_custom_snippets(self, line_to_key):
        """
        Args:
            line_to_key (str)

        Returns:
            (list[str])

        Get exception and double underscore snippets.
        """

        word_to_key = line_to_key.split(' ')[-1]

        # get "python" custom snippets from ./custom_snippets.json file
        custom_snippets = super(PythonSnippetsHandler, self).get_custom_snippets()
        custom_snippets += self._get_exception_snippets(line_to_key)
        custom_snippets += self._get_double_underscore_snippets(word_to_key)

        return custom_snippets

    def _get_double_underscore_snippets(self, word_to_key):
        """
        Args:
            word_to_key (str)

        Returns:
            (list[str] or [])

        Get double underscore snippets if <word_to_key> starts with an "_" char .
        """

        if word_to_key.startswith('_'):
            return ['__main__', '__name__', '__class__', '__init__']

        return []

    def _get_exception_snippets(self, line_to_key):
        """
        Args:
            word_to_key (str)

        Returns:
            (list[str] or [])

        Get "Exception" snippets after "except".
        """

        words_before_key = re.findall('\w+\.*\w*', line_to_key)
        standard_except = 'except Exception as e:'

        if len(words_before_key) == 1:
            if standard_except.startswith(words_before_key[0]):
                return [standard_except]

        if len(words_before_key) != 2 or words_before_key[0] != 'except':
            return []

        to_lower = words_before_key[1].lower()
        errors = [
            x for x in self.python_errors if x.lower().startswith(to_lower)
        ]

        return sorted([x +' as e:' for x in errors])

    def get_snippets(self, line, i, key):
        """
        Args:
            line (str)
            i (int)
            key (str)

        Returns:
            (list[str] or [])

        Get snippets list for current word.
        """

        word_before_key, word_to_key = self._get_tokens_before_key(line, i, key)

        if not word_to_key:         # for instance, on backspace erasing the first
            return None         # character of a word, we don't want all snippets to rise

        snippets = self.maya_cmds if self.cmds_regex.search(line[:i+1]) else []
        snippets += self._get_dir_snippets(word_before_key)
        snippets += self._get_text_snippets()
        snippets += self._get_modules_snippets()

        if snippets:
            snippets = list(set(snippets))      # filter duplicate snippets
            snippets = sorted(snippets)         # sort snippets

        snippets += self.get_custom_snippets(line[:i] +key)
        snippets += self._get_super_snippets()

        # get snippet words that start with word_to_key (no case match)
        return self._get_matching_snippets(snippets, word_to_key)

    def _get_super_snippets(self):
        """
        Args:
            word (str)

        Get super(Class, self).func expression snippets.
        """

        cursor = self._txt_edit.textCursor()

        class_name = self._get_previous_declaration_name(cursor, 'class')
        def_name = self._get_previous_declaration_name(cursor, 'def')

        short_super = 'super({}, self)'.format(class_name)
        long_super = 'super({}, self).{}('.format(class_name, def_name)

        result = [short_super, long_super] if class_name and def_name else \
                 [short_super] if class_name and not def_name else \
                 []

        return result

    def _get_previous_declaration_name(self, cursor, decl_str):
        """
        Args:
            cursor (QTextCursor)
            decl_str (str) : "class" or "def"

        Retrieve current "class" or "def" name from previous lines.
        """

        doc = cursor.document()

        regex = self.class_regex if decl_str == 'class' else self.def_regex
        search_cursor = doc.find(regex, cursor.position(), doc.FindBackward)

        line = search_cursor.block().text()
        if not line:
            return None

        return line.split(decl_str)[1].split('(')[0].split(':')[0].strip()

    def _get_modules_snippets(self):
        """
        Returns:
            (list[str])

        Get modules snippets from sys.
        """

        imported_modules = [x.split('.')[0] for x in sys.modules]
        return list(set(imported_modules))

    def _get_dir_snippets(self, word):
        """
        Returns:
            (list[str])

        Get dir(currentWord) as snippets ( removing the last '\.\w+' part )
        if error return empty list so the other snippets methods will be called.
        """

        if '.' in word:
            word = '.'.join(word.split('.')[:-1])

        try:
            return dir(word)
        except:
            return []


class SnippetBox(QtWidgets.QWidget):

    """
    Custom QWidget with QListWidget that will hold completion words, and arrow keys loop
    (up at row 0 will set selection at the end and reverse)
    """

    itemPressed = QtCore.Signal(QtWidgets.QListWidgetItem)

    def __init__(self, parent, list, pos):
        super(SnippetBox, self).__init__(parent)

        self.setStyleSheet('border : transparent;')
        apply_style(self)

        lay = QtWidgets.QVBoxLayout(self)
        self.view = QtWidgets.QListWidget()
        lay.addWidget(self.view)

        for word in list:
            self.add_item(word)

        self.move(pos)
        self.set_current_item(self.item(0))

        for w in [self, self.layout()]:
            w.setContentsMargins(0, 0, 0, 0)

        view_width = self.size_hint_for_column(0) + 2 * self.frame_width()
        view_height = self.size_hint_for_row(0) * self.count() + 2 * self.frame_width()
        self.setMaximumSize(view_width +16, view_height +2)

        self.view.itemPressed.connect(self.itemPressed.emit)

    def add_item(self, name):
        return self.view.addItem(name)

    def current_item(self):
        return self.view.currentItem()

    def set_current_item(self, item):
        return self.view.setCurrentItem(item)

    def item(self, index):
        return self.view.item(index)

    def frame_width(self):
        return self.view.frameWidth()

    def size_hint_for_column(self, col):
        return self.view.sizeHintForColumn(col)

    def size_hint_for_row(self, row):
        return self.view.sizeHintForRow(row)

    def set_current_row(self, row):
        return self.view.setCurrentRow(row)

    def current_row(self):
        return self.view.currentRow()

    def count(self):
        return self.view.count()

    def go_down(self):
        """
        Go an item down. If current item is already the last one, got to the top.
        """

        currRow = self.current_row()
        if currRow +1 < self.count():
            self.set_current_row(currRow +1)
        else:
            self.set_current_row(0)

    def go_up(self):
        """
        Go an item up. If current item is already the first one, got to the bottom.
        """

        currRow = self.current_row()
        if currRow -1 > 0:
            self.set_current_row(currRow -1)
        else:
            self.set_current_row(self.count() -1)
