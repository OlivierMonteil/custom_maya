# shorten absolute imports
from custom_maya import utils
from custom_maya.features.prefs_editor.handler import PrefsEditorHandler
from custom_maya.features.prefs_editor.manager import PrefsEditorManager


def get_manager():
    return utils.get_singleton(PrefsEditorManager)
