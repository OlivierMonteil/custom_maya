from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.prefs_editor import PrefsEditorHandler


@utils.traceback_on_all
class PrefsEditorManager(Manager):
    _handler_class = PrefsEditorHandler
    _instance = None
    _name = None

    def target_widget(self):
        return utils.get_script_editor().console_txt_edit

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
