from custom_maya import utils
from custom_maya.core import Handler

from custom_maya import logger

log = logger.get()


@utils.traceback_on_all
class PrefsEditorHandler(Handler):
    def __init__(self, parent=None):
        super(PrefsEditorHandler, self).__init__(parent)

        self.txt_edit = parent

    def install(self):
        pass

    def on_ctrl_signal(self, *args):
        log.info('Prefs-editor is not implemented yet.')
