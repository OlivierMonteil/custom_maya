"""
Text blocks expand/collapse widgets. The update of the CollapseButtons is handled
into the CollapseWidget class.
"""


from PySide2 import QtCore, QtGui

from custom_maya import utils
from custom_maya import constants as kk
from custom_maya.core import Handler
from custom_maya.features.blocks_collapse import CollapseWidget


@utils.traceback_on_all
class BlocksCollapseHandler(Handler):

    def __init__(self, parent=None, collapsible=True):
        super(BlocksCollapseHandler, self).__init__(parent)

        self._txt_edit = parent
        self._collapsible = collapsible
        self._collapse_widget = None
        self._lines_widget = None

    def install(self):
        """
        Apply line-numbers and blocks-collapse widgets last one only if required).
        """

        collapse_widget = CollapseWidget(self._txt_edit)

        # connect signals
        txt_bar = self._txt_edit.verticalScrollBar()
        txt_bar.valueChanged.connect(collapse_widget.update_from_txt_edit_slider)
        txt_bar.rangeChanged.connect(collapse_widget.update_from_txt_edit_slider)
        txt_bar.sliderMoved.connect(collapse_widget.update_from_txt_edit_slider)

        # set left-margin on parent QTextEdit (where this widget will over-lay).
        self._txt_edit.setViewportMargins(
            max(kk.LEFT_PADDING, self._txt_edit.viewportMargins().left()), 0, 0, 0
        )

        # override parent QTextEdit's paint event so it will update line-numbers
        # widget too
        self._txt_edit.textChanged.connect(collapse_widget.parse_text)
        self._txt_edit.viewport().paintEvent = utils.run_after_event(
            collapse_widget.paintEvent,
            self._txt_edit.viewport().paintEvent
        )

        collapse_widget.show()
