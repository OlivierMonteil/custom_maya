"""
Text blocks expand/collapse system, by adding a CollapseWidget on QTextEdits and
Collapse Buttons in association to class/def declaration blocks.

The custom_script_editor.managers.BlocksCollapseManager class is used to add this
system to the Script Editor tabs QTextEdit instances.

Also, the line-numbers display is re-implemented so it will match blocks collapse.
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.blocks_collapse.widget import CollapseWidget
from custom_maya.features.blocks_collapse.handler import BlocksCollapseHandler
from custom_maya.features.blocks_collapse.manager import BlocksCollapseManager


def get_manager():
    return utils.get_singleton(BlocksCollapseManager)
