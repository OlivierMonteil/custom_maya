try:
    from PySide2 import QtWidgets, QtGui, QtCore
except ImportError:
    from PySide import QtGui, QtCore
    from PySide import QtGui as QtWidgets

from custom_maya import constants as kk
from custom_maya import utils
from custom_maya.utils import Regex
from custom_maya.features.collapser import get_manager as get_collapser


class CollapseWidget(QtWidgets.QWidget):
    """
    Collapse/Expand widget. All the buttons are directly painted on this widget
    and their mouseEvents are also managed there (no actual QPushButton is used).
    """

    blocks_visibility_changed = QtCore.Signal(int, int, bool)
    offsetX = 2

    colors = [
        QtGui.QColor(207, 228, 255, 100),
        QtGui.QColor(89, 166, 61)
    ]

    radius = 10
    left_align = 5

    indent_rgx = Regex('^\s*')

    def __init__(self, txt_edit):
        super(CollapseWidget, self).__init__(txt_edit)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self.txt_edit = txt_edit
        self.buttons = []
        self._collapser = get_collapser()

        # self.resize(self.txt_edit.viewportMargins().left(), kk.INF_HEIGHT)

        self.parse_text()

    def sizeHint(self):
        return QtCore.QSize(self.txt_edit.viewportMargins().left(), kk.INF_HEIGHT)

    def _get_button(self, block):
        """
        Args:
            block (QtGui.QTextBlock)

        Returns:
            (CollapseButton or None)

        Get the existing CollapseButton that is associated to the <block>.
        """

        for button in self.buttons or ():
            if button.block == block:
                return button

    def _remove_button(self, button):
        self.buttons.remove(button)
        button.setParent(None)
        button.deleteLater()

    def parse_text(self):
        """
        """

        block = self.txt_edit.document().begin()
        # these two lists will be used to know which buttons are to be removed
        kept_buttons = []
        old_buttons = list(self.buttons)

        while block.isValid():
            text = block.text()

            # block matches a pattern
            if any(text.strip().startswith(x) for x in kk.COLLAPSIBLE_PATTERNS):
                found = self._get_button(block)
                if found:
                    kept_buttons.append(found)
                    found.update_data()
                    block = block.next()
                    continue

                self._add_button(block)

            block = block.next()

        # remove all unused buttons
        for button in reversed(old_buttons) or ():
            if button not in kept_buttons:
                self._remove_button(button)
        # remove all unused buttons
        for button in reversed(self.buttons) or ():
            if button.get_start() >= button.get_end():
                self._remove_button(button)

        self.buttons = sorted(self.buttons, key=lambda b: -1*b.block.blockNumber())

    def _add_button(self, block):
        """
        Args:
            block (QtGui.QTextBlock)

        Create new CollapseButton for <block>. Set its collapsed state from the
        next block's visibility state.
        """

        collapsed = True if not block.next().isVisible() else False

        button_data = ButtonData(block, self, collapsed)
        button_data.state_changed.connect(self.collapse_expand_blocks)

        self.buttons.append(button_data)

        return button_data

    def paintEvent(self, event):
        """ Qt re-implementation, paint all the fake buttons. """

        painter = QtGui.QPainter(self)
        painter.setBrush(QtCore.Qt.transparent)

        for data in self.buttons or ():
            if not data.block.isVisible():    # in case of nested hidden "buttons"
                data.bounding_rect = QtCore.QRect()
                continue

            start_rect = utils.get_block_bounding_rect(self.txt_edit, data.block)
            end_block = data.end_block
            while end_block != data.block:
                if end_block.isVisible():
                    break
                end_block = end_block.previous()

            end_rect = utils.get_block_bounding_rect(self.txt_edit, end_block)

            # don't draw the buttons if the block's height is smaller thant the
            # button's (on zoom out)
            if CollapseWidget.radius >= start_rect.height():
                data.bounding_rect = QtCore.QRect()
                continue

            if data.is_collapsed():
                end_rect = start_rect

            top = start_rect.top()
            bottom = end_rect.top() + end_rect.height()

            top += (start_rect.height() -self.radius)//2

            rect = QtCore.QRect(
                    self.left_align,
                    top ,
                    self.radius,
                    self.radius
                )
            data.bounding_rect = rect

            # paint the button if visible
            if top <= event.rect().bottom() and bottom >= event.rect().top():
                painter.setPen(self.colors[data.collapsed])
                # store the button's bouding rectangle for mousePressEvent detection
                painter.drawRect(rect)

                painter.drawLine(
                    self.left_align +3,
                    top +self.radius//2,
                    self.left_align +self.radius -3,
                    top +self.radius//2
                )

                if data.collapsed:
                    painter.drawLine(
                        self.left_align +self.radius//2,
                        top +3,
                        self.left_align +self.radius//2,
                        top +self.radius -3
                    )

    def update_from_txt_edit_slider(self, *args):
        self.update()

    def custom_resize_event(self, event):
        """
        Must be called on self.txt_edit.resizeEvent() (see handler.py).
        """

        rect = self.txt_edit.contentsRect()
        self.setGeometry(
            QtCore.QRect(
                rect.left(), rect.top(), self.txt_edit.viewportMargins().left(), rect.height()
            )
        )

    def mousePressEvent(self, event):
        collapse_all = None
        hit_button = None

        for data in self.buttons or ():
            if data.bounding_rect.contains(event.pos()):
                hit_button = data
                break

        if not hit_button:
            return

        if event.modifiers() == QtCore.Qt.ControlModifier:
            collapse_all = True
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            collapse_all = False

        if collapse_all is None:
            hit_button.toggle()
        else:
            for data in self.buttons or ():
                data.set_collapsed(collapse_all)

        event.accept()
        self.update()

    def collapse_expand_blocks(self):
        data = self.sender()
        self._collapser.set_blocks_visibility(
            self.txt_edit,
            data.get_start(),
            data.get_end(),
            not data.collapsed
        )
        self.update()


class ButtonData(QtCore.QObject):
    """
    This class but is used to store the button's state, start block, etc.
    No actual button will be used, but fake ones will be painted into the
    CollapseWidget's painteEvent method.
    """

    state_changed = QtCore.Signal()
    empty_line_rgx = Regex('^\s*$')

    def __init__(self, start_block, parent=None, collapsed=False):
        """
        Args:
            start_block (QtWidgets.QTextBlock)
            parent (QtWidgets.QWidget, optional)
            collapsed (bool, optional)
        """

        super(ButtonData, self).__init__(parent)

        self.block = start_block
        self.end_block = None
        self.collapsed = collapsed
        self.bounding_rect = QtCore.QRect()     # updated in CollapseWidget.paintEvent

        self.update_data()

    def toggle(self):
        self.collapsed = not self.collapsed
        self.state_changed.emit()

    def get_start(self):
        return self.block.blockNumber() +1

    def get_end(self):
        return self.end_block.blockNumber() +1

    def set_collapsed(self, state):
        if self.collapsed == state:
            return

        self.collapsed = state
        self.state_changed.emit()

    def update_data(self):

        block = self.block.next()
        start_indent_level = utils.get_indent_level(self.block, ignore_white_lines=True)

        while block.isValid():
            indent_level = utils.get_indent_level(block, ignore_white_lines=True)

            if indent_level is None or indent_level > start_indent_level:
                block = block.next()
                continue
            break

        # MEL proc closure
        # if block.text().startswith('}'):
        #     block = block.previous()
        # do not collapse ending line breaks
        while self.empty_line_rgx.match(block.previous().text()):
            block = block.previous()

        self.end_block = block.previous()

    def is_collapsed(self):
        """ Returns: (bool) """
        return self.collapsed
