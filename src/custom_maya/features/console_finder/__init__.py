"""
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.console_finder.handler import ConsoleFinderHandler
from custom_maya.features.console_finder.manager import ConsoleFinderManager


def get_manager():
    return utils.get_singleton(ConsoleFinderManager)
