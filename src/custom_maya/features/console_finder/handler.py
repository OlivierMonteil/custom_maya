import os
import sys

from PySide2 import QtCore, QtWidgets, QtGui

from maya import mel

from custom_maya import utils
from custom_maya.core import Handler

from custom_maya.features.console_finder.widget import SearchBar


@utils.traceback_on_all
class ConsoleFinderHandler(Handler):

    _padding = 5

    def __init__(self, parent=None):
        super(ConsoleFinderHandler, self).__init__(parent)

        self._txt_edit = parent
        self._splitter = parent.parent()
        
        self.search_bar = None

        self.current_search_str = None
        self.current_selection = None
        self.extra_selections = []

        self.timer = QtCore.QTimer(interval=500)
        self.timer.timeout.connect(self.find_text)
        self.idle = False

    def install(self):
        self.search_bar = SearchBar(self._txt_edit)
        self.stay_in_right_corner()
        self.search_bar.show()
        self.search_bar.triggered.connect(self.timer.start)
        self.search_bar.close_bar()

        self._txt_edit.installEventFilter(self)
        self._txt_edit.verticalScrollBar().installEventFilter(self)        
    
    def eventFilter(self, obj, event):
        """
        Args:
            obj (QTextEdit)

        Filter and handle key events (only).
        """

        if isinstance(obj, QtWidgets.QScrollBar):
            if event.type() in (event.Hide, event.Show):
                self.stay_in_right_corner()

        elif event.type() == event.Resize:
            self.stay_in_right_corner()

        if event.type() != event.KeyPress:
            return False

        if event.modifiers() == QtCore.Qt.ControlModifier:
            if event.key() == QtCore.Qt.Key_F:
                self.show_search_bar()
                selected_txt = self._txt_edit.textCursor().selectedText()
                if selected_txt:
                    self.search_bar.set_text(selected_txt)
                return True
            if event.key() in (QtCore.Qt.Key_Enter, QtCore.Qt.Key_Return):
                selected_txt = self._txt_edit.textCursor().selectedText()
                try:
                    mel.eval(selected_txt)
                except Exception as e:
                    print(e)
                    pass


        return False
    
    def show_search_bar(self):
        self.search_bar.setHidden(False)
        self.search_bar.field.setFocus()

    def stay_in_right_corner(self):
        """
        Resize and move search bar depending on console layout.
        """
        vbar = self._txt_edit.verticalScrollBar()
        hbar = self._txt_edit.horizontalScrollBar()
        vbar_offset = vbar.width() if vbar.isVisible() else 0
        hbar_offset = hbar.height() if hbar.isVisible() else 0

        self.search_bar.move(self._padding, self._splitter.sizes()[0] -self._padding -self.search_bar.height() -hbar_offset)
        self.search_bar.resize(self._splitter.width() -vbar_offset -self._padding*2, self.search_bar.height())

    def get_selected(self):
        return self._txt_edit.textCursor().selectedText()

    def get_selection_cursor(self, i):
        if not 0 <= i < len(self.extra_selections):
            return
        return self.extra_selections[i].cursor
    
    def select_next_match(self, forward):
        cursor = None
        sel_num = len(self.extra_selections)
        pos = self._txt_edit.textCursor().selectionStart()

        if not self.extra_selections:
            self.clear_selection()
            return

        if self.current_selection is None:
            for i in range(sel_num):
                cursor = self.get_selection_cursor(i)
                if cursor.block().isVisible() and cursor.selectionStart() >= pos:
                    self._txt_edit.setTextCursor(cursor)
                    self.current_selection = i
                    break

            if self.current_selection is None:
                for i in range(sel_num):
                    cursor = self.get_selection_cursor(i)
                    if cursor.block().isVisible():
                        self._txt_edit.setTextCursor(cursor)
                        self.current_selection = i
            return

        if len(self.extra_selections) == 1:
            cursor = self.get_selection_cursor(0)
            if not cursor.block().isVisible():
                self.current_selection = 0

        else:
            loop_indices = [0, sel_num-1]
            direction = 1 if forward else -1
            rge_start = self.current_selection +direction
            rge_end = sel_num if forward else -1

            if self.current_selection == loop_indices[forward]:
                self.current_selection = loop_indices[not forward]

            else:
                for i in range(rge_start, rge_end, direction):
                    cursor = self.get_selection_cursor(i)
                    if cursor.block().isVisible():
                        self.current_selection = i
                        break

        cursor = self.get_selection_cursor(self.current_selection)

        if cursor:
            self._txt_edit.setTextCursor(cursor)
        else:
            self.clear_selection()

    def clear_selection(self):
        cursor = self._txt_edit.textCursor()
        cursor.clearSelection()
        self._txt_edit.setTextCursor(cursor)
        self.current_selection = None
    
    def clear_selection(self):
        cursor = self._txt_edit.textCursor()
        cursor.clearSelection()
        self._txt_edit.setTextCursor(cursor)
        self.current_selection = None
    
    def find_text(self):
        self.timer.stop()
        
        args_dict = self.search_bar.get_params()
        if not args_dict:
            self.clear_highlights()
            return 

        search_str = args_dict.get('search_str', None)
        forward = args_dict.get('forward', True)

        if not search_str:
            self.clear_highlights()
            return

        if self.current_search_str == search_str and self.extra_selections:
            self.select_next_match(forward)
            return

        found = False
        doc = self._txt_edit.document()

        highlight_cursor = QtGui.QTextCursor(doc)
        cursor = QtGui.QTextCursor(doc)

        position = self._txt_edit.textCursor().position()

        highlight_color = self._txt_edit.palette().highlight()
        highlight_txt_color = self._txt_edit.palette().highlightedText()

        self.extra_selections = []
        self.current_selection = None

        while not highlight_cursor.isNull() and not highlight_cursor.atEnd():
            highlight_cursor = doc.find(search_str, highlight_cursor)

            if not highlight_cursor.isNull():
                               
                extra_sel = QtWidgets.QTextEdit.ExtraSelection()
                extra_sel.cursor = highlight_cursor
                extra_sel.format.setBackground(highlight_color)
                extra_sel.format.setForeground(highlight_txt_color)

                self.extra_selections.append(extra_sel)

        self._txt_edit.setExtraSelections(self.extra_selections)
        self.select_next_match(forward)
        self.current_search_str = search_str

    def clear_highlights(self):
        self._txt_edit.setExtraSelections([])
        self.current_selection = None
        self.extra_selections = []