from custom_maya import utils
from custom_maya.core import Manager
from custom_maya.features.console_finder import ConsoleFinderHandler


@utils.traceback_on_all
class ConsoleFinderManager(Manager):
    _handler_class = ConsoleFinderHandler
    _instance = None
    _name = None
    _window_titles = ['Script Editor']

    def apply(self):
        return super(ConsoleFinderManager, self).apply()

    def target_widget(self):
        return utils.get_script_editor().console_txt_edit

    def on_prefs_changed(self):
        pass

    def is_enabled(self, *args):
        return True
