import os

from PySide2 import QtWidgets, QtCore, QtGui

from custom_maya import constants as kk
from custom_maya import utils


class SearchBar(QtWidgets.QWidget):

    triggered = QtCore.Signal()

    def __init__(self, parent=None):
        super(SearchBar, self).__init__(parent)

        self.forward = True

        layout = QtWidgets.QHBoxLayout(self)

        self.field = QtWidgets.QLineEdit(self)
        self.previous_button = QtWidgets.QPushButton('Previous', self)
        self.next_button = QtWidgets.QPushButton('Next', self)
        self.close_button = QtWidgets.QPushButton('x', self)

        # self.close_button.setIcon(QtGui.QIcon(os.path.join(kk.ICONS_ROOT, 'close.png')))
        self.close_button.setFixedSize(16, 16)

        layout.addWidget(self.field)
        layout.addWidget(self.previous_button)
        layout.addWidget(self.next_button)
        layout.addWidget(self.close_button)

        for widget in (self, layout):
            widget.setContentsMargins(0, 0, 0, 0)

        for button in (self.previous_button, self.next_button):
            button.setFixedWidth(70)

        layout.setStretch(0, 1)

        self.previous_button.clicked.connect(self.get_back)
        self.next_button.clicked.connect(self.get_forward)
        self.close_button.clicked.connect(self.close_bar)
        self.field.returnPressed.connect(self.emit_signal)
        self.field.textChanged.connect(self.emit_signal)

    def get_params(self):
        if not self.isVisible():
            return None
        else:
            return {'search_str': self.text(), 'forward': self.forward}
        
    def get_back(self, *args):
        self.fwd_state = False
        self.emit_signal()
    
    def get_forward(self, *args):
        self.fwd_state = True
        self.emit_signal()
        
    def text(self):
        return self.field.text()

    def set_text(self, txt):
        return self.field.setText(txt)

    def close_bar(self):
        self.setHidden(True)
        self.emit_signal()

    def emit_signal(self, *args):
        self.triggered.emit()


class CloseButton(QtWidgets.QPushButton):
    def __init__(self, parent=None):
        super(CloseButton, self).__init__('', parent)

        path = os.path.join(kk.ICONS_ROOT, 'close.png')
        self.setIcon(QtGui.QIcon(path))
        self.setFixedSize(16, 16)