"""
Maya's Script Editor's tabs multi-cursor-editing management. As multi-cursors
are not featured into the Qt API, we will simply paint fake cursors and handle
the multi-editing into the custom_script_editor.keys.KeysHandler instance.

The MultiCursorHandler class is the one that is used to keep track of the fake
cursors and paint them on a QWidget overlay (on which are also painter the
max-line-width and the current line highlight).

The custom_script_editor.managers.MultiCursorManager class is used to add this
system to the Script Editor tabs QTextEdit instances.
"""

# shorten absolute imports
from custom_maya import utils
from custom_maya.features.multi_cursor.txt_cursor import MultiCursor
from custom_maya.features.multi_cursor.handler import MultiCursorHandler
from custom_maya.features.multi_cursor.manager import MultiCursorManager


def get_manager():
    return utils.get_singleton(MultiCursorManager)
