"""
MultiCursorHandler : events filter that handles multi cursors in QTextEdit.
"""

from PySide2 import QtCore, QtGui, QtWidgets

from custom_maya import utils
from custom_maya.core import Handler


TRIGGERING_EVENTS = [
    QtCore.QEvent.MouseButtonPress,
    QtCore.QEvent.MouseButtonRelease
]

CORRECTION_DELAY = 100      # msec, delay before last cursor correction, see
                            # MultiCursorHandler._update_last_cursor


@utils.traceback_on_all
class MultiCursorHandler(Handler):
    """
    Custom eventFilter installed on Maya Script Editor cmdScrollFieldExecuters.
    Allows multi-cursor editing with fake cursors display and keyEvents filtering.
    """

    needs_update = QtCore.Signal(object)

    def __init__(self, parent=None):
        super(MultiCursorHandler, self).__init__(parent)

        self._txt_edit = parent
        self.cursors = []

        overlay = utils.get_overlay(self._txt_edit, False)
        overlay.set_multi_cursor_widget(self)

    def install(self):
        """
        Args:
            txt_edit (QtWidgets.QTextEdit)

        Install self on <txt_edit>'s viewport and create an overlay widget on
        which will be painted the fake multi-cursors.
        """

        viewport = self._txt_edit.viewport()
        cursor = self._txt_edit.textCursor()
        self._txt_edit.setTextCursor(cursor)

        # install the event
        viewport.installEventFilter(self)

        # store txt_edit's QTextCursor and a copy of it that will be used to
        # actually perform the text edits
        self.cursors = [cursor]
        self.multi_cursor = QtGui.QTextCursor(cursor)   # creates a copy

    def eventFilter(self, obj, event):
        """
        Handle multi cursors on LMB events on QTextEdit.
        """

        if not event.type() in TRIGGERING_EVENTS:
            return False

        self._txt_edit = obj.parent()

        if event.type() == QtCore.QEvent.MouseButtonPress:
            if event.button() == QtCore.Qt.LeftButton:
                # add last cursor to extra-selections so its selection will not
                # visually disappear between mouse press and release
                self.update_extra_selections(include_last=True)

        elif event.type() == QtCore.QEvent.MouseButtonRelease:
            if event.button() == QtCore.Qt.LeftButton:
                # get new QTextCursor at mouse position
                new_cursor = self._txt_edit.textCursor()

                if event.modifiers() == QtCore.Qt.ControlModifier:
                    # check if new_cursor is already in self.cursors
                    already_in = self._cursor_exists(new_cursor)

                    if not already_in:
                        return self._add_cursor(new_cursor)

                    # remove from self.cursors if more than one
                    if len(self.cursors) > 1:
                        return self.remove_cursor(already_in)

                    # no need to go any further
                    return True

                self._add_cursor(new_cursor)
                self.clear_cursors()

                # fix last cursor after small delay
                QtCore.QTimer.singleShot(CORRECTION_DELAY, self._update_last_cursor)

        # pass the event through
        return False

    def _update_last_cursor(self):
        """
        FIXUP workaround : seems that `self._txt_edit.textCursor()` may need a
        small delay to get the correct values (pretty visible after deselecting
        some text and typing, which in some cases was overwriting the previous
        selection). That's why this call has been added to update properly the
        last cursor added into the list.
        """

        self.cursors[-1] = self._txt_edit.textCursor()

    def clear_cursors(self):
        """
        Clear all multi-cursor.
        """

        # remove all multi-cursors on simple LMB click
        old_cursors = self.cursors
        self.cursors = [self.cursors[-1]]
        # repaint all removed cursors area
        self.update_extra_selections()

        self.needs_update.emit(old_cursors)
        self._txt_edit.setTextCursor(self.cursors[0])

    def remove_cursor(self, cursor):
        """
        Args:
            cursor (QtGui.QTextCursor)

        Remove <cursor> to the current ones.
        """

        self.cursors.remove(cursor)

        # repaint the removed cursor's area
        self.needs_update.emit([cursor])
        # set QTextEdit cursor on the last one
        self._txt_edit.setTextCursor(self.cursors[-1])

        self.update_extra_selections()

        return True

    def update_extra_selections(self, include_last=False):
        """
        Display multi-selections into QTextEdit.
        """

        # get highlight colors
        highlight_color = self._txt_edit.palette().highlight()
        highlight_txt_color = self._txt_edit.palette().highlightedText()

        extra_selections = []

        # we don't want to make extra selection on the last cursor on mouse
        # release because it would be made in addition to the QTextEdit selection
        # highlight, however on mouse press events, we will add it, because
        # the selection highlight will be removed
        cursors = self.cursors if include_last else self.cursors[:-1]

        for cursor in cursors:
            if not cursor.hasSelection():
                continue

            extra_sel = QtWidgets.QTextEdit.ExtraSelection()
            extra_sel.cursor = cursor
            extra_sel.format.setBackground(highlight_color)
            extra_sel.format.setForeground(highlight_txt_color)

            extra_selections.append(extra_sel)

        self._txt_edit.setExtraSelections(extra_selections)

    def add_cursor_from_key(self, direction):
        """
        Args:
            direction (str) : 'up' or 'down'
        """

        if not direction in ('up', 'down'):
            return True

        new_cursor = self._txt_edit.textCursor()
        if direction == 'up':
            new_cursor.movePosition(new_cursor.Up, new_cursor.MoveAnchor)
        elif direction == 'down':
            new_cursor.movePosition(new_cursor.Down, new_cursor.MoveAnchor)

        already_in = self._cursor_exists(new_cursor)

        if not already_in:
            self._add_cursor(new_cursor)

        return True

    def _add_cursor(self, cursor):
        """
        Args:
            cursor (QtGui.QTextCursor)

        Add <cursor> to the current ones.
        """

        self.cursors.append(cursor)
        self._txt_edit.setTextCursor(self.cursors[-1])
        # ask for overlay update
        self.needs_update.emit(None)

        self.update_extra_selections()

        return True

    def _cursor_exists(self, cursor):
        """
        Args:
            cursor (QtGui.QTextCursor)

        Returns:
            (QtGui.QTextCursor or None)

        Check in self.cursors if any cursor already exists at the same position.
        """

        for c in self.cursors or ():
            if cursor == c:
                return c

        return None
