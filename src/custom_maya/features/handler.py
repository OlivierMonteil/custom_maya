import os

from PySide2 import QtCore

from custom_maya import utils
from custom_maya import controls
from custom_maya import constants as kk
from custom_maya import logger


RUNNING_ORDER = {
    'collapser': -1,
    'blocks_collapse': 1,
    'prefs_editor': 10
}

log = logger.get()


@utils.traceback_on_all
class FeaturesManager(QtCore.QObject):

    _instance = None

    def __init__(self, parent=None):
        super(FeaturesManager, self).__init__(parent=parent)

        self.setParent(parent)

        self._modules = get_feature_modules(kk.FEATURES_ROOT)

        log.verbose(
            'FeaturesManager initialised ({} features found).'.format(len(self._modules))
        )

    def apply(self, obj):
        for x in self._modules:
            mod = import_module(x)
            manager = mod.get_manager()

            if not manager.interested_by(obj):
                continue

            local_prefs = manager.get_local_prefs()
            control_icons = local_prefs.control_icons()
            enabled = local_prefs.enabled()

            if enabled or control_icons:
                new_handler = manager.apply()

                if new_handler:
                    if not isinstance(new_handler, (list, tuple)):
                        new_handler = [new_handler]

                    for h in new_handler:
                        h.set_enabled(enabled)

                    log.verbose('  -> {} "{}" applied.'.format(len(new_handler), x))

                    if control_icons:
                        ctrl_manager = controls.get_manager()
                        ctrl_manager.apply()

                        for h in new_handler:
                            ctrl_manager.add_control(control_icons, h, enabled)


class TreeNode(object):
    def __init__(self, path, parent=None):
        super(TreeNode, self).__init__()

        self.path = path.replace('\\', '/')
        self.parent = parent
        self.found = False

        name = self.path.split('/')[-1]
        self.name = parent.name +'.' +name if parent is not None else name

        self.children = self.get_children()

    def get_children(self):
        children = []

        for x in os.listdir(self.path):
            if 'template' in x.lower() or any(x.startswith(char) for char in ('.', '_')):
                continue
            if x == 'manager.py':
                self.found = True
                continue

            child_path = os.path.join(self.path, x)
            if os.path.isfile(child_path):
                continue

            children.append(TreeNode(child_path, self))

        return children

    def get_found_leaves(self):
        leaves = []
        if self.found:
            leaves.append(self.name)
        else:
            for child in self.children:
                leaves.extend(child.get_found_leaves())
        return leaves


def get_feature_modules(root):
    tree = TreeNode(root)
    modules = ['custom_maya.' +x for x in tree.get_found_leaves()]
    return sorted(modules, key=modules_order)

def modules_order(mod):
    name = mod.split('.')[-1]
    if name in RUNNING_ORDER:
        return RUNNING_ORDER[name]
    return 0

def import_module(name):
    """
    Args:
        name (str)

    Returns:
        (object or None)

    Import object from <obj_str> et return it. If object could not be imported,
    returns None.
    """

    from_list = []
    if '.' in name:
        from_list = ['.'.join(name.split('.')[:-1])]

    if utils.is_python_3():
        import importlib
        return importlib.import_module(name)
    else:
        return __import__(name, locals(), globals(), from_list)
