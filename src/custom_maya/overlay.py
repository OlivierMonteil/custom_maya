"""
Overlay widget for additional paintEvent() on QTExtEdits:

- current-line highligth
- max-length vertical line
- multi-cursors
"""


from PySide2 import QtWidgets, QtGui, QtCore

from custom_maya import constants as kk


CURSOR_COLORS = [
    (94, 132, 255),
    (117, 229, 92)
]
LINE_MAX_CHARS = 80


class OverlayWidget(QtWidgets.QWidget):
    """
    Overlay widget parented to ScriptEditor tab's QTextEdit.
    """

    def __init__(self, txt_edit, apply_padding=True):
        """
        Args:
            txt_edit (QtWidgets.QTextEdit)
            apply_padding (bool, optional)

        Initiates self.
        """

        super(OverlayWidget, self).__init__(txt_edit)

        self.already_painting = False

        self.setAttribute(QtCore.Qt.WA_TransparentForMouseEvents, True)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self._padding = kk.LEFT_PADDING if apply_padding else 0

        self._txt_edit = txt_edit
        self._multi_cursor_handler = None
        self._old_cursor = self._txt_edit.textCursor()

        self._max_line_pos_x = self._get_max_line_length()

        # cursor blinking timer
        self._timer = QtCore.QTimer(interval = 500)
        self._timer.timeout.connect(self._make_cursors_blink)

        self._cursor_state = True    # used to switch cursor's colors

        text_layout = self._txt_edit.findChild(QtGui.QAbstractTextDocumentLayout)
        text_layout.documentSizeChanged.connect(self._update_max_line_length)

        # set overlay's size and position to cover entirely the QTextEdit's viewport
        self.resize(kk.INF_WIDTH, kk.INF_HEIGHT)
        self.move(txt_edit.viewport().pos())

        # connect cursor changes to current-line highlight's update
        self._txt_edit.cursorPositionChanged.connect(self._on_cursor_change)

    def _on_blocks_visibility_changed(self, block):
        pass

    def _on_cursor_change(self):
        """
        (called on QTextEdit.cursorPositionChanged)
        Repaint both old and new cursor's line for current-line highlight's update.
        """


        current_cursor = self._txt_edit.textCursor()

        # do not repaint if the line remains the same
        if current_cursor.block().blockNumber() == self._old_cursor.block().blockNumber():
            return

        # set small offset on repaint() QRect to make sure everything is properly
        # updated
        offset = 2

        for cursor in (current_cursor, self._old_cursor):
            rect = self._txt_edit.cursorRect(cursor)

            rect = QtCore.QRect(
                0,
                rect.y() -offset,
                kk.INF_WIDTH,                       # repaint the whole line
                rect.height() +offset*2
            )

            self.repaint(rect)

        self._old_cursor = current_cursor

    def set_multi_cursor_widget(self, multi_cursor_handler):
        """
        Args:
            multi_cursor_widget (MultiCursorHandler)

        Connect <multi_cursor_handler> "needs_update" QtCore.Signal() to cursors
        repaint.
        """

        self._multi_cursor_handler = multi_cursor_handler
        multi_cursor_handler.needs_update.connect(self._repaint_cursors)

    def _update_max_line_length(self, *args):
        """
        Update max-length line position on QTextEdit zoom in/out..
        """

        old_rect = self._get_max_line_rect()
        self._max_line_pos_x = self._get_max_line_length()
        new_rect = self._get_max_line_rect()

        
        #self.repaint(old_rect)
        #self.update(new_rect)

    def _get_max_line_rect(self, width=2):
        """ Returns: (QtCore.QRect) """

        return QtCore.QRect(
            self._max_line_pos_x -width,
            0,
            width*3,
            kk.INF_HEIGHT
        )

    def _get_max_line_length(self):
        """
        Returns:
            (int)

        Get lines "max-length" width value from font metrics.
        """

        test_string = '_'*LINE_MAX_CHARS
        font = self._txt_edit.font()
        metrics = QtGui.QFontMetrics(font)
        width = metrics.boundingRect(test_string).width()

        return width +self._padding

    def _get_blink_color(self):
        """ Returns: (QtGui.QColor) """

        return QtGui.QColor(*CURSOR_COLORS[self._cursor_state])

    def _make_cursors_blink(self):
        """
        Toggle self.cursor_state and trigger repaint() on cursors (cursors color
        will be toggled as well).
        """

        self._cursor_state = not self._cursor_state
        self._repaint_cursors()

    def _repaint_cursors(self, cursors=None):
        """
        Args:
            cursors (list[QtGui.QTextCursor], optional)

        Repaint all cursors regions (with offset).
        """

        if len(self.get_multi_cursors()) > 1:
            # start blinking timer if not already active
            if not self._timer.isActive():
                self._timer.start()
        else:
            self._timer.stop()

        # set small offset on repaint() QRect to make sure everything is properly
        # updated
        offset = 2

        cursors = cursors if cursors else self.get_multi_cursors()

        for cursor in cursors or ():
            rect = self._txt_edit.cursorRect(cursor)

            rect = QtCore.QRect(
                rect.x() -offset +self._padding,
                rect.y() -offset,
                rect.width() +offset*2,
                rect.height() +offset*2
            )

            self.repaint(rect)

    def repaint(self, *args):
        if self.already_painting:
            return

        super(OverlayWidget, self).repaint(*args)

    def get_multi_cursors(self):
        """
        Returns:
            (list[QtGui.QTextCursor]) : returns empty list if not MultiCursorHandler
                                        is connected
        """

        if self._multi_cursor_handler is None:
            return []
        return self._multi_cursor_handler.cursors

    def paintEvent(self, event):
        """
        Qt re-implementation, paint all overlay items.
        """
        self.already_painting = True

        painter = QtGui.QPainter(self)
        painter.save()

        # paint "max-length" vertical bar
        painter.setPen(QtGui.QColor(207, 228, 255, 20))
        painter.drawLine(
            self._max_line_pos_x,
            0,
            self._max_line_pos_x,
            kk.INF_HEIGHT
        )

        # remove current pen (cursor paints only use background color)
        painter.setPen(QtCore.Qt.NoPen)

        # paint fake cursors
        multi_cursors = self.get_multi_cursors()

        if len(multi_cursors) > 1:
            painter.setBrush(self._get_blink_color())

            for cursor in multi_cursors:
                try:
                    rect = self._txt_edit.cursorRect(cursor)
                    # self.padding must be False on Maya Script Editor but may
                    # have to be applied if used on another QTextEdit
                    painter.drawRect(
                        rect.x() +self._padding,
                        rect.y(),
                        rect.width(),
                        rect.height()
                    )

                except:
                    pass

        # paint current line highlight
        rect = self._txt_edit.cursorRect(self._txt_edit.textCursor())
        painter.setBrush(QtGui.QColor(207, 228, 255, 10))

        painter.drawRect(
            self._padding,
            rect.y(),
            kk.INF_WIDTH,
            rect.height()
        )

        painter.restore()

        self.already_painting = False
