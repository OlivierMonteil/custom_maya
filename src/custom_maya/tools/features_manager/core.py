import os
import shutil
import sys

ROOTS = [
    r'C:\Users\Olivier\Documents\maya\scripts\custom_maya\src',
    r'C:\Program Files\Autodesk\Maya2017\Python\Lib\site-packages'
]
[sys.path.append(rt) for rt in ROOTS if not rt in sys.path]

from custom_maya import constants as kk

TEMPLATE_ROOT = os.path.join(kk.FEATURES_ROOT, '.template')


def to_snake_case(name):
    return name.lower().replace('-', '_')

def generate_feature(name):
    name = to_snake_case(name)
    target_root = os.path.join(kk.FEATURES_ROOT, name)
    # copy .template folder with new name
    shutil.copytree(TEMPLATE_ROOT, target_root)
    # search and replace names
    edit_new_features_files(name, target_root)

def edit_new_features_files(name, root):
    class_prefix = ''.join([x.capitalize() for x in name.split('_')])

    for x in os.listdir(root):
        path = os.path.join(root, x)

        if os.path.isdir(path):
            edit_new_features_files(name, path)
        else:
            new_lines = []
            with open(path, 'r') as opened_file:
                for line in opened_file:
                    line = line.replace('Template', class_prefix)
                    line = line.replace('.template', '.' +name)
                    new_lines.append(line)

            # re-write file
            with open(path, 'w') as opened_file:
                opened_file.write(''.join(new_lines))


if __name__ == '__main__':
    generate_feature('prefs_editor')
