"""
Simple tool for navigating through modules using dir() method.
"""

import os
import sys
import re
import traceback
import subprocess

from PySide2 import QtWidgets, QtCore, QtGui

from maya import cmds
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin
from maya.OpenMayaUI import MQtUtil

from custom_maya import utils
from custom_maya import constants as kk
from custom_maya.stylesheet import apply_style


BG_RGB = {
    'success': (89, 166, 61),
    'error' : (217, 82, 82),
    'warning': (204, 191, 128),
    'info': (61, 61, 61)
}

TEXT_RGB = {
    'success': (0, 0, 0),
    'error' : (0, 0, 0),
    'warning': (0, 0, 0),
    'info': (175, 175, 175)
}

STYLE_PATTERN = """
QTextEdit#%(object_name)s {
    color: rgb%(color)s;
    background : rgb%(background)s;
    %(paddingLine)s
}
"""

WINDOW_NAME = 'PythonDirNavigator'
WINDOW_TITLE = 'Python dir() Navigator'

UI_SCRIPT = """
from maya import cmds
from custom_maya.tools.dir_navigator import main

try:
    cmds.evalDeferred(main.run, lp=True)
except:
    pass
"""


class DirNavigator(MayaQWidgetDockableMixin, QtWidgets.QMainWindow):
    """
    Main dir() navigation window.
    """

    update_needed = QtCore.Signal()
    showFileAsked = QtCore.Signal()
    context_menu_asked = QtCore.Signal(str, QtCore.QPoint)

    def __init__(self, parent=None):
        super(DirNavigator, self).__init__(parent)

        self.setObjectName(WINDOW_NAME)
        self.setWindowTitle(WINDOW_TITLE)
        self.setWindowFlags(QtCore.Qt.Tool)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        apply_style(self)

        # set main widget and layout
        central_widget = QtWidgets.QWidget(self)
        lay = QtWidgets.QGridLayout(central_widget)
        self.setCentralWidget(central_widget)

        # create widgets
        self.objects_field = QtWidgets.QLineEdit(
            self,
            placeholderText='Enter object name...',
            toolTip='(imports will be done if necessary)'
        )

        self.back_button = IconButton('back', self, size=22, enabled=False)

        self.keywords_field = QtWidgets.QLineEdit(
            self,
            placeholderText='Enter keyword...',
            toolTip='Seperate keywords with coma. No case match.'
        )

        result_label = QtWidgets.QLabel('Results :', self)

        self.result_table = ResultTable(self)

        # populate layout
        lay.addWidget(self.objects_field, 0, 0)
        lay.addWidget(self.back_button, 0, 1)
        lay.addWidget(self.keywords_field, 1, 0, 1, 2)
        lay.addWidget(result_label, 2, 0, 1, 2)
        lay.addWidget(self.result_table, 3, 0, 1, 2)

        # add status bar
        self.message_bar = StatusBar(parent=self)
        self.setStatusBar(self.message_bar)

        # set few size rules
        for obj in [central_widget, self]:
            obj.setContentsMargins(2, 2, 2, 2)
        lay.setContentsMargins(4, 4, 4, 4)
        self.resize(450, 350)

        self.objects_field.returnPressed.connect(self.on_object_entered)
        self.keywords_field.returnPressed.connect(self.on_keyword_entered)
        self.objects_field.textChanged.connect(self.on_objects_changed)
        self.keywords_field.textChanged.connect(self.on_keyword_entered)

        self.result_table.cellDoubleClicked.connect(self.append_to_object)
        self.back_button.clicked.connect(self.go_back)

    def on_objects_changed(self):
        """
        (called on object's field.textChanged signal)
        Update back button's enabled state.
        """
        obj_str = self.objects_field.text()

        if obj_str and '.' in obj_str:
            self.back_button.setEnabled(True)
        else:
            self.back_button.setDisabled(True)

    def on_object_entered(self):
        """
        (called on object's field.returnPressed signal)
        Set focus on keywords field.
        """
        self.keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

    def on_keyword_entered(self, *args):
        """
        (called on keywords field.returnPressed signal)
        Search for matching items in dir(object) and display result into the
        results table.
        """
        obj_str = self.objects_field.text()
        keywords = self.keywords_field.text()
        
        if obj_str:
            self.update_needed.emit()
        else:
            self.error('Must enter valid object.')

    def clear_view(self):
        """ Clear results table. """

        while self.result_table.rowCount():
            self.result_table.removeRow(0)

    def get_args(self):
        return self.objects_field.text(), self.keywords_field.text()
    
    def get_clean_menu(self):
        return CleanMenu(self)
    
    def get_help_window(self):
        return HelpWindow(self)
    
    def populate_table(self, obj_str, found_strings):
        for i, attr in enumerate(found_strings) or ():
            self.result_table.insertRow(i)
            label = Label(attr, '{}.{}'.format(obj_str, attr), self)
            self.result_table.setCellWidget(i, 0, label)

    def append_to_object(self, row, col):
        """
        Args:
            row (int)
            col (int)

        (called on ResultTable.cellDoubleClicked signal).
        Add double-clicked item to objects field, separated with a dot.
        """

        label = self.result_table.cellWidget(row, col).text()
        current_object = self.objects_field.text()

        if not current_object.split('.')[-1] == label:
            # append to objects field
            self.objects_field.setText('{}.{}'.format(current_object, label))
            # clear ResultTable
            self.clear_view()
            # clear keywords field
            self.keywords_field.setText('')
            self.keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

    def go_back(self):
        """
        (called on back button.clicked signal)
        Set object field to current object's parent and set focus on keyword field.
        """

        obj_str = self.objects_field.text()
        new_obj_str = '.'.join(obj_str.split('.')[:-1])

        self.objects_field.setText(new_obj_str)
        self.keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

        self.clear_view()

    def contextMenuEvent(self, event):
        """ Qt re-implementation. """
        # get Label object under mouse
        global_pos = self.mapToGlobal(event.pos())
        pos = self.result_table.mapFromGlobal(global_pos)

        index = self.result_table.indexAt(pos)
        label = self.result_table.cellWidget(index.row(), index.column())
        if not label:
            return

        self.context_menu_asked.emit(label.full_path, global_pos)

    def success(self, message, timeout=5000):
        self.show_message(message, 'success', timeout=timeout)

    def error(self, message, timeout=5000):
        self.show_message(message, 'error', timeout=timeout)

    def warning(self, message, timeout=5000):
        self.show_message(message, 'warning', timeout=timeout)

    def info(self, message, timeout=5000):
        self.show_message(message, 'info', timeout=timeout)

    def show_message(self, message, lvl_type, timeout=5000):
        """
        Args:
            message (str)
            lvl_type (str) : 'info', 'warning', 'success' or 'error'
            timeout (int)

        Show <message> with <lvl_type> format and colors into message bar.
        """

        # format message
        if message:
            message = '{} : {}'.format(
                lvl_type.capitalize(), message
            )

        # display message
        self.message_bar.set_color(lvl_type)
        self.message_bar.showMessage(message, timeout=timeout)

    def show(self, *args, **kwargs):
        """
        Qt re-implementation, show UI with generated uiScript argument
        so it will be recreated on the next Maya session.
        """
        kwargs['dockable'] = True
        kwargs['uiScript'] = UI_SCRIPT
        super(DirNavigator, self).show(*args, **kwargs)

    @classmethod
    def _restoreUI(cls):
        """ Internal method to restore the UI when Maya is opened. """
        # create UI instance
        mayaUI = utils.getMayaWindow()
        instance = cls(mayaUI)

        # add UI to the WorkspaceControl
        workspaceControl = MQtUtil.getCurrentParent()
        mixinPtr = MQtUtil.findControl(instance.objectName())
        MQtUtil.addWidgetToMayaLayout(int(mixinPtr), int(workspaceControl))

class Label(QtWidgets.QLabel):
    """
    Custom QLabel with full path information.
    """

    def __init__(self, label, full_path, parent=None):
        super(Label, self).__init__(label, parent)

        self.full_path = full_path


class ResultTable(QtWidgets.QTableWidget):
    """
    QTableWidget for dir() results display.
    """

    def __init__(self, parent=None):
        super(ResultTable, self).__init__(parent)

        self.navigator = parent

        # set a few flags
        self.setColumnCount(1)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # edit headers
        h_header = self.horizontalHeader()
        h_header.hide()
        h_header.setStretchLastSection(True)
        v_header = self.verticalHeader()
        v_header.hide()
        v_header.setDefaultSectionSize(20)

        self.setVerticalScrollMode(self.ScrollPerItem)


class StatusBar(QtWidgets.QStatusBar):
    """
    QStatuBar for messages display.
    """

    def __init__(self, parent=None):
        super(StatusBar, self).__init__(parent)

        self.set_color('info')

        self.messageChanged.connect(self.on_message_change)

    def set_color(self, lvl_type):
        """
        Args:
            lvl_type (str) : 'info', 'warning', 'error' or 'success'
        Set QStatusBar background and text colors from <lvl_type>.
        """

        bg_rgb = str(BG_RGB[lvl_type])
        text_rgb = str(TEXT_RGB[lvl_type])

        self.setStyleSheet('background-color: rgb{}; color: rgb{};'.format(
                bg_rgb, text_rgb
            )
        )

    def on_message_change(self, message):
        """
        Args:
            message (str)

        Restore "info" color on empty <message> (amongst others, is triggered
        at messages timeout).
        """

        if not message:
            self.set_color('info')


class HelpWindow(QtWidgets.QDialog):
    """
    QDialog for help() display (see `derivating_stdout` context-manager).
    """

    def __init__(self, parent=None):
        super(HelpWindow, self).__init__(parent)

        self.setWindowTitle('Help')

        layout = QtWidgets.QVBoxLayout(self)

        self.txt_edit = QtWidgets.QTextEdit(
            self,
            lineWrapMode=QtWidgets.QTextEdit.NoWrap
        )

        self.txt_edit.setObjectName('PDNhelpText')
        self.highlighter = HelpHighlighter(self.txt_edit)

        layout.addWidget(self.txt_edit)
        self.resize(450, 350)

    def write(self, txt):
        """ sys.stdout re-implementation """
        self.txt_edit.append(txt)

    def writelines(self, lines):
        """ sys.stdout re-implementation """
        self.txt_edit.clear()
        self.txt_edit.append('\n'.join(lines))

    def scroll_to_the_top(self):
        self.txt_edit.verticalScrollBar().setSliderPosition(0)


class HelpHighlighter(QtGui.QSyntaxHighlighter):
    """
    Help result text highlighter.
    """

    def __init__(self, txt_edit):

        self.styles = self.get_styles()
        self.rules = self.get_rules()

        # apply stylesheet
        style_body = STYLE_PATTERN % {
            'object_name': txt_edit.objectName(),
            'color': self.styles['base_text'],
            'background': self.styles['background'],
            'paddingLine': ''
        }

        txt_edit.setStyleSheet(style_body)

        super(HelpHighlighter, self).__init__(txt_edit)

    def highlightBlock(self, line):
        """
        Args:
            line (str)

        Qt re-implementation. Apply syntax highlighting to the given line.
        """

        for regex, nth, txt_format in self.rules:
            for match in regex.finditer(line):
                start = match.start(nth)
                end = match.end(nth)

                self.setFormat(start, end -start, txt_format)

    def get_styles(self):
        """
        Returns:
            (dict) : from type {'name': QtGui.QTextCharFormat}

        Get text formats by name as dict.
        """

        return {
            'grey': char_format((90, 99, 111)),
            'orange': char_format((207, 154, 102)),
            'green': char_format((147, 195, 121)),
            'pink': char_format((200, 116, 220)),
            'yellow': char_format((228, 187, 106)),
            'blue': char_format((92, 166, 237)),
            'red': char_format((224, 109, 116)),
            'background': (29, 34, 46),
            'base_text': (170, 176, 190),
            'cyan': char_format((87, 180, 193))
        }

    def get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])

        Get all highlight rules.
        """

        # operators rules
        rules = [(utils.Regex('%s' % o), 0, self.styles['pink']) for o in kk.OPERATORS]
        # # python keywords rules
        # rules += [(utils.Regex('\\b(%s)\\b' % w), 0, self.styles['pink']) for w in kk.PYTHON_KEYWORDS]
        # intermediate objects rule
        rules += [(utils.Regex('(\.)(\w+)'), 2, self.styles['red'])]
        # functions names
        rules += [(utils.Regex('\\b(\w+)\\b\s*\('), 1, self.styles['blue'])]
        # class names
        rules += [(utils.Regex('(\\bclass\\b\s*)(\\b_*\w+_*\\b)'), 2, self.styles['yellow'])]
        # class args
        rules += [(utils.Regex('\\bclass\\b\s*\\b_*\w+_*\\b\((\w+)\)'), 1, self.styles['yellow'])]
        # missing
        rules += [(utils.Regex('\((\.\.\.)\)'), 1, self.styles['grey'])]
        # attributes
        rules += [(utils.Regex('^\s*\|*\s*(\w+)\s*\='), 1, self.styles['orange'])]
        # double-quoted string, possibly containing escape sequences
        rules += [(utils.Regex(r'"[^"\\]*(\\.[^"\\]*)*"'), 0, self.styles['green'])]
        # single-quoted string, possibly containing escape sequences
        rules += [(utils.Regex(r"'[^'\\]*(\\.[^'\\]*)*'"), 0, self.styles['green'])]

        rules += [
            # start '|' chars
            (utils.Regex('^(\s*\|\s*)'), 1, self.styles['grey']),
            # '| --------' lines
            (utils.Regex('^(\s*\|)\s*(\-+)'), 2, self.styles['grey']),
            # lines that ends with a ':'
            (utils.Regex('^(\s*\|\s*)*(.+\:\s*)$'), 2, self.styles['green']),
            # lines in CAPS (like "FILE")
            (utils.Regex('^([A-Z0-9\s]+)$'), 0, self.styles['orange']),
            # python objects
            (utils.Regex('(\<[\w\.\'\"\s\-]+\>*)(\.\.\.)*'), 1, self.styles['cyan']),
            # paths
            (utils.Regex('\s*([\w\s\-\:]+\\\\)+([\w\-]+\.\w+)\s*'), 0, self.styles['green'])
        ]
        return rules
    

class CleanMenu(QtWidgets.QMenu):
    def __init__(self, *args, **kwargs):
        result = super(CleanMenu, self).__init__(*args, **kwargs)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

    def addAction(self, *args, **kwargs):
        """
        Qt re-implementation. Ensure all added actions are properly
        parented to the menu, so they will be destroyed with it.
        """
        action = super(CleanMenu, self).addAction(*args, **kwargs)
        action.setParent(self)
        return action

    
class IconButton(QtWidgets.QPushButton):
    """
    Simple icon button with normal/hover/pressed/disabled icons set in paintEvent.
    """

    def __init__(self, icon_name, parent=None, size=14, **kwargs):
        super(IconButton, self).__init__(parent, **kwargs)

        self._icon_name = icon_name
        self._size = size
        self.setFixedSize(size, size)

    def paintEvent(self, event):
        """ Qt re-implementation, paints the button itself. """

        # set map
        map_name = '{}.png'.format(self._icon_name)

        if not self.isEnabled():
            map_name = '{}_disabled.png'.format(self._icon_name)
        elif self.isDown():
            map_name = '{}_pressed.png'.format(self._icon_name)
        elif self.underMouse():
            map_name = '{}_hover.png'.format(self._icon_name)

        map_path = os.path.join(kk.ICONS_ROOT, map_name)

        # set anti-aliased painter for self
        painter = QtGui.QPainter(self)
        painter.setRenderHint(painter.SmoothPixmapTransform)
        # paint map
        painter.drawImage(
            QtCore.QRect(0, 0, self._size, self._size),
            QtGui.QImage(map_path)
        )

        painter.end()


def char_format(rgb, style=''):
    """
    Args:
        rgb (tuple(int))
        style (str, optional)

    Returns:
        (QtGui.QTextCharFormat)

    Return a QtGui.QTextCharFormat with the given attributes (color, font
    weigth, etc).
    """

    if isinstance(rgb, (tuple, list)):
        color = QtGui.QColor(*rgb)
    else:
        # handle named colors as "red"
        color = QtGui.QColor()
        color.setNamedColor(rgb)

    ch_format = QtGui.QTextCharFormat()
    ch_format.setForeground(color)

    if 'bold' in style:
        ch_format.setFontWeight(QtGui.QFont.Bold)
    if 'italic' in style:
        ch_format.setFontItalic(True)

    return ch_format


def run():
    """
    Show "Python dir() Navigator" with Maya Window as parent.
    """
    if cmds.workspaceControl(WINDOW_NAME +'WorkspaceControl', q=True, exists=True):
        cmds.deleteUI(WINDOW_NAME +'WorkspaceControl')

    maya_ui_qt = utils.get_maya_window()
    utils.close_existing(maya_ui_qt, WINDOW_NAME)

    new_window = DirNavigator(parent=maya_ui_qt)
    return new_window
