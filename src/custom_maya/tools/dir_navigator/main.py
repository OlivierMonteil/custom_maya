import re
import sys
import traceback
import subprocess

from custom_maya import utils
from custom_maya.tools.dir_navigator import ui

SKIPPED = ['test']


@utils.traceback_on_all
class Manager(object):
    def __init__(self, window):

        self.window = window
        self.window.manager = self

        window.context_menu_asked.connect(self.show_context_menu)
        window.update_needed.connect(self.show_matching_dirs)
    
    def show_context_menu(self, obj_str, pos):
        # get object, name and source file
        filepath = get_file(obj_str, silent=True)

        # create menu
        menu = self.window.get_clean_menu()
        help_action = menu.addAction('Help')
        open_action = menu.addAction('Show in explorer') if filepath else None

        # run menu
        action = menu.exec_(pos)
        if action is None:
            return

        if action == help_action:
            help_window = self.window.get_help_window()
            
            with temporary_import(obj_str) as tmp_import:
                obj = tmp_import.get_object()
                with derivating_stdout(help_window), utils.mouseToBusyState():
                    help(obj)

            help_window.show()
            help_window.scroll_to_the_top()            

        elif action == open_action:
            show_file_in_explorer(filepath)
        
    @utils.busy
    def show_matching_dirs(self):
        """
        Get dir() items from objects field object that match with keywords field
        keyword(s). If no keyword was set, display all results.
        """

        self.window.info('')
        self.window.clear_view()

        found_strings = []

        obj_str, keywords = self.window.get_args()

        try:
            # some easy cases
            if obj_str == 'None':
                found_dirs = dir(None)
            elif obj_str == 'True':
                found_dirs = dir(True)
            elif obj_str == 'False':
                found_dirs = dir(False)
            elif re.match('\d+', obj_str):
                found_dirs = dir(int)
            elif re.match('\d+\.\d*', obj_str):
                found_dirs = dir(float)
            elif obj_str == 'list':
                found_dirs = dir(list)
            elif obj_str == 'tuple':
                found_dirs = dir(tuple)
            elif obj_str == 'set':
                found_dirs = dir(set)
            elif obj_str == 'int':
                found_dirs = dir(int)
            elif obj_str == 'float':
                found_dirs = dir(float)
            elif obj_str == 'str':
                found_dirs = dir(str)
            elif obj_str == 'bool':
                found_dirs = dir(bool)

            else:
                try:
                    found_dirs = get_dir(obj_str)

                except Exception as e:
                    self.window.error(e)
                    traceback.print_exc()
                    return

            if not keywords:
                found_strings = found_dirs

            # filter <found_dirs> by keywords (ignoring case)
            else:
                all_kewords = [x.lower().strip() for x in keywords.split(',')]
                for attr in found_dirs:
                    if any(kw in attr.lower() for kw in all_kewords):
                        found_strings.append(attr)

            if not found_strings:
                self.window.warning('No match found.')
                return
            
            self.window.populate_table(obj_str, found_strings)

            self.window.success('{} match.'.format(len(found_strings)))

        except:
            self.window.clear_view()
            traceback.print_exc()
        
        
class derivating_stdout(object):
    """
    Context manager for re-routing sys.stdout on input object at __enter__, and
    set everything back at __exit__.
    """

    def __init__(self, obj):
        self.obj = obj
        self.old = sys.stdout

    def __enter__(self):
        sys.stdout = self.obj
    def __exit__(self, *args):
        sys.stdout = self.old


def skipped_from_list(func):
    def wrap(obj, *args, **kwargs):
        if obj.as_string() in SKIPPED:
            return None
        return func(obj, *args, **kwargs)
    return wrap


class temporary_import(object):
    """
    Context manager for objects inspection, makes sure that necessary modules are
    imported at context entering. All temporary imports and dependencies will be
    removed at context exit.
    """

    def __init__(self, obj_str):

        self.obj_str = obj_str
        self.object = None
        self.startup_modules = dict(sys.modules)

    def __enter__(self, *args):
        """
        Returns:
            (self) : necessary to enabled "with temporary_import() as" syntax

        Make temp. necessary imports.
        """

        self.object = self.get_object()
        return self

    def __exit__(self, *args):
        """ Remove temporary imports no matter what. """

        to_be_removed = []

        # get keys that were not in sys.modules at startup
        for x in sys.modules:
            if x not in self.startup_modules:
                to_be_removed.append(x)

        # remove "surplus" keys
        for x in to_be_removed or ():
            del sys.modules[x]

    @skipped_from_list
    def get_dir(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (list[str] or None)

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get all names from self._object namespace.
        """

        if self.object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self.obj_str))

        return dir(self.object)

    def as_string(self):
        return self.obj_str

    def object(self):
        """
        Returns:
            (object or None)

        Get the object from the context's input string. If no object could be
        retrieved, returns None.
        """

        return self.object

    @skipped_from_list
    def get_source_file(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (str or None) : absolute path

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get self._object source file's path.
        """

        if self.object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self.obj_str))

        if not hasattr(self.object, '__file__'):
            if silent:
                return None
            raise AttributeError('Could not get \'{}\' source file.'.format(self.obj_str))

        return self.object.__file__

    @skipped_from_list
    def import_object(self, obj_str):
        """
        Args:
            obj_str (str)

        Returns:
            (object or None)

        Import object from <obj_str> et return it. If object could not be imported,
        returns None.
        """

        from_list = []
        if '.' in obj_str:
            from_list = ['.'.join(obj_str.split('.')[:-1])]

        try:
            obj = __import__(obj_str, locals(), globals(), from_list)
            return obj

        except:
            return None

    @skipped_from_list
    def get_object(self):
        """
        Args:
            obj_str (str) : module, method or any object absolute name

        Returns:
            (object or None)

        Get object from <obj_str> if exists (import necessary modules).
        """

        if self.obj_str in sys.modules:
            return sys.modules[self.obj_str]

        roots = []
        obj = None

        for x in self.obj_str.split('.'):

            import_str = '{}.{}'.format('.'.join(roots), x) if roots else x
            module = self.import_object(import_str)

            # x is not an importable object anymore, use getattr() for now
            if not module:
                if not obj:
                    return None

                obj = getattr(obj, x, None)

            else:
                obj = module
                roots.append(x)

        return obj


@utils.busy
def show_file_in_explorer(filepath):
    """
    Args:
        filepath (str) : absolute file path

    Show <filepath> in explorer.
    """
    subprocess.Popen(r'explorer /select,"{}'.format(
            filepath.replace('/', '\\')
        )
    )

def get_dir(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get dir() list from <obj_str> using temporary_import() context manager.
    """
    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_dir(silent=silent)

def get_file(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get source file's path from <obj_str> using temporary_import() context manager.
    """
    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_source_file(silent=silent)


def run():
    window = ui.run()
    manager = Manager(window)
    window.show(dockable=True)