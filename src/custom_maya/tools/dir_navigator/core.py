"""
Simple tool for navigating through modules using dir() method.
"""

import os
import sys
import re
import traceback
import subprocess

from PySide2 import QtWidgets, QtCore, QtGui

from custom_script_editor import utils
from custom_script_editor import constants as kk
from custom_script_editor.controls import IconButton
from custom_script_editor.stylesheet import apply_style
from custom_script_editor.highlight import Regex, STYLE_PATTERN
from custom_script_editor.highlight import char_format
from custom_script_editor.objects_inspector import temporary_import, getdir, getfile


BG_RGB = {
    'success': (89, 166, 61),
    'error' : (217, 82, 82),
    'warning': (204, 191, 128),
    'info': (61, 61, 61)
}

TEXT_RGB = {
    'success': (0, 0, 0),
    'error' : (0, 0, 0),
    'warning': (0, 0, 0),
    'info': (175, 175, 175)
}

WINDOW_NAME = 'PythonDirNavigator'
WINDOW_TITLE = 'Python dir() Navigator'
ICONS_ROOT = os.path.join(os.path.dirname(__file__), 'icons')

CUSTOM_MENU_LABEL = WINDOW_TITLE        # used by menu.py as QAction label

"""
Objects inspection module. Allows actions like getting dir() list or object's
source file, managing temporary imports for required modules.
"""

import sys


SKIPPED = ['test']


def skipped_from_list(func):
    def wrap(obj, *args, **kwargs):
        if obj.as_string() in SKIPPED:
            return None
        return func(obj, *args, **kwargs)
    return wrap


class temporary_import(object):
    """
    Context manager for objects inspection, makes sure that necessary modules are
    imported at context entering. All temporary imports and dependencies will be
    removed at context exit.
    """

    def __init__(self, obj_str):

        self._obj_str = obj_str
        self._object = None
        self._startup_modules = dict(sys.modules)

    def __enter__(self, *args):
        """
        Returns:
            (self) : necessary to enabled "with temporary_import() as" syntax

        Make temp. necessary imports.
        """

        self._object = self._get_object()
        return self

    def __exit__(self, *args):
        """ Remove temporary imports no matter what. """

        to_be_removed = []

        # get keys that were not in sys.modules at startup
        for x in sys.modules:
            if x not in self._startup_modules:
                to_be_removed.append(x)

        # remove "surplus" keys
        for x in to_be_removed or ():
            del sys.modules[x]

    @skipped_from_list
    def get_dir(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (list[str] or None)

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get all names from self._object namespace.
        """

        if self._object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self._obj_str))

        return dir(self._object)

    def as_string(self):
        return self._obj_str

    def object(self):
        """
        Returns:
            (object or None)

        Get the object from the context's input string. If no object could be
        retrieved, returns None.
        """

        return self._object

    @skipped_from_list
    def get_source_file(self, silent=False):
        """
        Args:
            silent (bool, optional) : if True, AttributeError will be silenced

        Returns:
            (str or None) : absolute path

        Raises:
            (AttributeError) : if no object could be retrieve from the input string

        Get self._object source file's path.
        """

        if self._object is None:
            if silent:
                return None
            raise AttributeError('No module named \'{}\' found.'.format(self._obj_str))

        if not hasattr(self._object, '__file__'):
            if silent:
                return None
            raise AttributeError('Could not get \'{}\' source file.'.format(self._obj_str))

        return self._object.__file__

    @skipped_from_list
    def _import_object(self, obj_str):
        """
        Args:
            obj_str (str)

        Returns:
            (object or None)

        Import object from <obj_str> et return it. If object could not be imported,
        returns None.
        """

        from_list = []
        if '.' in obj_str:
            from_list = ['.'.join(obj_str.split('.')[:-1])]

        try:
            obj = __import__(obj_str, locals(), globals(), from_list)
            return obj

        except:
            return None

    @skipped_from_list
    def _get_object(self):
        """
        Args:
            obj_str (str) : module, method or any object absolute name

        Returns:
            (object or None)

        Get object from <obj_str> if exists (import necessary modules).
        """

        if self._obj_str in sys.modules:
            return sys.modules[self._obj_str]

        roots = []
        obj = None

        for x in self._obj_str.split('.'):

            import_str = '{}.{}'.format('.'.join(roots), x) if roots else x
            module = self._import_object(import_str)

            # x is not an importable object anymore, use getattr() for now
            if not module:
                if not obj:
                    return None

                obj = getattr(obj, x, None)

            else:
                obj = module
                roots.append(x)

        return obj


def getdir(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get dir() list from <obj_str> using temporary_import() context manager.
    """

    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_dir(silent=silent)

def getfile(obj_str, silent=False):
    """
    Args:
        obj_str (str)
        silent (bool, optional)

    Get source file's path from <obj_str> using temporary_import() context manager.
    """

    with temporary_import(obj_str) as tmp_import:
        return tmp_import.get_source_file(silent=silent)



def busy(func):
    """
    Decorator that sets mouse as busy during <func> execution.
    """

    def wrap(self, *args, **kwargs):
        QtWidgets.qApp.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        try:
            result = func(self, *args, **kwargs)
            return result
        finally:
            QtWidgets.qApp.restoreOverrideCursor()

    wrap.__doc__ = func.__doc__
    return wrap


class DirNavigator(QtWidgets.QMainWindow):
    """
    Main dir() navigation window.
    """

    def __init__(self, parent=None):
        super(DirNavigator, self).__init__(parent)

        self.setObjectName(WINDOW_NAME)
        self.setWindowTitle(WINDOW_TITLE)
        apply_style(self)

        # set main widget and layout
        central_widget = QtWidgets.QWidget(self)
        lay = QtWidgets.QGridLayout(central_widget)
        self.setCentralWidget(central_widget)

        # create widgets
        self._objects_field = QtWidgets.QLineEdit(
            self,
            placeholderText='Enter object name...',
            toolTip='(imports will be done if necessary)'
        )

        self._back_button = IconButton('back', self, size=22, enabled=False)

        self._keywords_field = QtWidgets.QLineEdit(
            self,
            placeholderText='Enter keyword...',
            toolTip='Seperate keywords with coma. No case match.'
        )

        result_label = QtWidgets.QLabel('Results :', self)

        self._result_table = ResultTable(self)

        # populate layout
        lay.addWidget(self._objects_field, 0, 0)
        lay.addWidget(self._back_button, 0, 1)
        lay.addWidget(self._keywords_field, 1, 0, 1, 2)
        lay.addWidget(result_label, 2, 0, 1, 2)
        lay.addWidget(self._result_table, 3, 0, 1, 2)

        # add status bar
        self._message_bar = StatusBar(parent=self)
        self.setStatusBar(self._message_bar)

        # set few size rules
        for obj in [central_widget, self]:
            obj.setContentsMargins(2, 2, 2, 2)
        lay.setContentsMargins(4, 4, 4, 4)
        self.resize(450, 350)

        self.connect_signals()

    def connect_signals(self):
        self._objects_field.returnPressed.connect(self._on_object_entered)
        self._keywords_field.returnPressed.connect(self._on_keyword_entered)
        self._objects_field.textChanged.connect(self._on_objects_changed)
        self._keywords_field.textChanged.connect(self._on_keyword_entered)

        self._result_table.cellDoubleClicked.connect(self._append_to_object)
        self._back_button.clicked.connect(self._go_back)

    def _on_objects_changed(self):
        """
        (called on object's field.textChanged signal)
        Update back button's enabled state.
        """

        obj_str = self._objects_field.text()

        if obj_str and '.' in obj_str:
            self._back_button.setEnabled(True)
        else:
            self._back_button.setDisabled(True)

    def _on_object_entered(self):
        """
        (called on object's field.returnPressed signal)
        Set focus on keywords field.
        """

        self._keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

    def _on_keyword_entered(self, *args):
        """
        (called on keywords field.returnPressed signal)
        Search for matching items in dir(object) and display result into the
        results table.
        """

        mod = self._objects_field.text()
        if mod:
            self._get_matching_dirs()
        else:
            self._error('Must enter valid object.')

    def _clear_view(self):
        """ Clear results table. """

        while self._result_table.rowCount():
            self._result_table.removeRow(0)

    @busy
    def _get_matching_dirs(self):
        """
        Get dir() items from objects field object that match with keywords field
        keyword(s). If no keyword was set, display all results.
        """

        self._info('')
        self._clear_view()

        found_strings = []

        obj_str = self._objects_field.text()
        keywords = self._keywords_field.text()

        try:
            # some easy cases
            if obj_str == 'None':
                found_dirs = dir(None)
            elif obj_str == 'True':
                found_dirs = dir(True)
            elif obj_str == 'False':
                found_dirs = dir(False)
            elif re.match('\d+', obj_str):
                found_dirs = dir(int)
            elif re.match('\d+\.\d*', obj_str):
                found_dirs = dir(float)
            elif obj_str == 'list':
                found_dirs = dir(list)
            elif obj_str == 'tuple':
                found_dirs = dir(tuple)
            elif obj_str == 'set':
                found_dirs = dir(set)
            elif obj_str == 'int':
                found_dirs = dir(int)
            elif obj_str == 'float':
                found_dirs = dir(float)
            elif obj_str == 'str':
                found_dirs = dir(str)
            elif obj_str == 'bool':
                found_dirs = dir(bool)

            else:
                try:
                    found_dirs = getdir(obj_str)

                except Exception as e:
                    self._error(e)
                    traceback.print_exc()
                    return

            if not keywords:
                found_strings = found_dirs

            # filter <found_dirs> by keywords (ignoring case)
            else:
                all_kewords = [x.lower().strip() for x in keywords.split(',')]
                for attr in found_dirs:
                    if any(kw in attr.lower() for kw in all_kewords):
                        found_strings.append(attr)

            if not found_strings:
                self._warning('No match found.')
                return

            for i, attr in enumerate(found_strings) or ():
                self._result_table.insertRow(i)
                label = Label(attr, '{}.{}'.format(obj_str, attr), self)
                self._result_table.setCellWidget(i, 0, label)

            self._success('{} match.'.format(len(found_strings)))

        except:
            self._clear_view()
            traceback.print_exc()

    @busy
    def _show_file_in_explorer(self, filepath):
        """
        Args:
            filepath (str) : absolute file path

        Show <filepath> in explorer.
        """

        if filepath is None:
            self._error('could not get source object\'s source file.')
            return

        subprocess.Popen(r'explorer /select,"{}'.format(
                filepath.replace('/', '\\')
            )
        )

    @busy
    def _show_help(self, obj):
        """
        Args:
            obj (object)

        Open help window for <obj>.
        """

        window = HelpWindow(obj, self)
        window.show()
        window.scroll_to_the_top()

    def _append_to_object(self, row, col):
        """
        Args:
            row (int)
            col (int)

        (called on ResultTable.cellDoubleClicked signal).
        Add double-clicked item to objects field, separated with a dot.
        """

        label = self._result_table.cellWidget(row, col).text()
        current_object = self._objects_field.text()

        if not current_object.split('.')[-1] == label:
            # append to objects field
            self._objects_field.setText('{}.{}'.format(current_object, label))
            # clear ResultTable
            self._clear_view()
            # clear keywords field
            self._keywords_field.setText('')
            self._keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

    def _go_back(self):
        """
        (called on back button.clicked signal)
        Set object field to current object's parent and set focus on keyword field.
        """

        obj_str = self._objects_field.text()
        new_obj_str = '.'.join(obj_str.split('.')[:-1])

        self._objects_field.setText(new_obj_str)
        self._keywords_field.setFocus(QtCore.Qt.MouseFocusReason)

        self._clear_view()

    def contextMenuEvent(self, event):
        """ Qt re-implementation. """

        # get Label object under mouse
        global_pos = self.mapToGlobal(event.pos())
        pos = self._result_table.mapFromGlobal(global_pos)

        index = self._result_table.indexAt(pos)
        label = self._result_table.cellWidget(index.row(), index.column())

        if not label:
            return

        # get object, name and source file
        obj_str = label.full_path
        obj_file = getfile(obj_str, silent=True)

        # create menu
        menu = QtWidgets.QMenu(self)
        help_action = menu.addAction('Help')
        open_action = menu.addAction('Show in explorer') if obj_file else None

        # run menu
        action = menu.exec_(global_pos)
        if action is None:
            return

        if action == help_action:
            with temporary_import(obj_str) as tmp_import:
                obj = tmp_import.object()
                self._show_help(obj)

        elif action == open_action:
            self._show_file_in_explorer(obj_file)

    def _success(self, message, timeout=5000):
        self._show_message(message, 'success', timeout=timeout)

    def _error(self, message, timeout=5000):
        self._show_message(message, 'error', timeout=timeout)

    def _warning(self, message, timeout=5000):
        self._show_message(message, 'warning', timeout=timeout)

    def _info(self, message, timeout=5000):
        self._show_message(message, 'info', timeout=timeout)

    def _show_message(self, message, lvl_type, timeout=5000):
        """
        Args:
            message (str)
            lvl_type (str) : 'info', 'warning', 'success' or 'error'
            timeout (int)

        Show <message> with <lvl_type> format and colors into message bar.
        """

        # format message
        if message:
            message = '{} : {}'.format(
                lvl_type.capitalize(), message
            )

        # display message
        self._message_bar.set_color(lvl_type)
        self._message_bar.showMessage(message, timeout=timeout)


class Label(QtWidgets.QLabel):
    """
    Custom QLabel with full path information.
    """

    def __init__(self, label, full_path, parent=None):
        super(Label, self).__init__(label, parent)

        self.full_path = full_path


class ResultTable(QtWidgets.QTableWidget):
    """
    QTableWidget for dir() results display.
    """

    def __init__(self, parent=None):
        super(ResultTable, self).__init__(parent)

        self.navigator = parent

        # set a few flags
        self.setColumnCount(1)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

        # edit headers
        h_header = self.horizontalHeader()
        h_header.hide()
        h_header.setStretchLastSection(True)
        v_header = self.verticalHeader()
        v_header.hide()
        v_header.setDefaultSectionSize(20)

        self.setVerticalScrollMode(self.ScrollPerItem)


class StatusBar(QtWidgets.QStatusBar):
    """
    QStatuBar for messages display.
    """

    def __init__(self, parent=None):
        super(StatusBar, self).__init__(parent)

        self.set_color('info')

        self.messageChanged.connect(self.on_message_change)

    def set_color(self, lvl_type):
        """
        Args:
            lvl_type (str) : 'info', 'warning', 'error' or 'success'
        Set QStatusBar background and text colors from <lvl_type>.
        """

        bg_rgb = str(BG_RGB[lvl_type])
        text_rgb = str(TEXT_RGB[lvl_type])

        self.setStyleSheet('background-color: rgb{}; color: rgb{};'.format(
                bg_rgb, text_rgb
            )
        )

    def on_message_change(self, message):
        """
        Args:
            message (str)

        Restore "info" color on empty <message> (amongst others, is triggered
        at messages timeout).
        """

        if not message:
            self.set_color('info')


class derivating_stdout(object):
    """
    Context manager for re-routing sys.stdout on input object at __enter__, and
    set everything back at __exit__.
    """

    def __init__(self, obj):
        self.obj = obj
        self.old = sys.stdout

    def __enter__(self):
        sys.stdout = self.obj
    def __exit__(self, *args):
        sys.stdout = self.old


class HelpWindow(QtWidgets.QDialog):
    """
    QDialog for help() display (see `derivating_stdout` context-manager).
    """

    def __init__(self, obj, parent=None):
        super(HelpWindow, self).__init__(parent)

        self.setWindowTitle('Help')

        layout = QtWidgets.QVBoxLayout(self)

        self._txt_edit = QtWidgets.QTextEdit(
            self,
            lineWrapMode=QtWidgets.QTextEdit.NoWrap
        )

        self._txt_edit.setObjectName('PDNhelpText')
        self._highlighter = HelpHighlighter(self._txt_edit)

        layout.addWidget(self._txt_edit)
        self.resize(450, 350)

        # display help() result
        self._show_help(obj)

    def _show_help(self, obj):
        """ Print help(obj) in the QTextEdit. """

        with derivating_stdout(self):
            help(obj)

    def write(self, txt):
        """ sys.stdout re-implementation """
        self._txt_edit.append(txt)

    def writelines(self, lines):
        """ sys.stdout re-implementation """
        self._txt_edit.clear()
        self._txt_edit.append('\n'.join(lines))

    def scroll_to_the_top(self):
        self._txt_edit.verticalScrollBar().setSliderPosition(0)


class HelpHighlighter(QtGui.QSyntaxHighlighter):
    """
    Help result text highlighter.
    """

    def __init__(self, txt_edit):

        self._styles = self._get_styles()
        self._rules = self._get_rules()

        # apply stylesheet
        style_body = STYLE_PATTERN % {
            'object_name': txt_edit.objectName(),
            'color': self._styles['base_text'],
            'background': self._styles['background'],
            'paddingLine': ''
        }

        txt_edit.setStyleSheet(style_body)

        super(HelpHighlighter, self).__init__(txt_edit)

    def highlightBlock(self, line):
        """
        Args:
            line (str)

        Qt re-implementation. Apply syntax highlighting to the given line.
        """

        for regex, nth, txt_format in self._rules:
            for match in regex.finditer(line):
                start = match.start(nth)
                end = match.end(nth)

                self.setFormat(start, end -start, txt_format)

    def _get_styles(self):
        """
        Returns:
            (dict) : from type {'name': QtGui.QTextCharFormat}

        Get text formats by name as dict.
        """

        return {
            'grey': char_format((90, 99, 111)),
            'orange': char_format((207, 154, 102)),
            'green': char_format((147, 195, 121)),
            'pink': char_format((200, 116, 220)),
            'yellow': char_format((228, 187, 106)),
            'blue': char_format((92, 166, 237)),
            'red': char_format((224, 109, 116)),
            'background': (29, 34, 46),
            'base_text': (170, 176, 190),
            'cyan': char_format((87, 180, 193))
        }

    def _get_rules(self):
        """
        Returns:
            (list[tuple(QtCore.QRegExp, int, QtGui.QTextCharFormat)])

        Get all highlight rules.
        """

        # operators rules
        rules = [(Regex('%s' % o), 0, self._styles['pink']) for o in kk.OPERATORS]
        # # python keywords rules
        # rules += [(Regex('\\b(%s)\\b' % w), 0, self._styles['pink']) for w in kk.PYTHON_KEYWORDS]
        # intermediate objects rule
        rules += [(Regex('(\.)(\w+)'), 2, self._styles['red'])]
        # functions names
        rules += [(Regex('\\b(\w+)\\b\s*\('), 1, self._styles['blue'])]
        # class names
        rules += [(Regex('(\\bclass\\b\s*)(\\b_*\w+_*\\b)'), 2, self._styles['yellow'])]
        # class args
        rules += [(Regex('\\bclass\\b\s*\\b_*\w+_*\\b\((\w+)\)'), 1, self._styles['yellow'])]
        # missing
        rules += [(Regex('\((\.\.\.)\)'), 1, self._styles['grey'])]
        # attributes
        rules += [(Regex('^\s*\|*\s*(\w+)\s*\='), 1, self._styles['orange'])]
        # double-quoted string, possibly containing escape sequences
        rules += [(Regex(r'"[^"\\]*(\\.[^"\\]*)*"'), 0, self._styles['green'])]
        # single-quoted string, possibly containing escape sequences
        rules += [(Regex(r"'[^'\\]*(\\.[^'\\]*)*'"), 0, self._styles['green'])]

        rules += [
            # start '|' chars
            (Regex('^(\s*\|\s*)'), 1, self._styles['grey']),
            # '| --------' lines
            (Regex('^(\s*\|)\s*(\-+)'), 2, self._styles['grey']),
            # lines that ends with a ':'
            (Regex('^(\s*\|\s*)*(.+\:\s*)$'), 2, self._styles['green']),
            # lines in CAPS (like "FILE")
            (Regex('^([A-Z0-9\s]+)$'), 0, self._styles['orange']),
            # python objects
            (Regex('(\<[\w\.\'\"\s\-]+\>*)(\.\.\.)*'), 1, self._styles['cyan']),
            # paths
            (Regex('\s*([\w\s\-\:]+\\\\)+([\w\-]+\.\w+)\s*'), 0, self._styles['green'])
        ]

        return rules


def run():
    """
    Show "Python dir() Navigator" with Maya Window as parent.
    """

    maya_ui_qt = utils.get_maya_window()
    utils.close_existing(maya_ui_qt, WINDOW_NAME)

    new_window = DirNavigator(parent=maya_ui_qt)
    new_window.show()
